###Welcome###

This tool is a prototype designed to evaluate the impact of a feature change on existing configurations of a multi-product line. 

It takes as an input a folder containing multiple FeatureIDE projects, and the name of a feature to remove. 
As an output, it provide the list of configuration of each feature model that are invalidated by the change. 

This work has been performed as part of an ongoing research on Product Line Evolution. 

### This repository contains ###

	- Neo4JFM : neo4j feature model tool
		the core application
	- Neo4jFM_tests: the test used to validate the tool
	- ExternalDependencies: all external jars that are required to run the tool.
		this includes Neo4j Libraries (access to the database), Swing libraries (user interface), FeatureIDE libs (to configurations w/r to FMs), SAT4j libraries (required by FeatureIDE).
		
### Getting started ###

* You need: 
 Eclipse
 Eclipse Git integration
 Windows Builder for Eclipse (recommended if you want to edit the user interface)

* Setup

	Use the following Git command to clone the repository to a local directory

	git clone https://NJRD@bitbucket.org/NJRD/mpl-change-impact.git
	*Warning: the tests involved quite a lot of data. There are many files in the repository, the download might take a while.

	Using the Eclipse Git integration:
	(recommended) Open Eclipse in a clean workspace 	
	Start "Import" 
	In the following menu, select Git -> "Projects from Git"
	Select the "local repository" option
	Select (on the top right corner) "Add", to add a local repository searchable by Eclipse.
	Set the search path to the directory in which you cloned the repository.
	The local repository should now be visible in the list of available repos.


	Use "Import existing project" - you'll take advantage of the pre-existing project structure !
	
	*Warning: do not search for nested project. You would get all projects that were used to create test data. That's useless. 
	
	==> x projects should be present in your Eclipse workspace now: 
	- External Dependencies (only jars, used to facilitate distribution)
	- Neo4jFM (core project)
	- Neo4jFM_Tests (test projects, yes... )

* Almost done: 
	You now need to edit the paths.properties file
		it contains the path to the Neo4j database. The database engine is embedded in the code, so you can point to any empty folder.
		Warning: the folder will be scratched each time you  start the tool. 


* Play! 
	We provide a small working set: 3 feature models and some confurations in the "Data\_Sample" folder. 
	As an import path, copy/paste the path to your git Data\_Sample folder (complete)
	The models are named: model\_1,model\_2, and model\_3.
	Features you can removed are all of them, but you can try "Feature\_7", 8 or 12 - they are shared between the different models. 
	There are not many configurations in the sample, so expect large impact upon feature removal.
	
* Recommended:
	If you install the FeatureIDE plugin for Eclipse, you can open the Data\_Sample sub folder as FeatureIDE projects. 
	This way, you can review the feature models and configuration (and modify them if you wish). 
	Everything about FeatureIDE can be found [here](http://wwwiti.cs.uni-magdeburg.de/iti_db/research/featureide/).

### Contacts ###
For questions regarding the prototype, feel free to contact me (Nicolas) using the following email address: 
N.J.R.Dintzner (at) tudelft (dot) nl

