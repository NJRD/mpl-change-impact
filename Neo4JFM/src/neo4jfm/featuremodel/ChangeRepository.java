package neo4jfm.featuremodel;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

public class ChangeRepository
{

	private Map<String, List<ConfChangeReport>>	_conf_change_repo	= new HashMap<String, List<ConfChangeReport>>();
	private Map<String, List<CFMCChangeReport>>	_cfmc_change_repo	= new HashMap<String, List<CFMCChangeReport>>();
	
	private List<ConfChangeReport>				root_confs			= new ArrayList<ConfChangeReport>();
	private List<CFMCChangeReport>				root_cfmcs			= new ArrayList<CFMCChangeReport>();			
	static ChangeRepository						_instance			= null;

	public List<ConfChangeReport> getRootConfChanges()
	{
		return root_confs;
	}

	public List<CFMCChangeReport> getRootCFMCChanges()
	{
		return root_cfmcs;
	}

	public static ChangeRepository getRepo()
	{
		if ( _instance == null )
			_instance = new ChangeRepository();
		return _instance;
	}

	public List<ConfChangeReport> getConfChangeReportForModel(String model_name)
	{
		return _conf_change_repo.get( model_name );
	}

	public List<CFMCChangeReport> getCFMCChangeReportForModel(String model_name)
	{
		return _cfmc_change_repo.get( model_name );
	}

	public void addCFMCReport(CFMCChangeReport r)
	{
		String from = r.getFm1();
		String to = r.getFm2();

		if ( from == null || to == null || from.length() == 0 || to.length() == 0 )
			throw new IllegalArgumentException("Cannot store in repo cross-fm constraint change reports not assigned to 2 feature models" );

		// add to "from", and then, add to "to"
		List<CFMCChangeReport> from_changes = _cfmc_change_repo.get( from );
		if ( from_changes == null )
		{
			from_changes = new ArrayList<CFMCChangeReport>();
			_cfmc_change_repo.put( from , from_changes );
		}
		from_changes.add( r );

		List<CFMCChangeReport> to_changes = _cfmc_change_repo.get( to );
		if ( to_changes == null )
		{
			to_changes = new ArrayList<CFMCChangeReport>();
			_cfmc_change_repo.put( to , to_changes );
		}
		to_changes.add( r );

		if ( r.getCause() == null )
		{
			root_cfmcs.add( r );
		}

	}

	public void addConfChangeReport(ConfChangeReport r)
	{
		if ( r.fm == null || r.fm.length() == 0 )
			throw new IllegalArgumentException( "Cannot store in repo a report not assigned to a feature model" );

		List<ConfChangeReport> existing_reports = _conf_change_repo.get( r.fm );

		if ( existing_reports == null )
		{
			existing_reports = new ArrayList<ConfChangeReport>();
			_conf_change_repo.put( r.fm , existing_reports );
		}
		
		if(!existing_reports.contains( r ))
			existing_reports.add( r );

		if ( r.getCause() == null )
		{
			root_confs.add( r );
		}

	}

	public void clear_repo()
	{
//		_conf_change_repo	= new HashMap<String, List<ConfChangeReport>>();
//		_cfmc_change_repo	= new HashMap<String, List<CFMCChangeReport>>();
//
//		root_confs			= new ArrayList<ConfChangeReport>();
//		root_cfmcs			= new ArrayList<CFMCChangeReport>();			
		_instance = new ChangeRepository();
	}
	
	
	public int conf_change_count()
	{
			return _conf_change_repo.size();
	}
	
	public int cfmc_change_count()
	{
		return _cfmc_change_repo.size();
	}
	
	public int conf_change_report_counts()
	{
		int count = 0;
		for(String s : _conf_change_repo.keySet())
		{
			List<ConfChangeReport> l = _conf_change_repo.get( s );
			
			for(ConfChangeReport r : l)
			{
				count+= r.getRemovedConfigurations().size();
			}

		}
		return count;
	}
	
	public int cmfc_change_report_counts()
	{
		int count = 0;
		for(String s : _conf_change_repo.keySet())
		{
				List<CFMCChangeReport> l = _cfmc_change_repo.get( s );
				if(l == null)
					continue;
				
				for(CFMCChangeReport r : l)
				{
					count+= r.getRemoved_valid_selection_states().size();
				}

		}
		return count;
	}

	public List<CFMCChangeReport> getCFMCChangesForFMs(String source_name, String target_name)
	{
		List<CFMCChangeReport> reports = new ArrayList<CFMCChangeReport>();

		List<CFMCChangeReport> source_changes = this.getCFMCChangeReportForModel( source_name );
		for ( CFMCChangeReport r : source_changes )
		{
			if ( r.getFm2().equals( target_name ) )
				reports.add( r );
		}

		List<CFMCChangeReport> target_changes = this.getCFMCChangeReportForModel( target_name );
		for ( CFMCChangeReport r : target_changes )
		{
			if ( r.getFm2().equals( source_name ) )
				reports.add( r );
		}

		return reports;
	}

	/**
	 * String dump of the content - for debug purpose.
	 * @return
	 */
	public String dump()
	{
		StringBuffer info = new StringBuffer( 250 );

		for ( String fm_name : _conf_change_repo.keySet() )
		{
			info.append( "impact on feature model " + fm_name + ": " );
			for ( ConfChangeReport r : _conf_change_repo.get( fm_name ) )
			{
				info.append( r.dump() );
				info.append( "\n" );
			}
		}

		for ( String fm_name : _cfmc_change_repo.keySet() )
		{
			info.append( "impact on composition rules " + fm_name + ":  \n" );
			for ( CFMCChangeReport r : _cfmc_change_repo.get( fm_name ) )
			{
				info.append( r.dump() );
				info.append( "\n" );
			}
		}
		return info.toString();

	}

}
