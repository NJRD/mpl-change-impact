package neo4jfm.featuremodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Node;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.utils.GraphNavigator;


public class NeoCrossFMConstraint extends NeoEntity
{
	protected GraphNavigator nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
	
	public String rule = "";
	public String model_1 = "";
	public String model_2 = "";
	public List<String> state = new ArrayList<String>();
	
	
	public  NeoCrossFMConstraint(Node n)
	{
		super(n);
		rule = attributes.get( "rule" );
		
		List<Node> models = nav.getPointedNodes( n , RelTypes.CONSTRAINS );
		model_1 = nav.getAttributes( models.get( 0 ) ).get( Attributes.NAME );
		model_2 = nav.getAttributes( models.get( 1 ) ).get( Attributes.NAME );
	}
	
	
	public NeoCrossFMConstraint(String rule, String from, String to )
	{
		this._node = null;
		this.rule = rule; 
		model_1 = from;
		model_2 = to;
		
		String[] feat_names = rule.split( "\\|\\|" );
		for(int i = 0 ; i < feat_names.length ; i++)
		{
			if(feat_names[i].trim().length() > 0)
			state.add( feat_names[i].trim() );
		}
		
	}
	
	public static NeoCrossFMConstraint getCrossFMConstraint(long id)
	{
		return null;
	}

	public static List<List<String>> identifyConstraints(Map<String, List<String>> conf_set1, Map<String, List<String>> conf_set2, List<String> shared_features)
	{
		List<List<String>> validStates = new ArrayList<List<String>>();

		List<List<String>> state_set1 = getSharedStatesForFM( conf_set1 , shared_features );
		List<List<String>> state_set2 = getSharedStatesForFM( conf_set2 , shared_features );

		for ( List<String> state2 : state_set2 )
		{
			for ( List<String> state1 : state_set1 )
			{
				if ( state2.containsAll( state1 ) && state1.containsAll( state2 ) )
				{	// we found a compatible set of states, we need to make sure
					// it hasn't been identified yet
					boolean needToAdd = true;
					for ( List<String> state : validStates )
					{
						if ( state.containsAll( state1 ) )
							needToAdd = false;
					}

					if ( needToAdd )
						validStates.add( state1 );
				}
			}
		}
		return validStates;
	}

	private static List<List<String>> getSharedStatesForFM(Map<String, List<String>> otherConfs, List<String> sharedFeats)
	{
		List<List<String>> validStates = new ArrayList<List<String>>();
		for ( String confName : otherConfs.keySet() )
		{
			List<String> selectedFeatures = otherConfs.get( confName );
			List<String> shared_state = new ArrayList<String>();
			for ( String shared : sharedFeats )
			{
				if ( selectedFeatures.contains( shared ) )
					shared_state.add( shared );
				else
					shared_state.add( "!" + shared );
			}

			if ( shared_state.size() != 0 )
			{
				validStates.add( shared_state );
			}
		}
		return validStates;
	}


	
	
	
	/** 
	 * Updates the list of valid states with respect to changes existing in the change repository (adds what has been created, removes what has been removed).
	 * 
	 * @param reference_states the list of selection states as found in the database
	 * @param source_fm 	fm 1
	 * @param target_fm	fm 2
	 * @return an updated list of the valid selection state.
	 */
	public static List<List<String>> updateStatesWithChanges(List<List<String>> reference_states, NeoFM source_fm, NeoFM target_fm)
	{
		String target_name= "";
		if(target_fm != null)
				target_name = target_fm.attributes.get(Attributes.NAME) ;
		
		String source_name =  source_fm.attributes.get(Attributes.NAME);
		
		List<CFMCChangeReport> changes = new ArrayList<CFMCChangeReport>();
		
		List<CFMCChangeReport> changes1 = ChangeRepository.getRepo().getCFMCChangeReportForModel(source_name );
		if(changes1 != null)
		{
			for(CFMCChangeReport r : changes1)
			{
				if(r.getFm2().equals( target_name ))
				{
					changes.add(r);
				}
			}
		}
		
		List<CFMCChangeReport> changes2 = ChangeRepository.getRepo().getCFMCChangeReportForModel(target_name );
		if(changes2 != null)
		{
			for(CFMCChangeReport r : changes2)
			{
				if(r.getFm2().equals( source_name ))
				{
					changes.add(r);
				}
			}
		}
		
		List<List<String>> new_states = new ArrayList<List<String>>();
		new_states.addAll(reference_states);
		
		for(CFMCChangeReport r : changes)
		{
			new_states.addAll (r.getAdded_valid_selection_states());
		}
		
		List<List<String>> states_to_remove = new ArrayList<List<String>>();
		
		for(CFMCChangeReport r : changes)
		{
			List<List<String>> invalid_states = r.getRemoved_valid_selection_states();
			for(List<String> invalid_state : invalid_states)
			{
				for(List<String> ref : new_states)
				{
					if(ref.containsAll( invalid_state ) && invalid_state.containsAll( ref ))
						states_to_remove.add( invalid_state );
				}
			}
		}
		
		new_states.removeAll( states_to_remove );

		return new_states;
	}

	/**
	 * Return true of the conf is compliant with the rule.
	 * @param rule
	 * @param conf
	 * @return
	 */
	public static boolean isCompliant(List<String> rule, List<String> conf)
	{
		List<String> must_have = new ArrayList<String>();
		List<String> must_not_have = new ArrayList<String>();
		for(String expr : rule)
		{
			if(expr.startsWith( "!" ))
			{
				must_not_have.add(expr.substring( 1 , expr.length() ));
			}
			else
			{
				must_have.add(expr);
			}
		}
		
		if(conf.containsAll( must_have ))
		{
			boolean has_none_of = true;
			for(String s : must_not_have)
			{
				if(conf.contains( s ))
					has_none_of = false;
			}
			
			if(has_none_of)
				return true;
		}
		
		return false;
	}


	public static List<NeoCrossFMConstraint> updateStatesWithChanges(List<NeoCrossFMConstraint> rules, NeoFM neoFM)
	{
		List<CFMCChangeReport> changes1 = ChangeRepository.getRepo().getCFMCChangeReportForModel(neoFM.attributes.get( Attributes.NAME ) );
		if(changes1 == null)
			return rules;
		
		List<NeoCrossFMConstraint> toRemove = new ArrayList<NeoCrossFMConstraint>();
		List<NeoCrossFMConstraint> results =  new ArrayList<NeoCrossFMConstraint>();
		results.addAll(rules);
		
		for(CFMCChangeReport r : changes1 )
		{
			for(List<String> state : r.getRemoved_valid_selection_states())
			{
				for(NeoCrossFMConstraint rule : rules)
				{
					
					List<String> local = new ArrayList<String>();
					String[] values = rule.rule.split( "\\|\\|" );
					for ( String s : values )
					{
						String tmp = s;
						if ( tmp.trim().length() > 0 )
							local.add( s.trim() );
					}

					if(local.containsAll( state ) && state.containsAll( local ))
						toRemove.add(rule);
				}
			}
			
			for(List<String> state : r.getAdded_valid_selection_states())
			{
				String new_rule = "";
				for(String s : state)
				{
					new_rule+= (s+ " || ");
				}
				NeoCrossFMConstraint c = new NeoCrossFMConstraint( new_rule , r.getFm1() ,r.getFm2() );
				results.add( c );
			}
		}
	
		results.removeAll(toRemove);
		
		return results;
	}


	public boolean isSupportedByConfs(Map<String, List<String>> configurations)
	{
		List<String> must = new ArrayList<String>();
		List<String> must_not = new ArrayList<String>();
		
		
		if(  ( state == null  || state.size() == 0 )  &&  rule.length() != 0)
		{
			String[] values = this.rule.split( "\\|\\|" );
			for ( String s : values )
			{
				String tmp = s;
				if ( tmp.trim().length() > 0 )
					state.add( s.trim() );
			}
		}
		
		
		for(String s : state)
			if(s.contains( "!" ))
				must_not.add( s.replace( "!" , "" ).trim() );
			else
				must.add(s.trim());

		for(String conf_name : configurations.keySet())
		{
			List<String> conf = configurations.get( conf_name );
			boolean match =false;
			
			if(conf.containsAll( must) )
			{
				match = true;
				for(String s : must_not)
				{
					if(conf.contains( s ))
						match = false;
				}
			}
			if (match)
				return true;
		}
		return false;
	}
	
	
	
	
}
