package neo4jfm.featuremodel;


import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;


/**
 * This class represent a graph db entity. 
 * 
 */
public class NeoEntity
{
	protected long id = -1;
	
	public Map<String,String> attributes = null;
	
	static protected GraphNavigator nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
	static protected GraphEditor editor = new GraphEditor(ExternalResourcesRepository.graphDb);
	
	protected Node _node = null;
	
	public NeoEntity(Node n)
	{
		attributes = nav.getAttributes( n );
		id = n.getId();
		setNode( n );
	}
	public NeoEntity()
	{
		attributes = null;
	}
	public Node getNode()
	{
		return _node;
	}
	public void setNode(Node node)
	{
		this._node = node;
	}
	
}
