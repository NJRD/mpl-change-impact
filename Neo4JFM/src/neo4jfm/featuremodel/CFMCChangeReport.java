package neo4jfm.featuremodel;

import java.util.ArrayList;
import java.util.List;


public class CFMCChangeReport
{
	private String fm1 = ""; 
	private String fm2 = "";
	
	private List<List<String>> added_valid_selection_states = new ArrayList<List<String>>(); 
	private List<List<String>> removed_valid_selection_states = new ArrayList<List<String>>();
	private List<ConfChangeReport> propagation = new ArrayList<ConfChangeReport>();
	private List<Error> errors = new ArrayList<Error>();
	
	public List<ConfChangeReport>  getPropagatedChanges()
	{
		return propagation;
	}
	public void addError(Error err)
	{
		errors.add( err );
	}
	
	
	private ConfChangeReport cause = null;
	
	
	public List<ConfChangeReport> getPropagation()
	{
		return propagation;
	}

	public void addConfReportToPropagate(ConfChangeReport r)
	{
		this.propagation.add( r );
	}
	
	public void setPropagation(List<ConfChangeReport> propagation)
	{
		this.propagation = propagation;
	}


	public String getFm1()
	{
		return fm1;
	}

	
	public void setFm1(String fm1)
	{
		this.fm1 = fm1;
	}

	
	public String getFm2()
	{
		return fm2;
	}

	
	public void setFm2(String fm2)
	{
		this.fm2 = fm2;
	}

	
	public List<List<String>> getAdded_valid_selection_states()
	{
		return added_valid_selection_states;
	}

	
	public void setAdded_valid_selection_states(List<List<String>> added_valid_selection_states)
	{
		this.added_valid_selection_states = added_valid_selection_states;
	}

	
	public List<List<String>> getRemoved_valid_selection_states()
	{
		return removed_valid_selection_states;
	}

	
	public void setRemoved_valid_selection_states(List<List<String>> removed_valid_selection_states)
	{
		this.removed_valid_selection_states = removed_valid_selection_states;
	}

	public String dump()
	{
			StringBuffer info = new StringBuffer(250);
			
			info.append(" - C.F.M.C changes  between \" " + fm1 + "\" and  \"" + fm2 + "\"\n");
			
			for(Error err : errors)
			{
				info.append("\t+[ " + err.error + "] " + err.getMessage() + "\n");
			}
			
			if(!removed_valid_selection_states.isEmpty())
			{	
				info.append("\tThe following states are no longer valid\n");
				for(List<String> state : removed_valid_selection_states)
				{
					info.append( "\t"+state.toString()  +"\n");
				}
			}
			
			if(!added_valid_selection_states.isEmpty())
			{	
				info.append("The following states are now valid:\n");
				for(List<String> state : added_valid_selection_states)
				{
					info.append("\t" + state.toString()  +"\n");
				}
			}
			
			if(propagation.size() != 0 )
			{
				info.append("\tInduced configuration changes:  " + propagation.size() +"\n");
				for(ConfChangeReport r : propagation)
				{
					info.append( r.dump() );
				}
				
			}
			return info.toString();
	}

	public ConfChangeReport getCause()
	{
		return cause;
	}

	public void setCause(ConfChangeReport cause)
	{
		this.cause = cause;
	}
	public String details()
	{
		String msg = "Shared feature constraint changes between [ "+ fm1 + "] and ["+fm2+"]\n";

		if(0 != added_valid_selection_states.size() )
		{
			msg+="Added shared feature constraints: \n";
			
			for(List<String> added_state : added_valid_selection_states)
			{
				String state_list = "+";
				for(String s : added_state)
				{
					state_list += s + ", ";
				}
				msg+=state_list +"\n";
			}
		}
		
		if(0 != removed_valid_selection_states.size() )
		{
			msg+="Removed shared feature constraints: \n";
			
			for(List<String> rem_state : removed_valid_selection_states)
			{
				String state_list = "+";
				for(String s : rem_state)
				{
					state_list += s + " ";
				}
				msg+=state_list +"\n";;
			}
		}
		return msg;
	}



}
