package neo4jfm.featuremodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ConfChangeReport
{
	
	public String fm;
	
	private Map<String,List<String>> removedConfigurations = new HashMap<String,List<String>>(); 
	private Map<String,List<String>> addedConfigurations = new HashMap<String,List<String>>();
	
	private List<CFMCChangeReport> propagation = new ArrayList<CFMCChangeReport>();

	public List<CFMCChangeReport> getPropagation ()
	{
		return propagation;
	}
	
	public void resetPropagatedChanges()
	{
		propagation = new ArrayList<CFMCChangeReport>();
	}
	
	private CFMCChangeReport cause = null;
	
	private List<Error> errors = new ArrayList<Error>();
	
	public void addError(Error err)
	{
		errors.add( err );
	}
	
	public Map<String,List<String>> getRemovedConfigurations()
	{
		return removedConfigurations;
	}
	
	public void setCause (CFMCChangeReport r)
	{
		cause = r;
	}
	
	public CFMCChangeReport getCause()
	{
		return cause;
	}

	public void setRemovedConfigurations(Map<String,List<String>> removedConfigurations)
	{
		if(removedConfigurations == null)
			this.removedConfigurations = new HashMap<String,List<String>>();
		else
			this.removedConfigurations = removedConfigurations;
	}
	
	public Map<String,List<String>>getAddedConfigurations()
	{
		return addedConfigurations;
	}

	
	public void setAddedConfigurations(Map<String,List<String>> addedConfigurations)
	{
		if(addedConfigurations == null)
			this.addedConfigurations = new HashMap<String,List<String>>();
		else
			this.addedConfigurations = addedConfigurations;
	}
	
	public void addPropagatedChange(CFMCChangeReport report)
	{
		propagation.add(report);
	}
	
	public void addPropagatedChanges(List<CFMCChangeReport> reports)
	{
		propagation.addAll(reports);
	}
	
	public String dump()
	{
		StringBuffer info = new StringBuffer(250);
		info.append("____________________\n");
		info.append( " - Configuration changes in \"" + fm + "\"\n");
		
		for(Error err : errors)
		{
			info.append("\t+[ " + err.error + "] " + err.getMessage() + "\n");
		}
		
		info.append("\tAdded configurations (" + addedConfigurations.size() + " )\n");
		for(String add_conf_name : addedConfigurations.keySet())
		{
				info.append("\t\tconfiguration ["+add_conf_name+"]: ");
				for(String feat : addedConfigurations.get( add_conf_name ))
				{
					info.append(feat + ",  ");
				}
				info.append("\n");
		}
		
		info.append("\tRemoved configurations (" + removedConfigurations.size() + " )\n");
		
		for(String add_conf_name : removedConfigurations.keySet())
		{
				info.append("\t\tconfiguration ["+add_conf_name+"] : ");
				for(String feat : removedConfigurations.get( add_conf_name ))
				{
					info.append(feat + ",  ");
				}
				info.append("\n");
		}
		info.append("\n");
		
		if(!propagation.isEmpty())
		{
			info.append( "\tInduced C.F.M.C changes :  (" + propagation.size()+")\n");
			for(CFMCChangeReport r : this.propagation)
			{
				info.append( r.dump());
				info.append( "\n");
			}
		}
		return info.toString();
	}

	public String details()
	{
		String msg = "Configuration change on feature model " + fm + "\n";
		if(0 != addedConfigurations.size())
		{
			msg+="Added configurations : \n";
			for(String s : addedConfigurations.keySet())
			{
				msg+= "[" + s +"] :"; 
				for(String f : addedConfigurations.get( s ))
					msg+= f+ " ";
				msg+="\n";
			}
			
		}
		
		if(0 != removedConfigurations.size())
		{
			msg+="Removed configurations ("+removedConfigurations.size()+"): \n";
			for(String s : removedConfigurations.keySet())
			{
				msg+= "[" + s +"] :"; 
//				for(String f : removedConfigurations.get( s ))
//					msg+= f + " ";
				msg+="\n";
			}
			msg+="\n";
		}
		
		return msg;
		
	}
	
}
