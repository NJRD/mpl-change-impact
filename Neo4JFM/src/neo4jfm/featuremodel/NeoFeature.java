package neo4jfm.featuremodel;

import java.util.Map;

import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

public class NeoFeature extends NeoEntity
{

	
	protected NeoFeature(Node n)
	{
		super(n);
	}
	
	public static NeoFeature getFeature(long neo4jId)
	{
		return null;
	}
	
	public static NeoFeature createNew(String name, Map<String,String> properties)
	{
		properties.put("type", Types.FEATURE);
		Node n  = editor.createNode( name , properties );
		return new NeoFeature(n);
	}
	
	public void addSubFeature(NeoFeature feature, String c)
	{
		Relationship r = getNode().createRelationshipTo(feature.getNode(), RelTypes.CONTAINS);
		r.setProperty( Attributes.CHOICE_TYPE, c );
	}

}
