package neo4jfm.featuremodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Constraint;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.Error.errors;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;


import de.ovgu.featureide.fm.core.Feature;
import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.io.UnsupportedModelException;
import de.ovgu.featureide.fm.core.io.xml.XmlFeatureModelReader;

/*
 * This class is an abstraction of feature models as represented in the Neo4j
 * database.
 */

public class NeoFM extends NeoEntity
{

	final static protected GraphNavigator	nav				= new GraphNavigator( ExternalResourcesRepository.graphDb );
	final static protected GraphEditor		editor			= new GraphEditor( ExternalResourcesRepository.graphDb );

	private String							conf_path		= "";
	private FeatureModel					_featureide_fm	= null;

	protected NeoFM(Node n)
	{
		super( n );
	}

	/**
	 * Instantiate a new FM with "name" as a name and all pairs of Strings as
	 * attribute. This creates a new FM in the Neo4J database.
	 * 
	 * @param name
	 *            name of the FM
	 * @param properties
	 *            attributes (key, value)
	 * @return a NeoFM object.
	 */
	public static NeoFM createNew(String name, Map<String, String> properties)
	{
		properties.put(Attributes.TYPE , Types.FEATURE_MODEL );
		Node n = editor.createNode( name , properties );
		return new NeoFM( n );
	}

	/**
	 * Retrieve the FM with id "neo4jid"
	 * 
	 * @param neo4jId
	 *            the unique Neo4j identifier of a Feature Model node
	 * @return a well constructed NeoFM object.
	 */
	public static NeoFM getFM(long neo4jId)
	{
		Node n = nav.getNodeById( neo4jId );
		return new NeoFM( n );
	}

	/**
	 * Obtain a feature model with name "name". If it already exists it is
	 * recovered form the database, if not, one is created.
	 * 
	 * @param name
	 *            name of the FM to retrieve / create
	 * @return a well formed NeoFM
	 */
	public static NeoFM getFM(String name)
	{
		Node featureModel = nav.getNodeByType( name , Types.FEATURE_MODEL );

		if ( featureModel == null ) // this is a new model:
			featureModel = editor.createNode( name , editor.getFeatureModelProperties() );

		NeoFM fm = new NeoFM( featureModel );
		return fm;
	}

	/**
	 * Get the featureIDE view of the model.
	 * 
	 * @return the FeatureIDE version of this model (READ ONLY!)
	 */
	public FeatureModel get_featureide_fm()
	{
		return _featureide_fm;
	}

	/**
	 * Sets the FeatureIDE view of this model.
	 * 
	 * @param feature_model
	 */
	public void set_featureide_fm(FeatureModel feature_model)
	{
		this._featureide_fm = feature_model;
	}

	/**
	 * Build a feature model from a FeatureIDE project folder (model and
	 * configurations)
	 * 
	 * @param prjPath
	 *            path to the FeatureIDE project folder
	 * @return a well formed NeoFM object, and its representation in the
	 *         database.
	 */
	public static NeoFM getFMFromFeatureIDE(String prjPath)
	{
		String model_path = "";
		String model_name = "";
		String conf_path = "";

		if ( !prjPath.endsWith( "/" ) )
			prjPath += "/";

		File local = new File( prjPath );
		String[] content = local.list();

		for ( String s : content )
		{
			if ( s.endsWith( ".xml" ) )
			{
				model_path = prjPath + s;
				model_name = s.substring( 0 , s.lastIndexOf( "." ) );
			}
			if ( s.startsWith( "conf" ) && new File( prjPath + s ).isDirectory() )
				conf_path = prjPath + s;
		}

		FeatureModel model = new FeatureModel();
		XmlFeatureModelReader reader = new XmlFeatureModelReader( model );

		try
		{
			reader.readFromFile( new File( model_path ) );
		}
		catch ( UnsupportedModelException e1 )
		{
			e1.printStackTrace();
		}
		catch ( FileNotFoundException e )
		{
			e.printStackTrace();
		}

		NeoFM fm = NeoFM.getFM( model_name );

		fm.set_featureide_fm( model );
		fm.conf_path = conf_path;

		return fm;
	}

	/**
	 * Deletes all features, configurations and composition rules of this model
	 * in the database. This should be done before importing a new FeatureIDE
	 * folder.
	 */
	public void cleanup_model()
	{
		if ( getNode() == null )
			throw new IllegalArgumentException(
					"Cannot clean up Neo4j feature model without a reference to its Neo4j FM node." );

		List<Node> existing_confs = nav.getPointingNodes( getNode() , RelTypes.IS_A );
		List<Long> cross_tree_rules = nav.getCrossTreeConstraintIds( getNode() );

		List<Long> features = nav.getAllNonSharedFeatures( getNode() );
		for ( Node n : existing_confs )
			if ( !features.contains( n.getId() ) )
				features.add( n.getId() );

		for ( Long n : cross_tree_rules )
			if ( !features.contains( n ) )
				features.add( n );

		editor.deleteNodes( features );

	}

	/**
	 * Updates a FM with its FeatureIDE view. imports features, composition
	 * rules and configurations.
	 * 
	 */
	public void syncWithFeatureIDEModel()
	{
		if ( _featureide_fm == null )
			throw new IllegalArgumentException("cannot update Neo4j model without FeatureIDE file references. Set FeatureIDE model first." );
		if ( getNode() == null )
			throw new IllegalArgumentException("cannot update Neo4j model without a Neo4j Node reference. Set Neo4j FM node first." );

		importFeatures( _featureide_fm.getRoot() , getNode() );
		importCompositionRules( _featureide_fm , getNode() );
		importFeatureIDEConfigurations();
	}

	private void importFeatures(Feature feat, Node parent)
	{

		Node neo4jfeature = nav.getNodeByType( feat.getName() , Types.FEATURE );
		if ( neo4jfeature == null )
			neo4jfeature = editor.createNode( feat.getName() , editor.getFeatureProperties() );

		String rel = "";
		if ( !feat.isRoot() )
			rel = getRelTypeForFeature( feat );

		Map<String, String> relProps = new HashMap<String, String>();
		relProps.put( Attributes.CHOICE_TYPE , rel );
		editor.connectN1ToN2( parent , neo4jfeature , RelTypes.CONTAINS , relProps );

		List<Feature> subs = feat.getChildren();
		for ( Feature sub : subs )
		{
			importFeatures( sub , neo4jfeature );
		}
	}

	private void importCompositionRules(FeatureModel model, Node featureModel)
	{
		List<de.ovgu.featureide.fm.core.Constraint> cs = model.getConstraints();
		for ( de.ovgu.featureide.fm.core.Constraint c : cs )
		{
			String constraint = c.getNode().toString();
			Node rule = editor.createNode( c.toString() , editor.getCompositionRuleProperties() );
			Map<String, String> attrs = new HashMap<String, String>();
			attrs.put( Attributes.RULE , constraint );
			editor.connectN1ToN2( rule , featureModel , RelTypes.CONSTRAINS , attrs );
		}
	}

	private void importFeatureIDEConfigurations()
	{
		if(conf_path == null || conf_path.length() == 0 )
			return;
		
		File conf_folder = new File( conf_path );
		String[] configurations = conf_folder.list();

		try
		{
			for ( String conf : configurations )
			{
				if ( conf.startsWith( "." ) )
					continue;

				List<String> featureNames = new ArrayList<String>();
				File conf_desc = new File( conf_path + "/" + conf );
				//System.out.println( "now importing from " + conf_desc.getAbsolutePath());
				BufferedReader reader = new BufferedReader( new FileReader( conf_desc ) );
				String line = "";
				while ( ( line = reader.readLine() ) != null )
				{
					while(line.startsWith( "\"" ) && !line.endsWith( "\"" ))
					{
						String partial = reader.readLine();
						if(null != partial)
							line += partial.trim() + " ";
						else
							break;
					}
					if(line.length() != 0)
						featureNames.add( line );
				}
				reader.close();

				createNeo4JConfiguration( conf , featureNames , this.getNode() );
			}
		}
		catch ( FileNotFoundException e )
		{
			e.printStackTrace();
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}
	}

	public void createNeo4JConfiguration(String conf, List<String> featureNames, Node featureModel)
	{
		Node configuration = editor.createNode( conf , editor.getConfigurationProperties() );
		editor.connectN1ToN2( configuration , featureModel , RelTypes.IS_A , new HashMap<String, String>() );

		for ( String name : featureNames )
		{
			Node feat = nav.getNodeByType( name , Types.FEATURE );
			editor.connectN1ToN2( configuration , feat , RelTypes.CONTAINS , new HashMap<String, String>() );
		}
	}

	/**
	 * Updates the cross-design constraints of this FM in the Neo4J database.
	 * Note: Uses Neo4j information only.
	 */
	public void updateCrossdesignConstraints()
	{

		Map<String, List<String>> shared_features_by_fm = nav.getSharedFeatureNames( getNode() );

		if ( shared_features_by_fm.keySet().size() == 0 )
			return;

		Map<String, List<String>> configurations = nav.getConfigurations( getNode().getId() );

		for ( String target_fm : shared_features_by_fm.keySet() )
		{
			List<String> shared_features = shared_features_by_fm.get( target_fm );

			Node other_fm = nav.getNodeByType( target_fm , Types.FEATURE_MODEL );
			Map<String, List<String>> other_fm_configurations = nav.getConfigurations( other_fm.getId() );

			List<List<String>> new_states = NeoCrossFMConstraint.identifyConstraints( configurations , other_fm_configurations , shared_features );
			List<List<String>> reference_states = nav.getCrossFMRules( getNode().getId() , other_fm.getId() );

			List<List<String>> toRemove = getSelectionsToRemove( reference_states , new_states );
			List<List<String>> toAdd = getSelectionsToAdd( reference_states , new_states );
			
			if ( ( !toRemove.isEmpty() || !toAdd.isEmpty() ) )
			{
				editor.createCrossFmConstraints( getNode() , other_fm , toAdd );
				editor.deleteCrossFmConstraints( getNode() , other_fm , toRemove );
			}
		}
	}

	/**
	 * Given 2 lists of feature selection states, a reference one and a new one,
	 * this method returns the list of selection states that needs to be added
	 * to the reference one for both to be in sync.
	 * 
	 * @param reference_states
	 *            list of feature selection states (list of feature names)
	 * @param new_states
	 *            list of feature selection states (list of feature names)
	 * @return a list of selection states (list of feature names with qualifier
	 *         - ! ).
	 */
	public static List<List<String>> getSelectionsToAdd(List<List<String>> reference_states,
			List<List<String>> new_states)
	{
		List<List<String>> states_to_add = new ArrayList<List<String>>();
		for ( List<String> new_state : new_states )
		{
			boolean need_to_add = true;
			for ( List<String> reference_state : reference_states )
			{
				if ( new_state.containsAll( reference_state ) )
				{
					need_to_add = false;
					break;
				}
			}

			if ( need_to_add )
			{
				Iterator<List<String>> state_itr = states_to_add.iterator();
				while ( state_itr.hasNext() && need_to_add )
				{
					List<String> known_state = state_itr.next();
					if ( known_state.containsAll( new_state ) && new_state.containsAll( known_state ) )
						need_to_add = false;
				}

				if ( need_to_add )
					states_to_add.add( new_state );
			}
		}
		return states_to_add;
	}

	/**
	 * Given 2 lists of feature selection states, this method returns which
	 * selections state are in the reference list but not in the new list.
	 * 
	 * @param reference_states
	 *            list of feature selection states (list of feature names)
	 * @param new_states
	 *            list of feature selection states (list of feature names)
	 * @return a list of feature selection state.
	 */
	static public List<List<String>> getSelectionsToRemove(List<List<String>> reference_states,
			List<List<String>> new_states)
	{
		List<List<String>> states_to_remove = new ArrayList<List<String>>();
		for ( List<String> new_selection_state : reference_states )
		{
			boolean need_to_remove = true;
			for ( List<String> old_selection_state : new_states )
			{
				if ( new_selection_state.containsAll( old_selection_state ) )
				{
					need_to_remove = false;
					break;
				}
			}

			if ( need_to_remove )
			{
				states_to_remove.add( new_selection_state );
			}
		}
		return states_to_remove;
	}

	private String getRelTypeForFeature(Feature feat)
	{
		String rel = null;
		if ( feat.isAlternative() )
			rel = Constraint.ALTERNATIVE;
		if ( feat.isOr() )
			rel = Constraint.OR;
		if ( feat.isAnd() )
			if ( feat.isMandatory() )
				rel = Constraint.MANDATORY;
			else
				rel = Constraint.OPTIONAL;

		return rel;
	}

	/**
	 * Simulates configurations changes. The result is stored as "propagated"
	 * effects of the initial report.
	 * 
	 * @param report
	 *            the change report of the initial change.
	 */
	public void simulateConfChange(ConfChangeReport report)
	{
		if ( getNode() == null )
			throw new IllegalArgumentException( "You need to initialize the FM with a Neo4j node first" );

		if(report.getAddedConfigurations().size() ==  0)
		{
			simulateConfRemoval(report);
			return ;
		}
		
		Map<String, List<String>> shared_features_with_all_fms = nav.getSharedFeatureNames( getNode() );
		if ( shared_features_with_all_fms.keySet().size() == 0 )
			return; // no possible impact beyond this, only configurations will be modified as there are no CFMC.

		// get this FM latest configurations.
		Map<String, List<String>> configurations = updateConfigurationsWithChanges(nav.getConfigurations( getNode().getId() ) , report );
		if(configurations == null)
			return;
		
		// No more configurations ?
		if ( configurations.size() == 0 )
		{
			Error err = new Error( "simulated configuration changes on " + this.attributes.get( Attributes.NAME ) + " invalidated all configurations. " );
			err.error = errors.NO_MORE_CONFIG;
			report.addError( err );
		}
		
		ChangeRepository.getRepo().addConfChangeReport( report );

		// assess the effect of configuration changes on CFMCs.
		for ( String target_fm_name : shared_features_with_all_fms.keySet() )
		{
			NeoFM target_fm = NeoFM.getFM( target_fm_name );

			List<String> shared_features = shared_features_with_all_fms.get( target_fm_name );												// obtain shared features
			Map<String, List<String>> target_fm_configurations = nav.getConfigurations( target_fm.getNode().getId() );		// get other fm  configurations
			target_fm_configurations = target_fm.updateConfigurationsWithChanges( target_fm_configurations , null );			// update them according to latest changes
			if(target_fm_configurations == null)
				continue;

			List<List<String>> reference_states = nav.getCrossFMRules( getNode().getId() , target_fm.getNode().getId() );		// get CFMC rules
			reference_states = NeoCrossFMConstraint.updateStatesWithChanges( reference_states , this , target_fm );			// update them according to the latest changes

			// compute new constraints w/r to updated configurations
			List<List<String>> new_states = NeoCrossFMConstraint.identifyConstraints( configurations , target_fm_configurations , shared_features );

			// compare updated constraints and newly computed ones to identify delta.
			List<List<String>> toRemove = getSelectionsToRemove( reference_states , new_states );
			List<List<String>> toAdd = getSelectionsToAdd( reference_states , new_states );

			if ( !toRemove.isEmpty() || !toAdd.isEmpty() )
			{	 // updates are necessary
				CFMCChangeReport new_report = new CFMCChangeReport();
				new_report.setFm1( this.attributes.get( Attributes.NAME ) );
				new_report.setFm2( target_fm_name );
				new_report.setAdded_valid_selection_states( toAdd );
				new_report.setRemoved_valid_selection_states( toRemove );
				new_report.setCause( report );
				report.addPropagatedChange( new_report );
			}
		}

		// apply propagation
		for ( CFMCChangeReport r : report.getPropagation())
		{
			String target_name = r.getFm2();
			NeoFM target = NeoFM.getFM( target_name );
			target.simulateCFMCChange( r );
		}

		return;
	}

	private void  simulateConfRemoval(ConfChangeReport report)
	{
	
		
		Map<String, List<String>> shared_features_with_all_fms = nav.getSharedFeatureNames( getNode() );
		if ( shared_features_with_all_fms.keySet().size() == 0 )
			return; // no possible impact beyond this, only configurations will be modified as there are no CFMC.

		// get this FM latest configurations.
		Map<String, List<String>> configurations = updateConfigurationsWithChanges(nav.getConfigurations( getNode().getId() ) , report );
		if(configurations == null)
				return;
			
		// No more configurations ?
		if ( configurations.size() == 0 )
		{
			Error err = new Error( "simulated configuration changes on " + this.attributes.get( Attributes.NAME ) + " invalidated all configurations. " );
			err.error = errors.NO_MORE_CONFIG;
			report.addError( err );
		}
		
		
		List<NeoCrossFMConstraint> rules = nav.getCrossFMRules( this.id );
		List<NeoCrossFMConstraint>  remaining_rules = NeoCrossFMConstraint.updateStatesWithChanges( rules , this  );			// update them according to the latest changes
		List<NeoCrossFMConstraint> toRemove = new ArrayList<NeoCrossFMConstraint>();
		
		for(NeoCrossFMConstraint r : remaining_rules)
		{
			if(!r.isSupportedByConfs(configurations))
				toRemove.add(r);
		}
	
		ChangeRepository.getRepo().addConfChangeReport( report );


			if ( !toRemove.isEmpty() )
			{	 // updates are necessary
				
				for(NeoCrossFMConstraint r : toRemove)
				{
					CFMCChangeReport new_report = new CFMCChangeReport();
					
					if(this.attributes.get( Attributes.NAME ).equals( r.model_1 ))
					{
						new_report.setFm1( r.model_1);
						new_report.setFm2( r.model_2 );
					}
					else
					{
						new_report.setFm2( r.model_1);
						new_report.setFm1( r.model_2 );
					}
						
					List<String> state = r.state;
					
					boolean added = false;
					for(CFMCChangeReport prop : report.getPropagation())
					{
						if( prop.getFm1().equals( r.model_1 ) && prop.getFm2().equals(r.model_2))
						{
							List<List<String>> n = prop.getRemoved_valid_selection_states();
							n.add( state );
							added = true;
						}
					}
					
					if(!added)
					{
						List<List<String>> container = new ArrayList<List<String>>();
						container.add(state);
 						new_report.setRemoved_valid_selection_states( container);
 						new_report.setCause( report );
						report.addPropagatedChange( new_report );
					}
				}
			}

		// apply propagation
		for ( CFMCChangeReport r : report.getPropagation())
		{
			String target_name = r.getFm2();
			NeoFM target = NeoFM.getFM( target_name );
			target.simulateCFMCChange( r );
		}

		return;
	}

	/**
	 * Returns the list of available configurations, based on the change report,
	 * known changes and the list of configuration passes as input.
	 * 
	 * @param configurations
	 *            the list of configuration to filter
	 * @param new_report
	 *            the latest change report
	 * @return a Map of configuration name / configuration features, all valid
	 *         with respect to the existing changes.
	 */
	private Map<String, List<String>> updateConfigurationsWithChanges(Map<String, List<String>> configurations,
			ConfChangeReport new_report)
	{

		Map<String, List<String>> result = new HashMap<String, List<String>>();

		// initialize change list with new report information
		Map<String, List<String>> new_confs = new HashMap<String, List<String>>();;
		Map<String, List<String>> removed_confs = new HashMap<String, List<String>>();;

		// complete change list with info from repository
		List<ConfChangeReport> known_changes = ChangeRepository.getRepo().getConfChangeReportForModel(this.attributes.get( Attributes.NAME ) );
		if ( known_changes != null )
		{
			for ( ConfChangeReport r : known_changes )
			{
				if(r == new_report)
					continue;
				
				new_confs.putAll( r.getAddedConfigurations() );
				removed_confs.putAll( r.getRemovedConfigurations() );
			}
		}

		// filtering strategy: add all known and created configurations to the
		// result list, and filter it out right after.
		result.putAll( configurations );
		for ( String new_conf_name : new_confs.keySet() )
		{
			result.put( new_conf_name , new_confs.get( new_conf_name ) );
		}
		// run through each resulting config and test it against "removed"
		// configs info to identify invalid ones
		for ( String removed_conf_name : removed_confs.keySet() )
		{
			result.remove( removed_conf_name );
		}
		
		if(result.size() == 0)
		{		
			return null;
		}
		else
		{
			if(new_report == null)
				return result;
			
			List<String> rem_op_to_delete = new ArrayList<String>();
			List<String> add_op_to_delete = new ArrayList<String>();
			
			if(!new_report.getRemovedConfigurations().isEmpty())
				for ( String removed_conf_name : new_report.getRemovedConfigurations().keySet() )
				{
					List<String> key = result.remove( removed_conf_name );
					if(key == null) //nothing to remove here
						rem_op_to_delete.add( removed_conf_name );//new_report.getRemovedConfigurations().remove( removed_conf_name );
				}
			
			for(String k : rem_op_to_delete)
				new_report.getRemovedConfigurations().remove( k );
			
			if(!new_report.getAddedConfigurations().isEmpty())
				for ( String added_conf_name : new_report.getAddedConfigurations().keySet() )
				{
					if(!result.containsKey( added_conf_name ))
						result.put( added_conf_name, new_report.getAddedConfigurations().get( added_conf_name ));
					else
						add_op_to_delete.add(added_conf_name);//remove( added_conf_name );
				}
			for(String k : add_op_to_delete)
				new_report.getAddedConfigurations().remove( k );
		}

		return result;
	}

	/**
	 * Simulates an update of the CFMC and computes the impact across the MPL.
	 * The results are stored as "propagated" changes in the initial report
	 * 
	 * @param report
	 *            the description of the initial change.
	 */
	public void simulateCFMCChange(CFMCChangeReport report)
	{
		if ( getNode() == null )
			throw new IllegalArgumentException( "You need to initialize the FM with a Neo4j node first" );

		if ( !report.getFm1().equals( this.attributes.get( Attributes.NAME ) )
				&& !report.getFm2().equals( this.attributes.get( Attributes.NAME ) ) )
		{	// affecting the wrong fm here, this will do nothing. skip it.
			return;
		}

		NeoFM source = null;
		NeoFM target = null;
		if ( report.getFm1().equals( this.attributes.get( Attributes.NAME ) ) )
		{
			source = this;
			target = NeoFM.getFM( report.getFm2() );
		}
		else
		{
			target = this;
			source = NeoFM.getFM( report.getFm1() );
		}

		List<List<String>> reference_states = nav.getCrossFMRules( source.getNode().getId() , target.getNode().getId() );			// current state of the "valid selection states"
		List<List<String>> up_to_date_cfmc = NeoCrossFMConstraint.updateStatesWithChanges( reference_states , source , target );	// updated version of the "valid selection states" with all the latest changes

		//ALREADY NULL WHEN ONE FM HAS JUST BEEN EMPTIED BUT NOT THE OTHER...
		Map<String, List<String>> source_confs = source.updateConfigurationsWithChanges(nav.getConfigurations( source.id ) , null ); // get updated source configurations
		boolean source_already_empty = false;	
		if(source_confs == null)
		{
				source_already_empty = true;
		}
		
		Map<String, List<String>> target_confs = source.updateConfigurationsWithChanges( nav.getConfigurations( target.id ) , null );	 	// get updated target configurations
		boolean target_already_empty = false;
		if(target_confs == null)
		{
			target_already_empty = true;
		}

		ChangeRepository.getRepo().addCFMCReport( report );
		up_to_date_cfmc.addAll( report.getAdded_valid_selection_states() );
		List<List<String>> toRem = new ArrayList<List<String>>();
		for(List<String> state : up_to_date_cfmc)
		{
			for(List<String> rem_state : report.getRemoved_valid_selection_states())
			{
				if(rem_state.containsAll( state ) && state.containsAll( rem_state ))
					toRem.add( state );
			}
		}
		up_to_date_cfmc.removeAll( toRem );
		
		//remove configurations that do not contain any shared feature. 
		Map<String, List<String>>  source_shared_features = nav.getSharedFeatureNames( source._node );
		List<String> shared_features = source_shared_features.get(target.attributes.get( Attributes.NAME ));
		Map<String, List<String>> relevant_source_confs = removeUnsharedConfs(source_confs,shared_features);
		Map<String, List<String>> target_source_confs = removeUnsharedConfs(target_confs,shared_features);
		
		Map<String, List<String>> source_conf_to_remove = getNonCompliantConfigurations( up_to_date_cfmc , relevant_source_confs );	// check updated configurations against updated rules.
		Map<String, List<String>> target_conf_to_remove = getNonCompliantConfigurations( up_to_date_cfmc , target_source_confs );		// check updated configurations against updated rules.

		if ( ! source_already_empty  && source_confs.size() == source_conf_to_remove.size() )
		{
			Error err = new Error( " CFMC  changes between " + source.attributes.get( Attributes.NAME ) + "and " + target.attributes.get( Attributes.NAME ) 	+ " invalidated all remaining configurations of  " + source.attributes.get( Attributes.NAME )  );
			err.error = errors.COMPATIBILITY_BROKEN;
			report.addError( err );
		}

		if (  ! target_already_empty  && target_confs.size() == target_conf_to_remove.size())
		{
			Error err = new Error( " CFMC changes  between " + source.attributes.get( Attributes.NAME ) + "and "
					+ target.attributes.get( Attributes.NAME )  + " invalidated all remaining configurations of  " + target.attributes.get( Attributes.NAME ) );
			err.error = errors.COMPATIBILITY_BROKEN;
			report.addError( err );
		}

		
		if ( source_conf_to_remove.size() != 0 && !target.attributes.get( Attributes.NAME ).contains( "interface" ))
		{
			ConfChangeReport conf_changes = new ConfChangeReport();
			conf_changes.fm = source.attributes.get( Attributes.NAME );
			conf_changes.setRemovedConfigurations( source_conf_to_remove );
			conf_changes.setCause( report );
			report.addConfReportToPropagate( conf_changes );
		}

		if ( target_conf_to_remove.size() != 0 && !source.attributes.get( Attributes.NAME ).contains( "interface" ) )
		{
			ConfChangeReport conf_changes = new ConfChangeReport();
			conf_changes.fm = target.attributes.get( Attributes.NAME );
			conf_changes.setRemovedConfigurations( target_conf_to_remove );
			conf_changes.setCause( report );
			report.addConfReportToPropagate( conf_changes );
		}

		for ( ConfChangeReport r : report.getPropagation() )
		{
			NeoFM fm = NeoFM.getFM( r.fm );
			fm.simulateConfChange( r );
		}

		return;
	}

	private Map<String, List<String>> removeUnsharedConfs(Map<String, List<String>> source_confs, List<String> shared_features)
	{
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		
		if(shared_features == null || source_confs == null || source_confs.isEmpty() || shared_features.isEmpty())
			return result;
		
		for(String conf_name : source_confs.keySet())
		{
			List<String> features = source_confs.get( conf_name );
			
			for(String shared_feature : shared_features)
			{
				if(features.contains( shared_feature ))
				{
					result.put(conf_name, features);
					break;
				}
			}
		}
		
		return result;
	}

	private Map<String, List<String>> getNonCompliantConfigurations(List<List<String>> rules,
			Map<String, List<String>> configurations)
	{
		Map<String, List<String>> source_conf_to_remove = new HashMap<String, List<String>>();

		for ( String conf_name : configurations.keySet() )
		{	// each conf must be ok with at least one rule. if not, set it a
			// impacted/removed

			boolean compliant = false;

			for ( List<String> rule : rules )
			{
				if ( NeoCrossFMConstraint.isCompliant(   rule , configurations.get( conf_name )) )
				{
					compliant = true;
				}
			}

			if ( !compliant )
			{
				source_conf_to_remove.put( conf_name , configurations.get( conf_name ) );
			}
		}

		return source_conf_to_remove;
	}

	public static List<ConfChangeReport>  simulateFeatureRemoval(String featureName)
	{
		Node removed_feat = nav.getNodeByType( featureName , Types.FEATURE );
		Map<String, List<String>> impacted_configurations = nav.getConfsForFeature( removed_feat );

		List<ConfChangeReport> changes = new ArrayList<ConfChangeReport>();

		for ( String fm_name : impacted_configurations.keySet() )
		{
			Map<String, List<String>> removed_confs = new HashMap<String, List<String>>();

			List<String> conf_names = impacted_configurations.get( fm_name );
			for ( String conf_name : conf_names )
			{
				List<String> features = new ArrayList<String>();

				Node conf = nav.getNodeByType( conf_name , Types.CONFIGURATION );
				List<Node> feats = nav.getPointedNodes( conf , RelTypes.CONTAINS );
				for ( Node n : feats )
				{
					String feat_name = nav.getAttributes( n ).get( Attributes.NAME );
					if ( !features.contains( feat_name ) )
						features.add( feat_name );
				}

				removed_confs.put( conf_name , features );
			}

			ConfChangeReport report = new ConfChangeReport();
			report.fm = fm_name;
			report.setRemovedConfigurations( removed_confs );
			changes.add( report );
			
			ChangeRepository.getRepo().addConfChangeReport( report );
		}
		
		for ( ConfChangeReport r : changes )
		{
			NeoFM impacted_fm = NeoFM.getFM( r.fm );
			impacted_fm.simulateConfChange( r );
		}
		
		return changes;
	}
}
