package neo4jfm;

import neo4jfm.ui.Neo4JFM_Viewer;
import neo4jfm.utils.GraphEditor;

public class Main
{
	
    public static void main( final String[] args ) throws Exception
    {
    	GraphEditor.resetAll(ExternalResourcesRepository.getDbPath());
        
    	ExternalResourcesRepository.createDb();
    	
    	Neo4JFM_Viewer.run();
    }
    

}