package neo4jfm.utils;

import neo4jfm.domain.Model.RelTypes;

import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.graphdb.event.TransactionEventHandler;



/**
 * This class is the dispatch class for events occurring in the database.
 * 
 * This replaces what would be implemented as triggers in MySQL database.
 *
 */
public class EventHandler implements TransactionEventHandler<Object>
{

	@Override
	public void afterCommit(TransactionData arg0, Object arg1)
	{
		
		
		
	}

	@Override
	public void afterRollback(TransactionData arg0, Object arg1)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object beforeCommit(TransactionData data) throws Exception
	{
		Iterable<Relationship> rels = data.createdRelationships();
		
		for(Relationship rel : rels)
		{
			if(rel.isType( RelTypes.CONTAINS ))
			{	//an element was added to a design or an interface - we need to check which design-interface need to be updated
				
			}
		}
		return null;
	}

}
