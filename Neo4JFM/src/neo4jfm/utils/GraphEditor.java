package neo4jfm.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.domain.Model;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

import org.neo4j.kernel.impl.util.StringLogger;

public class GraphEditor {

	  static 	GraphDatabaseService    database = null;
	  static 	ExecutionEngine			queryEgine	= null;
	  static	GraphNavigator 			nav = null;
	
	public GraphEditor(GraphDatabaseService db)
	{
		database = db;
		queryEgine = new ExecutionEngine( db , StringLogger.DEV_NULL );
		nav = new GraphNavigator( database );

	}
	
	public void reset()
	{
		nav = null;
	}
	
	/**
	 * Creates a new node with a set of properties. 
	 * @param name name of the node (property)
	 * @param properties <key, value> map of properties to give to the node.
	 * @return the ID of the created node.
	 * 
	 * Note: this is done in a single transaction context.
	 */
	public Node createNode(String name, Map<String,String> properties)
	{
		Node nodeId = null;
		Transaction tx = database.beginTx();
		 try 
		 {
			 
			 Node newNode = database.createNode();
			 nodeId = newNode;
			 
			 newNode.setProperty(Model.Attributes.NAME, name);
			 for(String key : properties.keySet())
			 {
				 newNode.setProperty(key, properties.get(key));
			 }
		     tx.success();

		 }
		 catch(Exception e)
		 {
			 tx.failure();
			 tx.close();
		 }
		
		 tx.close();
		 
		 return nodeId;
	}
	
	/**
	 * Reset the database entirely (removes everything)
	 * 
	 * @param db_path path to the database file
	 */
    public static void resetAll(String db_path)
    {
        try
        {
        	if(db_path == null)
        		return;
        	
        	File dbFolder = new File(db_path);
        	if(dbFolder.exists())
        		delete(new File(db_path));
        }
        catch ( IOException e )
        {
            throw new RuntimeException( e );
        }
    }
    
	private static void delete(File f) throws IOException {
		  if (f.isDirectory()) {
		    for (File c : f.listFiles())
		      delete(c);
		  }
		  if (!f.delete())
		    throw new FileNotFoundException("Failed to delete file: " + f);
		}
    
    
    public void connectN1ToN2(Node n1, Node n2, RelTypes type, Map<String,String> properties)
    {
		Transaction tx = database.beginTx();
		 try 
		 {
			 Relationship r = n1.createRelationshipTo( n2 , type );
			 for(String key : properties.keySet())
			 {
				r.setProperty(key, properties.get(key));
			 }
		     tx.success();

		 }
		 catch(Exception e)
		 {
			 System.err.println( "error: " + e.getMessage() );
			 tx.failure();
			 tx.close();
		 }
		
		 tx.close();
    }

	public void deleteNodes(List<Long> feature_ids)
	{
		
		if(feature_ids.size() == 0 )
			return;
		
		String id_list = "[";
		for(Long id : feature_ids)
		{
			id_list+="\'"+id+"\',";
		}
		
		id_list = id_list.substring( 0 , id_list.length() -1  );
		id_list+="]";
		
		Transaction tx = database.beginTx();
		 try 
		 {
				String query = "Match n where \n" +
						"ID(n) in " + id_list +" \n" +
						"optional match (n)-[r]-() \n" +
						"delete n,r;";
				ExecutionResult result = queryEgine.execute( query );
				System.out.println( result.dumpToString() );
				
				tx.success();

		 }
		 catch(Exception e)
		 {	
			 System.err.println( e.getMessage() );
			 tx.failure();
			 tx.close();
		 }
		
		 tx.close();
	}
	
	
	public Map<String,String> getFeatureModelProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.TYPE, Types.FEATURE_MODEL);
		return props;
	}
	
	public Map<String,String> getFeatureProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.TYPE, Types.FEATURE);
		return props;
	}
	
	public Map<String,String> getConfigurationProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.TYPE, Types.CONFIGURATION);
		return props;
	}
	
	public Map<String,String> getCompositionRuleProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.TYPE, Types.COMPOSITION_RULE);
		return props;
	}
	
	public Map<String,String> getCrossFMRuleProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.TYPE, Types.CROSS_MODEL_CONSTRAINT);
		return props;
	}
	
	
	
	
	// old stuff.
	
	public Map<String,String> getInterfaceProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.INTERFACE);
		props.put(Attributes.TYPE, Types.FEATURE_MODEL);
		return props;
	}
	
	public Map<String,String> getProductProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PRODUCT);
		props.put(Attributes.TYPE, "");
		return props;
	}
	
	public Map<String,String> getProductUsageProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PRODUCT_USAGE);
		props.put(Attributes.TYPE, "");
		return props;
	}
	
	public Map<String,String> getInterfaceUsageProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.INTERFACE_USAGE);
		props.put(Attributes.TYPE, Types.CONFIGURATION);
		return props;
	}
	
	public Map<String,String> getFMFolderProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.FOLDER);
		props.put(Attributes.TYPE, Types.FEATURE);
		return props;
	}
	
	public Map<String,String> getInterfaceParameterProperties(String parameter_name, String parameter_value)
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PARAMETER);
		props.put(Attributes.TYPE, Types.FEATURE);
		props.put(Attributes.PARAM_NAME,parameter_name);
		props.put(Attributes.PARAM_VALUE, parameter_value);
		return props;
	}

	public void createCrossFmConstraints(Node featureModel, Node otherFM, List<List<String>> valid_selections)
	{
		for(List<String> selection : valid_selections)
		{
			String attr = "";
			for(String feat : selection )
			{
				attr+=feat+" || ";
			}

			Map<String,String> props = getCrossFMRuleProperties();
			props.put( Attributes.RULE , attr );

			Node constraint = createNode("test", props);
			connectN1ToN2( constraint , featureModel , RelTypes.CONSTRAINS , new HashMap<String,String>() );
			connectN1ToN2( constraint , otherFM , RelTypes.CONSTRAINS , new HashMap<String,String>() );
		}

	}

	public void deleteCrossFmConstraints(Node feature_model, Node other_fm, List<List<String>> states_to_remove)
	{
		List<Node> rules = nav.getCrossFMRules( feature_model , other_fm );
		List<Long> node_id_to_delete= new ArrayList<Long>();
		
		for(Node n : rules)
		{
			Map<String,String> n_attrs = nav.getAttributes( n );
			String expr = n_attrs.get( "rule" );
			String[] selection = expr.split( "||" );
			List<String> selection_state = new ArrayList<String>();
			for(String s : selection)
				selection_state.add( s );
			
			for(List<String> to_remove : states_to_remove)
				if(to_remove.containsAll( selection_state ))
				{
					node_id_to_delete.add( n.getId() );
					break;
				}
		}
		
		deleteNodes( node_id_to_delete );
		
	}
	
}
