package neo4jfm.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import neo4jfm.domain.Model;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.NeoCrossFMConstraint;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.impl.util.StringLogger;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.Monitor;

@SuppressWarnings("deprecation")
public class GraphNavigator
{

	 static  GraphDatabaseService	db			= null;
	 static ExecutionEngine		queryEngine	= null;

	public GraphNavigator(GraphDatabaseService database)
	{
		db = database;
		queryEngine = new ExecutionEngine( db , StringLogger.DEV_NULL );
	}
	
	

	public Node getProductRootNode()
	{
		return getNodeByDomain( "\"External product\"" , Domains.PRODUCT );
	}

	public Node getDesignNode(String name)
	{
		return getNodeByDomain( name , Domains.DESIGN );
	}

	public Node getInterfaceRootNode()
	{
		return getNodeByDomain( "External interfaces" , Domains.INTERFACE );
	}

	/**
	 * Retrieve a node by its name and dome attribute value.
	 * 
	 * @param name
	 * @param domain
	 * @return
	 */
	public Node getNodeByDomain(String name, String domain)
	{
		Transaction tx = db.beginTx();
		Node root = null;
		try
		{
			String query = "MATCH (n) WHERE n.name = \"" + name + "\" and n.domain = \"" + domain + "\" RETURN n";
			ExecutionResult result = queryEngine.execute( query );
			Iterator<Node> n_column = result.columnAs( "n" );
			while ( n_column.hasNext() )
			{
				root = n_column.next();
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return root;
	}

	public Node getNodeByType(String name, String type)
	{

		if ( name.startsWith( "\"" ) && name.endsWith( "\"" ) )
			name = name.substring( 1 , name.length() - 1 );

		Transaction tx = db.beginTx();
		Node root = null;
		try
		{
			String query = "MATCH (n) WHERE n.name = \"" + name
					+ "\" and n.type = \"" + type + "\" RETURN n";
			ExecutionResult result = queryEngine.execute( query );

			Iterator<Node> n_column = result.columnAs( "n" );
			while ( n_column.hasNext() )
			{
				root = n_column.next();
			}

			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve node " + name + " of type " + type );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return root;
	}

	/**
	 * Returns configurations using a given feature. 
	 * 
	 * @param feat
	 * @return a Map <feature model name, list of configuration names>
	 */
	public Map<String, List<String>> getConfsForFeature(Node feat)
	{
		Map<String, List<String>> results = new HashMap<String, List<String>>(); // design name, config names.
		Transaction tx = db.beginTx();
		try
		{
			String query = "Match feat, conf, fm where \n" +
					" ID(feat) = " + feat.getId()
					+ " and conf.type =\"configuration\" and fm.type=\"feature model\" \n" +
					" and feat<-[:CONTAINS]-conf-[:IS_A]->fm \n" +
					"return fm.name, conf.name";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{
				String fmName = (String) row.get( "fm.name" );
				String conf = (String) row.get( "conf.name" );

				List<String> confs = results.get( fmName );
				if ( confs == null )
					confs = new ArrayList<String>();

				if ( !confs.contains( conf ) )
					confs.add( conf );

				results.put( fmName , confs );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve configurations and FM containing feature " );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return results;
	}

	public Map<String, String> getAttributeValues(Node n, List<String> attributes)
	{
		Map<String, String> results = new HashMap<String, String>();
		Transaction tx = db.beginTx();

		try
		{
			Iterator<String> itr = attributes.iterator();
			while ( itr.hasNext() )
			{
				String attr = itr.next();
				results.put( attr , (String) n.getProperty( attr ) );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve node properties" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();

		return results;
	}

	/**
	 * Get the nodes pointed by "node" through a relationship of type "relationship". 
	 * It's a 1 level graph jump, no more. 
	 * 
	 * @param node the node to start from
	 * @param relationship the relationship type
	 * @return a list of Neo4J node
	 * 
	 */
	public List<Node> getPointedNodes(Node node, RelTypes relationship)
	{
		return getFirstLevelNodes( node , relationship , Direction.OUTGOING );
	}

	public List<Node> getPointingNodes(Node node, RelTypes relationship)
	{
		return getFirstLevelNodes( node , relationship , Direction.INCOMING );
	}

	/**
	 * Get a node object from the Neo4j DB given its ID. 
	 * @param id the id of the node to retrieve
	 * @return the node.
	 */
	public Node getNodeById(long id)
	{
		Transaction tx = db.beginTx();
		Node n = null;

		try
		{
			n = db.getNodeById( id );
			tx.success();
		}
		catch ( Exception e )
		{
			System.out.println( "failed to obtain a graph node by id" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return n;

	}

	private ArrayList<Node> getFirstLevelNodes(Node node, RelTypes relationship, Direction dir)
	{
		Transaction tx = db.beginTx();
		ArrayList<Node> results = new ArrayList<Node>();
		try
		{

			TraversalDescription tr = Traversal.description().breadthFirst()
					.relationships( relationship , dir )
					.evaluator( Evaluators.excludeStartPosition() )
					.evaluator( Evaluators.toDepth( 1 ) );
			Traverser trav = tr.traverse( node );

			Iterator<Node> itr = trav.nodes().iterator();

			while ( itr.hasNext() )
			{
				Node n = itr.next();
				results.add( n );
			}

			tx.success();

		}
		catch ( Exception e )
		{
			tx.failure();
		}

		tx.close();
		return results;
	}

	/**
	 * Checks if a node a at least one relationship of a given type. 
	 * 
	 * @param n the node
	 * @param rel the relationship type
	 * @param d the direction (incoming/outgoing) - note: use the Neo4J "Direction" object for this.
	 * @return true if node has at least one rel of this type, false otherwise.
	 */
	public boolean hasRel(Node n, RelTypes rel, Direction d)
	{
		Transaction tx = db.beginTx();
		boolean b = false;
		try
		{
			b = n.hasRelationship( rel , d );
			tx.success();
		}
		catch ( Exception e )
		{
			tx.failure();
		}

		tx.close();
		return b;
	}

	/**
	 * Returns all attributes of node "n"
	 * 
	 * @param n the node
	 * @return a Map <attribute name, attribute value>
	 */
	public Map<String, String> getAttributes(Node n)
	{

		Transaction tx = db.beginTx();
		Map<String, String> results = null;

		try
		{
			results = new HashMap<String, String>();
			Iterable<String> collection = n.getPropertyKeys();

			for ( String s : collection )
			{
				results.put( s , (String) n.getProperty( s ) );
			}

			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "Failed to recover node attributes and values" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return results;
	}

	/**
	 * Return all cross-tree constraints of a FM. 
	 *  
	 * @param fm the feature model
	 * @return a list of feature boolean expression representing the composition rules.
	 */
	public List<Long> getCrossTreeConstraintIds(Node fm)
	{
		List<Long> results = new ArrayList<Long>();

		Transaction tx = db.beginTx();
		try
		{
			long fm_id = fm.getId();
			String query = "Match rule, model where ID(model)=" + fm_id + " and rule.type=\"" + Types.COMPOSITION_RULE
					+ "\" and rule-[:" + RelTypes.CONSTRAINS + "]->model return rule;";

			ExecutionResult result = queryEngine.execute( query );
			Iterator<Node> n_column = result.columnAs( "rule" );
			while ( n_column.hasNext() )
			{
				Node rule = n_column.next();
				results.add( rule.getId() );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return results;
	}

	/**
	 * Return all cross fm rules linking fm1 and fm2
	 * @param fm1 a feature model
	 * @param fm2 another feature model
	 * @return a list of cross-model rule Nodes.  
	 */
	public List<Node> getCrossFMRules(Node fm1, Node fm2)
	{
		List<Node> results = new ArrayList<Node>();
		Transaction tx = db.beginTx();
		try
		{
			String query = "Match n,m,p where ( n-[:CONSTRAINS*]->m ) and ( n-[:CONSTRAINS*]->p )and ID(m)=" + fm1.getId() + " and ID(p)=" + fm2.getId() + " Return distinct n";
			ExecutionResult result = queryEngine.execute( query );

			Iterator<Node> n_column = result.columnAs( "n" );
			while ( n_column.hasNext() )
			{
				Node n = n_column.next();
				results.add( n );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return results;
	}

	/**
	 * return cross-model rules between 
	 * @param fm1_id
	 * @param fm2_id
	 * @return
	 */
	public List<List<String>> getCrossFMRules(long fm1_id, long fm2_id)
	{
		List<List<String>> results = new ArrayList<List<String>>();
		Transaction tx = db.beginTx();
		try
		{
			String query = "Match n,m,p where n.type=\""+Types.CROSS_MODEL_CONSTRAINT+"\"   and  ( n-[:CONSTRAINS]->m ) and ( n-[:CONSTRAINS]->p )and ID(m)=" + fm1_id + " and ID(p)=" + fm2_id + " Return distinct n";
			ExecutionResult result = queryEngine.execute( query );

			Iterator<Node> n_column = result.columnAs( "n" );
			while ( n_column.hasNext() )
			{
				Node n = n_column.next();
				String r = (String) n.getProperty( "rule" );
				List<String> rule = new ArrayList<String>();
				String[] values = r.split( "\\|\\|" );
				for ( String s : values )
				{
					String tmp = s;
					if ( tmp.trim().length() > 0 )
						rule.add( s.trim() );
				}

				results.add( rule );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "Failed to retrieve cross-fm composition rules!" );
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return results;
	}

	
	/**
	 * return cross-model rules between 
	 * @param fm1_id
	 * @param fm2_id
	 * @return
	 */
	public List<NeoCrossFMConstraint> getCrossFMRules(long fm1_id)
	{
		List<NeoCrossFMConstraint> results = new ArrayList<NeoCrossFMConstraint>();
		Transaction tx = db.beginTx();
		try
		{
			String query = "Match n,m,p where n.type=\""+Types.CROSS_MODEL_CONSTRAINT+"\"  and ( n-[:CONSTRAINS]->m ) and ( n-[:CONSTRAINS]->p )and ID(m)=" + fm1_id + " and not ( ID(p) = ID(m) )  Return distinct n";
			ExecutionResult result = queryEngine.execute( query );

			Iterator<Node> n_column = result.columnAs( "n" );
			while ( n_column.hasNext() )
			{
				Node n = n_column.next();
				NeoCrossFMConstraint cfmc = new NeoCrossFMConstraint(n);
				
				results.add( cfmc );
//				String r = (String) n.getProperty( "rule" );
//				List<String> rule = new ArrayList<String>();
//				String[] values = r.split( "\\|\\|" );
//				for ( String s : values )
//				{
//					String tmp = s;
//					if ( tmp.trim().length() > 0 )
//						rule.add( s.trim() );
//				}
//
//				results.add( rule );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "Failed to retrieve cross-fm composition rules!" );
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return results;
	}

	
	public Iterable<Relationship> getRelationships(Node n, Direction dir, RelTypes type)
	{
		Iterable<Relationship> rels = new ArrayList<Relationship>();

		Transaction tx = db.beginTx();
		try
		{
			rels = n.getRelationships( dir , type );
		}
		catch ( Exception e )
		{
			tx.failure();
		}
		tx.success();
		tx.close();

		return rels;
	}

	public String getRelationshipAttributeValue(Relationship r, String attributeName)
	{
		String value = "";
		Transaction tx = db.beginTx();

		try
		{

			value = (String) r.getProperty( attributeName );

			tx.success();
		}
		catch ( Exception e )
		{
			tx.failure();
		}

		tx.close();
		return value;
	}

	public Long getEndNodeId(Relationship r)
	{
		Long id = (long) -1;
		Transaction tx = db.beginTx();

		try
		{

			id = r.getEndNode().getId();

			tx.success();
		}
		catch ( Exception e )
		{
			tx.failure();
		}

		tx.close();
		return id;
	}

	public List<String> getComponentNamesFromDesign(Node design)
	{
		List<String> results = new ArrayList<String>();

		Transaction tx = db.beginTx();
		try
		{
			String query = "Match design,component where ( design-[:CONTAINS*]->component ) and ID(design)="
					+ design.getId() + " and component.type=\"" + Domains.COMPONENT + "\" "
					+ " and design.type=\"" + Domains.DESIGN + "\" Return distinct component";
			ExecutionResult result = queryEngine.execute( query );

			Iterator<Node> n_column = result.columnAs( "component" );
			while ( n_column.hasNext() )
			{
				Node n = n_column.next();
				results.add( (String) n.getProperty( "name" ) );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}
		tx.close();
		return results;

	}

	public Map<String, List<String>> getSharedComponentNames(long designId)
	{
		Node design = this.getNodeById( designId );
		return getSharedComponentNames( design );
	}

	public Map<String, List<String>> getSharedFeatureNames(long designId)
	{
		Node design = this.getNodeById( designId );
		return getSharedFeatureNames( design );
	}
	
	/**
	 * Returns the list of features that this FM shares with others. 
	 * 
	 * @param feature_model
	 * @return a Map indexed by feature model name. Each entry is a list of name of features.
	 */
	public Map<String, List<String>> getSharedFeatureNames(Node feature_model)
	{
		Map<String, List<String>> results = new HashMap<String, List<String>>();

		Transaction tx = db.beginTx();
		try
		{
			long id = feature_model.getId();
			String feature_model_name = (String) feature_model.getProperty( "name" );

			String query = "Match origin, target,intermediate  \n" +
					" where origin.type = \"" + Types.FEATURE_MODEL + "\" and target.type=\"" + Types.FEATURE_MODEL
					+ "\" and intermediate.type = \"" + Types.FEATURE + "\"" +
					" and ID(origin)=" + id +
					" and (origin-[:CONTAINS*]->intermediate<-[:CONTAINS*]-target) \n" +
					" and not intermediate.name in ['7.5', '8.1', '8.2' ] \n " + 
					" return distinct ID(intermediate), intermediate.name, ID(target), target.name";
			
			//Monitor m = MeasureFactory.start("get shared features: query exectuion");
			ExecutionResult result = queryEngine.execute( query );
			tx.success();
			//m.stop();
			
		//	System.out.println("retrieved " + result.columnAs( "target.name" ). + " shared features");
			
			Monitor m2 = MeasureFactory.start("get shared features: extract results");
			for ( Map<String, Object> row : result )
			{
				String featureName = (String) row.get( "intermediate.name" );
				String otherFM = (String) row.get( "target.name" );

				if ( otherFM != null && otherFM.equals( feature_model_name ) )
					continue;

				if ( results.get( otherFM ) != null )
				{
					List<String> sharedFeatures = results.get( otherFM );
					if ( !sharedFeatures.contains( featureName ) )
						sharedFeatures.add( featureName );
				}
				else
				{
					List<String> sharedOnes = new ArrayList<String>();
					sharedOnes.add( featureName );
					results.put( otherFM , sharedOnes );
				}
			}
			m2.stop();
			tx.close();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
			tx.close();
		}
		return results;
	}

	public Map<String, List<String>> getSharedComponentNames(Node design)
	{
		Map<String, List<String>> results = new HashMap<String, List<String>>();

		Transaction tx = db.beginTx();
		try
		{
			long id = design.getId();
			String query = "Match origin, target,intermediate  \n" +
					"where origin.type = \"" + Domains.DESIGN + "\" and target.type=\"" + Domains.DESIGN
					+ "\" and intermediate.type = \"" + Domains.COMPONENT + "\"" +
					"and ID(origin)=" + id +
					"and not (target<-[:CONTAINS]-()) and not (origin<-[:CONTAINS]-()) " +
					"and (origin-[:CONTAINS*]->intermediate<-[:CONTAINS*]-target) " +
					"return distinct ID(intermediate),intermediate.name, ID(target), target.name";
			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{

				String sharedFeatureName = (String) row.get( "intermediate.name" );
				String otherDesign = (String) row.get( "target.name" );

				if ( results.get( otherDesign ) != null )
				{
					List<String> sharedFeatures = results.get( otherDesign );
					if ( !sharedFeatures.contains( sharedFeatureName ) )
						sharedFeatures.add( sharedFeatureName );
				}
				else
				{
					List<String> sharedOnes = new ArrayList<String>();
					sharedOnes.add( sharedFeatureName );
					results.put( otherDesign , sharedOnes );
				}
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return results;
	}

	public List<String> getDesignNamesForComponent(Node component)
	{
		List<String> designNames = new ArrayList<String>();

		Transaction tx = db.beginTx();
		try
		{
			long id = component.getId();
			String query = "Match comp,design \n" +
					"Where ID(comp)=" + id + " and design-[:CONTAINS*]->comp\n" +
					"and not (design<-[:CONTAINS]-()) and design.type=\"" + Domains.DESIGN + "\"\n" +
					"return design.name";
			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{
				String designName = (String) row.get( "design.name" );
				if ( !designNames.contains( designName ) )
					designNames.add( designName );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();

		return designNames;

	}

	public List<String> getInterfaceImplementation(Node itf)
	{
		List<String> designNames = new ArrayList<String>();

		Transaction tx = db.beginTx();
		try
		{
			long id = itf.getId();

			String query = "Match itf, itf_parent,design where ID(itf)=" + id + " " +
					"and design.type=\"" + Domains.DESIGN + "\"" +
					"and (itf<-[:IMPLEMENTS]-design )" + 
					"return design.name";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{
				String designName = (String) row.get( "design.name" );
				if ( !designNames.contains( designName ) )
					designNames.add( designName );
			}
			tx.success();

		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();

		return designNames;
	}

	public List<String> getParameterImplementation(Node param)
	{
		List<String> implInfo = new ArrayList<String>();
		Transaction tx = db.beginTx();
		try
		{
			long id = param.getId();
			String query = "Match param, impl,design " +
					"where id(param)=" + id + " " +
					"and impl.type=\"" + Domains.IMPLEMENTATION + "\" " +
					"and design.type=\"" + Domains.DESIGN + "\" " +
					"and param<-[:" + RelTypes.CONTAINS + "]-impl-[:" + RelTypes.IS_A + "]->design " +
					"return distinct design.name,  impl.name";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{
				String designName = (String) row.get( "design.name" );
				String implName = (String) row.get( "impl.name" );
				implInfo.add( designName + " with implementation " + implName );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();

		return implInfo;
	}

	/**
	 * returns all configurations of a given design The map contains the name of
	 * the configuration and the associated list contains the names of the
	 * selected features.
	 * 
	 * @param design_id
	 * @return
	 */
	public Map<String, List<String>> getDesignImplementations(long design_id)
	{

		Map<String, List<String>> confs = new HashMap<String, List<String>>();
		Transaction tx = db.beginTx();
		try
		{
			String query = "Match n,d,c where \n" +
					"c.type=\"" + Model.Domains.COMPONENT + "\"and d.domain=\"" + Model.Domains.DESIGN
					+ "\" and n.type=\"" + Model.Domains.IMPLEMENTATION + "\" \n" +
					"and n-[:" + Model.RelTypes.IS_A + "]->d and ID(d)=" + design_id + "\n" +
					"and n-[:" + Model.RelTypes.CONTAINS + "]->c\n" +
					"return n.name,c.name;";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{

				String implName = (String) row.get( "n.name" );
				String comp = (String) row.get( "c.name" );

				if ( confs.containsKey( implName ) )
				{
					List<String> feats = confs.get( implName );
					if ( !feats.contains( comp ) )
						feats.add( comp );
				}
				else
				{
					List<String> feats = new ArrayList<String>();
					feats.add( comp );
					confs.put( implName , feats );
				}
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
		}

		tx.close();
		return confs;
	}

	/**
	 * returns all configurations of a given design The map contains the name of
	 * the configuration and the associated list contains the names of the
	 * selected features.
	 * 
	 * @param fm_id
	 * @return
	 */
	public Map<String, List<String>> getConfigurations(long fm_id)
	{

		Map<String, List<String>> confs = new HashMap<String, List<String>>();
		Transaction tx = db.beginTx();
		try
		{
//			String query = "Match conf,fm where \n" +
//					" fm.type=\"" + Model.Types.FEATURE_MODEL
//					+ "\" and conf.type=\"" + Model.Types.CONFIGURATION + "\" \n" +
//					"and conf-[:" + Model.RelTypes.IS_A + "]->fm and ID(fm)=" + fm_id + "\n" +
//					"return conf.name,feat.name;";
			
			String query = "Match conf, fm, feat where \n" +
					" fm.type=\"" + Model.Types.FEATURE_MODEL
					+ "\" and conf.type=\"" + Model.Types.CONFIGURATION + "\" \n" +
					 " and feat.type=\"" + Model.Types.FEATURE + "\" \n" +
					"and conf-[:" + Model.RelTypes.IS_A + "]->fm and ID(fm)=" + fm_id + "\n" +
					"and conf-[:" + RelTypes.CONTAINS + "]->feat "+ "\n" +
					"return conf.name, feat.name;";


			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{

				String implName = (String) row.get( "conf.name" );
				String comp = (String) row.get( "feat.name" );

				if ( confs.containsKey( implName ) )
				{
					List<String> feats = confs.get( implName );
					if ( !feats.contains( comp ) )
						feats.add( comp );
				}
				else
				{
					List<String> feats = new ArrayList<String>();
					feats.add( comp );
					confs.put( implName , feats );
				}
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
			tx.close();
		}

		tx.close();
		return confs;
	}

	public List<Long> getAllNonSharedFeatures(Node existing_model)
	{
		List<Long> ids = new ArrayList<Long>();
		Transaction tx = db.beginTx();
		try
		{
			long model_id = existing_model.getId();

			String query = "Match fm1, fm2, f where \n" +
					"f.type=\"" + Model.Types.FEATURE + "\"and fm1.type=\"" + Model.Types.FEATURE_MODEL
					+ "\"and fm2.type=\"" + Model.Types.FEATURE_MODEL + "\" \n" +
					"and fm1-[:" + Model.RelTypes.CONTAINS + "*]->f and ID(fm1)=" + model_id + "\n" +
					"and not fm2-[:" + Model.RelTypes.CONTAINS + "*]->f\n" +
					"return ID(f);";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{
				Long id = (Long) row.get( "ID(f)" );
				if ( !ids.contains( id ) )
					ids.add( id );
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
			tx.close();
		}

		tx.close();
		return ids;
	}

	public Map<String, List<String>> getConfigurationsWithSelection(long id, List<String> state)
	{
		Map<String, List<String>> confs = new HashMap<String, List<String>>();

		String valid_feature_range = "[";
		String invalid_feature_range = "[";
		for ( String s : state )
		{
			if ( s.startsWith( "!" ) )
				invalid_feature_range += "\"" + s.substring( 1 , s.length() ) + "\",";
			else
				valid_feature_range += "\"" + s + "\",";
		}

		if ( invalid_feature_range.endsWith( "," ) )
			invalid_feature_range = invalid_feature_range.substring( 0 , invalid_feature_range.length() - 1 );
		if ( valid_feature_range.endsWith( "," ) )
			valid_feature_range = valid_feature_range.substring( 0 , valid_feature_range.length() - 1 );

		invalid_feature_range += "]";
		valid_feature_range += "]";

		// TraceLog.getLogger().tick( new Exception() );

		Transaction tx = db.beginTx();
		try
		{
			String query = "Match conf,fm, feat where \n" +
					"feat.type=\"" + Model.Types.FEATURE + "\"and fm.type=\"" + Model.Types.FEATURE_MODEL
					+ "\" and conf.type=\"" + Model.Types.CONFIGURATION + "\" \n" +
					"and conf-[:" + Model.RelTypes.IS_A + "]->fm and ID(fm)=" + id + "\n" +
					"and conf-[:" + Model.RelTypes.CONTAINS + "]->feat\n" +
					" and feat.name in " + valid_feature_range + " \n" +
					" and not feat.name in " + invalid_feature_range + " \n" +
					"return conf.name,feat.name;";

			ExecutionResult result = queryEngine.execute( query );

			for ( Map<String, Object> row : result )
			{

				String implName = (String) row.get( "conf.name" );
				String comp = (String) row.get( "feat.name" );

				if ( confs.containsKey( implName ) )
				{
					List<String> feats = confs.get( implName );
					if ( !feats.contains( comp ) )
						feats.add( comp );
				}
				else
				{
					List<String> feats = new ArrayList<String>();
					feats.add( comp );
					confs.put( implName , feats );
				}
			}
			tx.success();
		}
		catch ( Exception e )
		{
			System.err.println( "failed to retrieve interface root node" );
			e.printStackTrace();
			tx.failure();
			tx.close();
		}
		tx.close();
		return confs;
	}

}
