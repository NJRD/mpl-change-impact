package neo4jfm;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class ExternalResourcesRepository {
	
	private static  String DB_PATH ; //= "/Users/Dante/Documents/workspace/Git_repositories/impact/database";
	private static  String PROJECT_PATH  = "/Users/Dante/Documents/workspace/Git_repositories/impact/input/";
	private static  String EXPORT_PATH = "/Users/Dante/Documents/workspace/Git_repositories/impact/input/";
	
	private static void loadPaths() throws IOException
	{
		Properties properties = new Properties();
		try {
		  properties.load(new FileInputStream("paths.properties"));
		  
		  DB_PATH = properties.getProperty("Neo4j");
		} catch (IOException e) {
		  System.err.println("Failed to load property file");
		  throw e;
		}
	}


	
	public static GraphDatabaseService graphDb = null;

	public static void createDb() throws IOException
	{
		loadPaths();
	    ExternalResourcesRepository.graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( getDbPath() );
	    ExternalResourcesRepository.registerShutdownHook(ExternalResourcesRepository.graphDb); 
	}
	
	public static void createDb(GraphDatabaseService db) throws IOException
	{
		loadPaths();
	    ExternalResourcesRepository.graphDb = db;
	    ExternalResourcesRepository.registerShutdownHook(ExternalResourcesRepository.graphDb); 
	}
	
	static void registerShutdownHook( final GraphDatabaseService graphDb )
	{
	    Runtime.getRuntime().addShutdownHook( new Thread()
	    {
	        @Override
	        public void run()
	        {
	        	if(graphDb != null)
	        		graphDb.shutdown();
	        }
	    } );
	}

	public static void shutDown()
	{
	    if(ExternalResourcesRepository.graphDb != null)
	    {
	    	ExternalResourcesRepository.graphDb.shutdown();
	    	ExternalResourcesRepository.graphDb = null;
	    }
	}
	
	public static String getImportPath()
	{
		return PROJECT_PATH;
	}
	
	public static String getExportPath()  
	{

		return EXPORT_PATH;
	}
	
	public static void setImportPath(String p )
	{
		PROJECT_PATH = p;
	}
	
	public static void setExportPath(String p )
	{
		
		EXPORT_PATH = p;
	}

	public static void setDbPath(String p )
	{
		DB_PATH = p;
	}
	
	
	public static String getDbPath() throws IOException 
	{
		loadPaths();
		return DB_PATH;
	}


	

}
