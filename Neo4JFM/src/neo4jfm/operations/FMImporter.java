package neo4jfm.operations;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

/**
 * wraps import operation from FeatureIDE folder to Neo4J database
 * 
 * @author Dante
 * 
 */
public class FMImporter
{

	private String	prj_path	= "";
	GraphNavigator	nav			= new GraphNavigator( ExternalResourcesRepository.graphDb );
	GraphEditor		editor		= new GraphEditor( ExternalResourcesRepository.graphDb );

	public FMImporter(String folder_path)
	{
		prj_path = folder_path;
	}

	public NeoFM importIntoNeo4J()
	{
		NeoFM fm = NeoFM.getFMFromFeatureIDE( prj_path );
		fm.cleanup_model();
		fm.syncWithFeatureIDEModel();
		fm.updateCrossdesignConstraints();
		
		return fm;
	}
}
