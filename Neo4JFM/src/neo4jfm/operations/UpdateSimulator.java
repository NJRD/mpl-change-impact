package neo4jfm.operations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Node;
import org.sat4j.specs.TimeoutException;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.utils.GraphNavigator;

import de.ovgu.featureide.fm.core.Feature;
import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.FeatureModelAnalyzer;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.Selection;

public class UpdateSimulator
{

	FeatureModel			_fm				= null;
	String					_fm_name		= "";
	List<List<String>>		_new_configs	= null;
	static GraphNavigator	nav				= new GraphNavigator( ExternalResourcesRepository.graphDb );;

	public UpdateSimulator(String modelName, FeatureModel fm, List<List<String>> configurations)
	{
		_fm = fm;
		_fm_name = modelName;
		_new_configs = configurations;
	}

	public void startSimulation()
	{
		feature_model_check();
	}

	@SuppressWarnings("deprecation")
	private void feature_model_check()
	{
		Node model_node = nav.getNodeByType( _fm_name , Types.FEATURE_MODEL );
		if ( model_node == null )
		{
			// no update here, it's a creation a new feature model.
			// OperationsLog.getLogger().addAction( "update simulation - info" ,
			// "the model does not exists. We are creating new data." );
		}
		else
		{
			FMExporter exp = new FMExporter();
			List<Long> ids = new ArrayList<Long>();
			ids.add( model_node.getId() );
			FeatureModel old_fm = exp.CreateModelWithRoots( ids , true );

			FeatureModelAnalyzer old_analyzer = old_fm.getAnalyser();
			FeatureModelAnalyzer new_analyzer = _fm.getAnalyser();

			compareFM( old_analyzer , new_analyzer );
			compareConfigurations( model_node , old_fm , _fm );

		}

	}

	private void compareConfigurations(Node old_fm_node, FeatureModel old_fm, FeatureModel _fm2)
	{
		Map<String, List<String>> old_confs = nav.getConfigurations( old_fm_node.getId() );

		if ( old_confs.size() != _new_configs.size() )
		{
			if ( old_confs.size() > _new_configs.size() )
			{
				// OperationsLog.getLogger().addAction(
				// "update simulation - info" ,
				// "Less configurations given with the new model." );
			}
			if ( old_confs.size() < _new_configs.size() )
			{
				// OperationsLog.getLogger().addAction(
				// "update simulation - info" ,
				// "More configurations than with the new model" );
			}

		}

		for ( String conf_name : old_confs.keySet() )
		{
			try
			{
				Configuration old_conf = new Configuration( _fm2 );
				for ( String feat_name : old_confs.get( conf_name ) )
				{
					old_conf.setManual( feat_name , Selection.SELECTED );
				}
			}
			catch ( Exception e )
			{
//				 OperationsLog.getLogger().addAction(
//				 "[WARNING] update simulation" ,
//				 "Old configuration cannot be re-instanciated with the new model :"+
//				 conf_name );
			}
		}
	}

	private void compareFM(FeatureModelAnalyzer reference, FeatureModelAnalyzer target)
	{
		boolean ref_valid = false;
		boolean target_valid = false;
		try
		{
			ref_valid = reference.isValid();
			target_valid = target.isValid();
		}
		catch ( TimeoutException e )
		{
			e.printStackTrace();
			return;
		}

		List<Feature> ref_dead = reference.getDeadFeatures();
		List<Feature> target_dead = target.getDeadFeatures();
		List<Feature> ref_false_opts = reference.getFalseOptionalFeatures();
		List<Feature> target_false_opts = target.getFalseOptionalFeatures();

		if ( ref_valid == target_valid )
			if ( !ref_valid )
			{
				// OperationsLog.getLogger().addAction( "[ERROR] FM update " ,
				// "the old and new FM are both invalid" );
		}

		if ( !target_valid )
		{
			// OperationsLog.getLogger().addAction( "[ERROR] FM update " ,
			// "the new FM is invalid" );
		}

		if ( !ref_dead.containsAll( target_dead ) || !target_dead.containsAll( ref_dead ) )
		{
			if ( ref_dead.size() < target_dead.size() )
			{
				// OperationsLog.getLogger().addAction( "[WARNING] FM update " ,
				// "New model is introducing additional deadfeatures.It was : "+ref_dead.size()
				// + " and now is "+ target_dead.size() );
			}
			if ( ref_dead.size() > target_dead.size() )
			{
				// OperationsLog.getLogger().addAction( "[WARNING] FM update " ,
				// "New model is reducing the number of deadfeatures. It was : "+ref_dead.size()
				// + " and now is "+ target_dead.size() );
			}
		}

		if ( !ref_false_opts.containsAll( target_false_opts ) || !target_dead.containsAll( ref_false_opts ) )
		{
			if ( ref_false_opts.size() < target_false_opts.size() )
			{
				// OperationsLog.getLogger().addAction( "[WARNING] FM update " ,
				// "New model is introducing additional false optional features. It was : "+ref_false_opts.size()
				// + " and now is "+ target_false_opts.size() );
			}
			if ( ref_false_opts.size() > target_false_opts.size() )
			{
				// OperationsLog.getLogger().addAction( "[WARNING] FM update " ,
				// "New model is reducing the number of false optional features.It was : "+ref_false_opts.size()
				// + " and now is "+ target_false_opts.size() );
			}
		}
	}
	
	public static ConfChangeReport simulateConfUpdate(NeoFM fm, String featureide_prj_path) throws IOException
	{
		Map<String,List<String>> known_confs = nav.getConfigurations( fm.getNode().getId() );
		
		File prj_folder = new File(featureide_prj_path+"/configs");
		String[] conf_files = prj_folder.list();
		Map<String,List<String>> new_confs = new HashMap<String,List<String>>();
		
		getConfsFromFeatureIDEFolder( prj_folder , conf_files , new_confs );
		ConfChangeReport r  = compareConfs(known_confs, new_confs);
		r.fm = fm.attributes.get( Attributes.NAME);
		
		fm.simulateConfChange( r );
		
		return r;
	}

	static private ConfChangeReport compareConfs(Map<String, List<String>> reference, Map<String, List<String>> target)
	{

		ConfChangeReport r = new ConfChangeReport();
		Map<String,List<String>> removedConfs = new HashMap<String,List<String>>();
		Map<String,List<String>> addedConfs = new HashMap<String,List<String>>();
		
		for(String old_conf_name : reference.keySet())
		{
			List<String> ref_feats = reference.get( old_conf_name );
			List<String> target_feats = target.get( old_conf_name );
			if(target_feats == null)
			{
				removedConfs.put( old_conf_name, reference.get( old_conf_name ));
				continue;
			}
			else
			{	
				if( ! ( ref_feats.containsAll( target_feats ) && target_feats.containsAll( ref_feats ) ) )
				{	//the feature list has been updated.
					removedConfs.put( old_conf_name, reference.get( old_conf_name ));
					addedConfs.put( old_conf_name, reference.get( old_conf_name ));
				}
			}
		}
		
		for(String new_conf_name : target.keySet())
		{
			if(reference.get( new_conf_name ) == null)
			{
				addedConfs.put(new_conf_name, target.get( new_conf_name ));
			}
		}
		
		r.setAddedConfigurations( addedConfs );
		r.setRemovedConfigurations( removedConfs );
		
		return r;
	}

static	private void getConfsFromFeatureIDEFolder(File prj_folder, String[] conf_files, Map<String, List<String>> new_confs)
			throws FileNotFoundException, IOException
	{
		for(int i = 0; i < conf_files.length; i++)
		{
			String conf_file_name = conf_files[i];
			if(conf_file_name.startsWith( "." ))
				continue;
			File conf = new File(prj_folder.getPath()+"/"+conf_file_name);
			BufferedReader reader = new BufferedReader(new FileReader(conf));
			List<String> feat_names = new ArrayList<String>();
			String line = reader.readLine();
			while(line != null)
			{
				if(line.startsWith( "\"" ) && line.endsWith( "\"" ))
					line = line.substring( 1 , line.length() - 1 );
				feat_names.add( line.trim() );
				line = reader.readLine();
			}
			new_confs.put(conf_file_name,feat_names);
		}
	}
}
