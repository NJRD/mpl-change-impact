package neo4jfm.operations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Design;
import neo4jfm.domain.Implementation;
import neo4jfm.domain.Interface;
import neo4jfm.domain.InterfaceUsage;
import neo4jfm.domain.Model.Domains;
import neo4jfm.reports.CompatibilityConstraint;
import neo4jfm.reports.CompatibilityReport;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;

import de.ovgu.featureide.fm.core.FeatureModel;

public class Scenarios
{

	private static GraphNavigator	nav			= null;
	private static Scenarios		_instance	= null;

	public static Scenarios getValidator()
	{
		if ( _instance == null )
		{
			_instance = new Scenarios();
			nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		}
		return _instance;
	}

	private Scenarios()
	{

	}

	public String highResolutionOnVideoChainDesign()
	{
		StringBuffer info = new StringBuffer( 200 );
		FMExporter gen = new FMExporter();
		List<Long> ids = new ArrayList<Long>();

		Node n = nav.getNodeByDomain( "video chain" , Domains.DESIGN );
		ids.add( n.getId() );

		Node n2 = nav.getNodeByDomain( "video export" , Domains.INTERFACE );
		ids.add( n2.getId() );

		FeatureModel model = gen.CreateModelWithRoots( ids , true );
		CompatibilityChecker compat = new CompatibilityChecker( model , nav );

		List<String> itfNames = new ArrayList<String>();
		itfNames.add( "Video export resolution: 1600x1200" );

		info.append( "Checking which implementation of the \"video chain\" system can satisfy the interface \"Video export\" with the following parameter: \n" );
		info.append( "	Video export resolution: 1600x1200 \n" );
		CompatibilityReport report = compat.assessDesignInterfaceCompliance( itfNames , n , null );

		info.append( "Compatible implementations:\n" );
		for ( Implementation impl : report.compatibles )
		{
			info.append( "	" + impl.name + "(" + impl.designName + ")\n" );
		}

		info.append( "Incompatible implementations:\n" );
		for ( Implementation impl : report.incompatibles )
		{
			info.append( "	" + impl.name + "(" + impl.designName + ") " + impl.errCode + "\n" );
		}
		return info.toString();
	}

	public String magellanUSFullCheckIn2Releases()
	{

		StringBuffer info = new StringBuffer( 250 );
		info.append( "tests scenario: check existing information to identify which existing implementation\n" );
		info.append( "can satisfy the Magellan-USA product for 2 different releases \n" );

		Node n = nav.getNodeByDomain( "Magellan-USA" , Domains.PRODUCT );

		CompatibilityChecker compat = new CompatibilityChecker( nav );
		info.append( "\n" );
		info.append( "===" );
		info.append( "Checking if Magellan-US can be implemented in release 8.1 \n" );
		Node release81 = nav.getNodeByDomain( "8.1" , Domains.RELEASE );
		List<InterfaceUsage> report1 = compat.assessExternalProductCompatibilityWithRelease( n , release81 );
		for ( InterfaceUsage itf : report1 )
		{
			info.append( "\n" );
			info.append( itf.report() );
		}

		info.append( "\n" );
		info.append( "===" );
		info.append( "Checking if Magellan-US can be implemented in release 8.2 \n" );
		Node release82 = nav.getNodeByDomain( "8.2" , Domains.RELEASE );
		List<InterfaceUsage> report2 = compat.assessExternalProductCompatibilityWithRelease( n , release82 );
		for ( InterfaceUsage itf : report2 )
		{
			info.append( "\n" );
			info.append( itf.report() );
		}

		return info.toString();
	}

	public String singleProductWith2interfaces()
	{
		StringBuffer info = new StringBuffer( 200 );

		info.append( "tests scenario: Retrieve valid implementation for the Magellan EU product\n" );
		info.append( "This product requires 2 interfaces : \"video export\" and \"display modes\" \n" );
		info.append( "Only flexvision compliant configuration of the video chains should be seen as valid\n" );

		Node n = nav.getNodeByDomain( "Magellan-CE" , Domains.PRODUCT );

		CompatibilityChecker compat = new CompatibilityChecker( nav );
		info.append( "\n" );
		info.append( "===" );
		info.append( "Checking if Magellan-EU can be implemented in release 8.1 \n" );
		Node release81 = nav.getNodeByDomain( "8.1" , Domains.RELEASE );
		List<InterfaceUsage> report1 = compat.assessExternalProductCompatibilityWithRelease( n , release81 );
		for ( InterfaceUsage itf : report1 )
		{
			info.append( "\n" );
			info.append( itf.report() );
		}

		return info.toString();
	}

	public String mixedFeatureTest(String productName, String[] components)
	{
		StringBuffer info = new StringBuffer( 200 );
		Node externalProduct = nav.getNodeByDomain( productName , Domains.PRODUCT );

		boolean err = false;
		if ( externalProduct == null )
		{
			info.append( "Unable to find the external product " + productName
					+ ". Are you sure it's spelled correctly ?\n" );
			err = true;
		}

		// Design: video chain, component names= w.c.b.2, utp, splitter...
		Map<String, List<String>> designConstraints = new HashMap<String, List<String>>();

		CompatibilityConstraint constraints = new CompatibilityConstraint();

		for ( String compo : components )
		{
			compo = compo.trim();
			Node c = nav.getNodeByDomain( compo , Domains.COMPONENT );
			if ( c == null )
			{
				info.append( "Unable to find component " + compo + ". Are you sure it's spelled correctly ?\n" );
				err = true;
			}
			else
			{
				constraints.requiredFeatures.add( compo );

				List<String> designNames = nav.getDesignNamesForComponent( c );
				for ( String design : designNames )
				{
					if ( designConstraints.keySet().contains( design ) )
					{
						List<String> designRequirements = designConstraints.get( design );
						if ( !designRequirements.contains( compo ) )
							designRequirements.add( compo );
					}
					else
					{
						List<String> designReqs = new ArrayList<String>();
						designReqs.add( compo );
						designConstraints.put( design , designReqs );
					}
				}
			}
		}

		if ( err )
		{
			info.append( " invalid input. Aborting. Sorry! \n" );
			return info.toString();
		}

		CompatibilityChecker compat = new CompatibilityChecker( nav );
		Node release81 = nav.getNodeByDomain( "8.1" , Domains.RELEASE );
		List<InterfaceUsage> report1 = compat.assessExternalProductCompatibilityWithRelease( externalProduct ,
				release81 , constraints );

		for ( InterfaceUsage itf : report1 )
		{
			info.append( "\n" );
			info.append( itf.report() );
		}
		return info.toString();
	}

	public void filterReports(List<InterfaceUsage> reports, Map<String, List<String>> designConstraints)
	{
		for ( InterfaceUsage report : reports )
		{
			for ( Interface itf : report.requiredInterfaces )
			{
				List<Implementation> invalidImpls = new ArrayList<Implementation>();
				for ( Implementation impl : itf.compatibleImpl )
				{
					if ( designConstraints.containsKey( impl.designName ) )
					{
						if ( !impl.selectedFeatures.containsAll( designConstraints.get( impl.designName ) ) )
						{
							invalidImpls.add( impl );
						}
					}
				}
				itf.compatibleImpl.removeAll( invalidImpls );
			}
		}
	}

	public String getConfForDesignTest()
	{
		StringBuffer info = new StringBuffer( 250 );

		Node n = nav.getNodeByDomain( "video chain" , Domains.DESIGN );
		Design d = new Design( n );
		// d.nodeIde;

		Map<String, List<String>> confs = nav.getDesignImplementations( d.nodeId );
		for ( String key : confs.keySet() )
		{
			info.append( "configuration : " + key + "\n" );
			for ( String feat : confs.get( key ) )
			{
				info.append( " " + feat + "\n" );
			}
		}

		List<String> invalidConfs = d.update();

		for ( String s : invalidConfs )
		{
			info.append( "invalid configuration : " + s + "\n" );
		}
		return info.toString();
	}

	public String updateFMFromFile1()
	{
		FMImporter importer = new FMImporter( ExternalResourcesRepository.getImportPath() + "/model_3/" );
		importer.importIntoNeo4J();

		return "";

	}
}
