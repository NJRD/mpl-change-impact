package neo4jfm.operations;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.prop4j.NodeReader;

import de.ovgu.featureide.fm.core.Constraint;
import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.Selection;
import de.ovgu.featureide.fm.core.io.UnsupportedModelException;
import de.ovgu.featureide.fm.core.io.xml.XmlFeatureModelReader;
import de.ovgu.featureide.fm.core.io.xml.XmlFeatureModelWriter;
import de.ovgu.featureide.ui.actions.generator.BuilderConfiguration;

public class FMExporter {

	static private String suffix = "<calculations Auto=\"true\" Constraints=\"true\" Features=\"true\" Redundant=\"true\"/><comments/><featureOrder userDefined=\"false\"/></featureModel>"; 
	static private String prefix = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><featureModel chosenLayoutAlgorithm=\"1\">";
	
	GraphNavigator nav = null;
	public FMExporter()
	{
		nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
	}
	
	public FeatureModel CreateModelWithRoots(List<Long> ids, boolean addRule) {
		StringBuffer input = new StringBuffer(250);
		
		System.out.println("dumping model with " + ids.size() + " roots");
		
		input.append(prefix);
		
		getNodes(ids,input);
		
		input.append(suffix);

		FeatureModel model = new FeatureModel();
		XmlFeatureModelReader reader = new XmlFeatureModelReader(model);
		
		try 
		{			
			reader.readFromString(input.toString());
		} 
		catch (UnsupportedModelException e1) 
		{
			e1.printStackTrace();
		}
		
		List<String> rules = getApplicableRules(ids);
		for(String rule : rules)
		{
			addRuleToModel(model,rule);
		}

		dumpModelToFile(model);
		
		if(ids.size() == 1)
		{
			String modelName = model.getRoot().getName();	
			addConfigurationsToModel(ids.get(0), modelName);
		}
		
		
		return model;
	}
	

	private List<String> getApplicableRules(List<Long> ids) 
	{
		List<String> rules = new ArrayList<String>();
		
		for(Long id : ids)
		{
			Node localRoot = nav.getNodeById(id);
			List<Node> candidateRules = nav.getPointingNodes(localRoot, RelTypes.CONSTRAINS);
			
			for(Node rule : candidateRules)
			{
				List<Node> constrainedNodes = nav.getPointedNodes(rule, RelTypes.CONSTRAINS);

				boolean allIn = true;
				for(Node constrained : constrainedNodes)
				{
					Long candidateId = constrained.getId();
					if(!ids.contains(candidateId))
						allIn = false;
				}
				
				if(allIn)
				{
					String expr = nav.getAttributes(rule).get("rule");
					if( !rules.contains(expr) )
						rules.add(expr);		
				}
			}
		}
		return rules;
	}

	private void addRuleToModel(FeatureModel model, String expr) {
		NodeReader nodeReader = new NodeReader();
		org.prop4j.Node prop = nodeReader.stringToNode(expr);
		Constraint c = new Constraint(model,prop);
		model.addConstraint(c);
	}

	
	
	private void addConfigurationsToModel( Long featureModelId,String modelName)
	{
		
		Map<String,List<String>> confs= nav.getConfigurations( featureModelId );
		System.out.println("dumping configurations: " + confs.keySet().size() + " found for root " + featureModelId);
		for(String confName : confs.keySet())
		{
			List<String> selectedFeatures = confs.get( confName );

			try{
				
				String  output = ExternalResourcesRepository.getExportPath() + modelName + "/confs";
				File outputFolder =new File(output);
				System.out.println( "dumping configurations to " + outputFolder );
				if(!outputFolder.exists())
				{
					boolean success = outputFolder.mkdirs();
					if (!success) {
						System.err.println( "Failed to create file for configuration dump" );
					}
				}
				
				File outputFile = new File(outputFolder+confName);
				System.out.println( "dumping conf to file : " + outputFile );
				FileWriter writer = new FileWriter( outputFile );
				for(String name : selectedFeatures)
				{
					writer.append( name +"\n" );
				}
				
				writer.flush();
				writer.close();

			}
			catch(Exception e)
			{
				System.err.println("invalid configuration : " + confName  );
			}
		}

		
	}
	
	
	private void dumpModelToFile(FeatureModel model) {
		String modelName = model.getRoot().getName();		
		String  output = ExternalResourcesRepository.getExportPath() +modelName;
		File outputFolder =new File(output);
		if(!outputFolder.exists())
		{
			boolean success = outputFolder.mkdirs();
			if (!success) {
				System.err.println( "Failed to create file for model dump" );
			}
		}
		
		XmlFeatureModelWriter writer = new XmlFeatureModelWriter(model);
		writer.writeToFile(new File(output+"/model_"+modelName+".xml"));
	}
	
	
	private void dumpConfigurationsToFiles(FeatureModel m)
	{
		
	}

	private void getNodes(List<Long> ids, StringBuffer input) {
	
		String prefix = "<struct>\n";
		String suffix = "</struct>\n";
		
		input.append(prefix);
		if(ids.size() > 1)
		{
			//need to create virtual root
			String virtualNode = "<and abstract=\"true\" mandatory=\"true\" name=\"virtual\">\n";
			input.append(virtualNode);
			for( long id : ids)
			{
				expandNode(id,input,true);
			}
			input.append("</and>\n");
		}
		else
		{
			expandNode(ids.get(0),input,true);
		}
		input.append(suffix);
	}


	private void expandNode(long id, StringBuffer input, boolean mandatory) {
				
		Node n = nav.getNodeById(id);
		Iterable<Relationship> contains = nav.getRelationships(n, Direction.OUTGOING, RelTypes.CONTAINS);
		String reltype  = "";
		String tagValue = "";
		
		Map<Long,String> map = new HashMap<Long,String>();
		for(Relationship r : contains)
		{
			
			reltype = nav.getRelationshipAttributeValue(r,"type"); 
			map.put(nav.getEndNodeId(r),reltype);
		}
		
		if("external product".equalsIgnoreCase(nav.getAttributes(n).get("type")))
		{
			
			Iterable<Relationship> characterized = nav.getRelationships(n,Direction.OUTGOING, RelTypes.REQUIRES);
			for(Relationship r : characterized)
			{
				reltype = nav.getRelationshipAttributeValue(r,"type"); 
				map.put(nav.getEndNodeId(r),reltype);
			}
		}
		
		
		if(map.size() == 0 )
		{//no relationship, add node info and return;
			input.append("<feature mandatory=\""+mandatory+"\" name=\""+ (nav.getAttributes(n).get("name")).replace("(", " ").replace(")", " ")+"\"/>\n");
			return;
		}
		
		boolean isAlt = false;
		boolean isOr = false;
		
		isAlt = map.containsValue("alt");
		isOr = map.containsValue("or");
		if(isAlt && isOr)
		{
			System.err.println("got an OR and ALT struct under the same node. Check definition of node id "+ n.getId() +" - "+(nav.getAttributes(n).get("name")));
		}
		
		if(isAlt)
			tagValue = "alt";
		else if (isOr)
			tagValue = "or";
		else
			tagValue = "and";

		
		input.append("<"+tagValue+" mandatory=\""+mandatory+"\" name=\""+(nav.getAttributes(n).get("name"))+"\">\n");

		for(Long nodeId : map.keySet())
		{
			String type = map.get(nodeId);
			if("mandatory".equalsIgnoreCase(type))
			{
				expandNode(nodeId,input,true);
			}
			else
			{
				expandNode(nodeId,input,false);
			}
		}
		
		input.append("</"+tagValue+">");
	}
}
