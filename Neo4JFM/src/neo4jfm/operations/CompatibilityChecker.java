package neo4jfm.operations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.domain.Implementation;
import neo4jfm.domain.Interface;
import neo4jfm.domain.InterfaceUsage;
import neo4jfm.domain.Implementation.IncompatibilityType;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.reports.CompatibilityConstraint;
import neo4jfm.reports.CompatibilityReport;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;

import de.ovgu.featureide.fm.core.Feature;
import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.FeatureNotFoundException;
import de.ovgu.featureide.fm.core.configuration.Selection;
import de.ovgu.featureide.fm.core.configuration.SelectionNotPossibleException;

public class CompatibilityChecker
{

	private FeatureModel		model	= null;
	private GraphNavigator			nav		= null;
	private CompatibilityReport	report	= null;
	private FMExporter			gen;

	/**
	 * 
	 * @param m
	 *            FeatureIDE Feature model, with constraints
	 */
	public CompatibilityChecker(FeatureModel m, GraphNavigator n)
	{
		model = m;
		nav = n;
		gen = new FMExporter();
	}

	public CompatibilityChecker(GraphNavigator n)
	{
		nav = n;
		gen = new FMExporter();
	}

	public CompatibilityReport assessDesignInterfaceCompliance(List<String> itfFeatureNames, Node design, CompatibilityConstraint constraints)
	{
		return assessDesignInterfaceComplianceForRelease( itfFeatureNames , design , null, constraints );
	}

	public CompatibilityReport assessDesignInterfaceComplianceForRelease(List<String> itfFeatureNames, Node design, Node release, CompatibilityConstraint constraints)
	{
		String designName = nav.getAttributes( design ).get( "name" );
		List<Node> implementations = nav.getPointingNodes( design , RelTypes.IS_A );
		if ( implementations.size() == 0 )
		{
			System.err.println( "Compatibility requested done on design "+ designName + " but no implementation found." );
			return report;
		}

		CompatibilityReport report = new CompatibilityReport();
		List<String> availableComponents = nav.getComponentNamesFromDesign( design );

		//adding required features from constraints
		List<String> requiredFeatures = new ArrayList<String>();
		List<String> forbiddenFeatures = new ArrayList<String>();
		getFeatureSelectionConstraints( constraints , availableComponents , requiredFeatures , forbiddenFeatures );

		for ( Node impl : implementations )
		{
			Implementation reportImpl = new Implementation(impl);
			reportImpl.name = reportImpl.attributes.get( "name");
			reportImpl.designName = designName;

			List<Node> components = nav.getPointedNodes( impl , RelTypes.CONTAINS );
			for ( Node component : components )
				reportImpl.selectedFeatures.add( nav.getAttributes( component ).get( "name" ) );
			
			if ( ! validateProductConstraints( report , requiredFeatures , forbiddenFeatures , reportImpl ) )
			{
				System.out.println("Found configuration violating product constraints");
				continue;
			}
			
			if ( release != null && !checkImplementationAvailabilityForRelease( release , components ) )
			{
				reportImpl.errCode = IncompatibilityType.INCOMPATIBLE_RELEASE;
				report.incompatibles.add( reportImpl );
				continue;
			}
			resolveFeatureSelection( itfFeatureNames , availableComponents , reportImpl );
			validateFeatureSelection( report , reportImpl );
		}
		return report;
	}

	private void getFeatureSelectionConstraints(CompatibilityConstraint constraints, List<String> availableComponents,
			List<String> requiredFeatures, List<String> forbiddenFeatures)
	{
		if(constraints == null)
		{
			return;
		}
		
		for(String feature : constraints.requiredFeatures)
		{
			if(availableComponents.contains( feature ))
			{
				System.out.println("Adding required feature for current design : " + feature);
				requiredFeatures.add( feature );
			}
		}
		
		for(String feature : constraints.forbiddenFeatures)
		{
			if(availableComponents.contains( feature ))
			{
				System.out.println("Adding forbidden feature for current design : " + feature);
				forbiddenFeatures.add( feature );
			}
		}
		
	}

	private boolean validateProductConstraints(CompatibilityReport report, List<String> requiredFeatures,
			List<String> forbiddenFeatures, Implementation reportImpl)
	{
		for(String featureName : reportImpl.selectedFeatures)
		{
			if(forbiddenFeatures.contains( featureName ))
			{
				reportImpl.errCode = IncompatibilityType.PRODUCT_CONSTRAINTS_VIOLATION_FORBIDDEN_FEATURE_INCLUDED;
				report.incompatibles.add( reportImpl );
				return false;
			}
		}
		
		for(String requiredFeature : requiredFeatures)
		{
			if(!reportImpl.selectedFeatures.contains( requiredFeature ))
			{
				reportImpl.errCode = IncompatibilityType.PRODUCT_CONSTRAINTS_VIOLATION_REQUIRED_FEATURE_MISSING;
				report.incompatibles.add( reportImpl );
				return false;
			}
		}
		
		return true;
	}

	
	
	private void validateFeatureSelection(CompatibilityReport report, Implementation reportImpl)
	{
		Configuration conf = new Configuration( model );
		try
		{
			setSelection( reportImpl , conf );
		}
		catch ( SelectionNotPossibleException e )
		{
			reportImpl.errCode = IncompatibilityType.RULE_VIOLATION;
			report.incompatibles.add( reportImpl );
			return;
		}
		catch ( FeatureNotFoundException e )
		{
			System.err.println( " Attempt to set feature " + reportImpl.name
					+ " on design " + reportImpl.designName
					+ " failed as it does not exists" );
			return;
		}

		if ( conf.valid() )
		{
			reportImpl.errCode = IncompatibilityType.VALID;
			report.compatibles.add( reportImpl );
		}
		else
		{
			reportImpl.errCode = IncompatibilityType.INCOMPLETE_SELECTION;
			report.compatibles.add( reportImpl );
		}
		return;
	}

	private void resolveFeatureSelection(List<String> itfFeatureNames, List<String> availableComponents, Implementation reportImpl)
	{
		reportImpl.selectedFeatures.addAll( itfFeatureNames );
		for ( String s : availableComponents )
		{
			if ( !reportImpl.selectedFeatures.contains( s )
					&& !reportImpl.unselectedFeatures.contains( s ) )
			{
				reportImpl.unselectedFeatures.add( s );
			}
		}
	}

	private void setSelection(Implementation reportImpl, Configuration conf)
	{
		for ( String name : reportImpl.selectedFeatures )
		{
			conf.setManual( name , Selection.SELECTED );
		}

		for ( String name : reportImpl.unselectedFeatures )
		{
			conf.setManual( name , Selection.UNSELECTED );
		}
	}

	private boolean checkImplementationAvailabilityForRelease(Node release, List<Node> components)
	{
		// check if all components are available for the given release
		if ( release == null )
			return true;

		String releaseName = nav.getAttributes( release ).get( "name" );

		for ( Node component : components )
		{
			boolean found = false;
			String type = nav.getAttributes( component ).get( "type" );

			if ( !"component".equalsIgnoreCase( type ) )
				continue; // this is not a component. It does not need to be
							// checked.

			List<Node> pointings = nav.getPointingNodes( component ,RelTypes.CONTAINS );
			
			for ( Node n : pointings )
			{
				if ( nav.getAttributes( n ).get( "name" ).equals( releaseName ) )
				{
					found = true;
				}
			}

			if ( !found )
			{
				System.out.println( " Component " + nav.getAttributes( component ).get( "name" ) + " is not available in release " + releaseName );
				return false;
			}
		}
		return true;
	}

	/**
	 * Assuming n is an "external product" type of node
	 * 
	 * @param externalProduct
	 */
	public List<InterfaceUsage> assessExternalProductCompatibilityWithRelease(Node externalProduct, Node release)
	{
		return assessExternalProductCompatibilityWithRelease(externalProduct,release,null);
	}

	
	public List<InterfaceUsage> assessExternalProductCompatibilityWithRelease(Node externalProduct, Node release, CompatibilityConstraint constraints)
	{
		List<Long> ids = new ArrayList<Long>();
		ids.add( externalProduct.getId() );

		List<Node> usages = nav.getPointingNodes( externalProduct , RelTypes.IS_A );
		List<Node> interfaces = nav.getPointedNodes( externalProduct , RelTypes.USES );

		List<InterfaceUsage> validProductUsage = new ArrayList<InterfaceUsage>();
		List<InterfaceUsage> invalidProductUsage = new ArrayList<InterfaceUsage>();
		
		//retrieve here constraints associated with the product in question.
		
		for ( Node usage : usages )
		{
			InterfaceUsage use = assessInterfaceUsage(usage, interfaces, release, constraints);
			if ( use.isValid )
				validProductUsage.add( use );
			else
				invalidProductUsage.add( use );
		}
		return validProductUsage;
		
	}
	
	private InterfaceUsage assessInterfaceUsage(Node usage, List<Node> interfaces, Node release, CompatibilityConstraint constraints  )
	{
		List<Node> designsForUsage = new ArrayList<Node>();

		InterfaceUsage report = new InterfaceUsage();
		report.name = nav.getAttributes( usage ).get( "name" );

		List<Node> usedParameters = nav.getPointedNodes( usage , RelTypes.REQUIRES );
		for ( Node n : usedParameters )
			report.features.add( nav.getAttributes( n ).get( "name" ) );

		for ( Node itf : interfaces )
		{
			List<Node> designs = nav.getPointingNodes( itf , RelTypes.IMPLEMENTS );
			designsForUsage.addAll( designs );
			validateIsolatedInterfaceUsage( release , usedParameters , itf , report , constraints );
		}

		if ( report.isValid )
		{

			// designName: video chain, valid configuration : {A,B};{C,D}...
			Map<String, List<List<String>>> validConfigurations = new HashMap<String, List<List<String>>>();
			buildValidUsageMap( report , validConfigurations );

			crossDesignCheck( designsForUsage , report , validConfigurations );
		}

		return report;
	}

	private void buildValidUsageMap(InterfaceUsage report, Map<String, List<List<String>>> validConfigurations)
	{

		for ( Interface itfUsage : report.requiredInterfaces )
		{
			for ( Implementation impl : itfUsage.compatibleImpl )
			{
				String designName = impl.designName;
				if ( validConfigurations.containsKey( designName ) )
				{
					List<List<String>> knownConfigs = validConfigurations
							.get( designName );
					boolean alreadyKnown = false;
					for ( List<String> knownConfig : knownConfigs )
					{
						if ( knownConfig.containsAll( impl.selectedFeatures ) )
							alreadyKnown = true;
					}

					if ( !alreadyKnown )
					{
						knownConfigs.add( impl.selectedFeatures );
					}
				}
				else
				{
					List<String> validConfig = impl.selectedFeatures;
					List<List<String>> validConfigs = new ArrayList<List<String>>();
					validConfigs.add( validConfig );
					validConfigurations.put( designName , validConfigs );
				}
			}
		}

	}

	private void validateIsolatedInterfaceUsage(Node release, List<Node> usedParameters, Node itf, InterfaceUsage report, CompatibilityConstraint constraints)
	{
		Interface usedInterface = new Interface();
		usedInterface.name = (String) nav.getAttributes( itf ).get( "name" );
		report.requiredInterfaces.add( usedInterface );
		
		System.out.println("now check the validity of interface " + usedInterface.name);

		// extract parameter provided by the current interface
		List<String> selectedFeatureNames = getUsedParametersInInterface(usedParameters , itf );
		usedInterface.requiredFeatures.addAll( selectedFeatureNames );

		List<Node> designs = nav.getPointingNodes( itf , RelTypes.IMPLEMENTS );

		if ( designs.size() == 1 )
		{
			Node design = designs.get( 0 );
			checkDesignInterfaceCompatibility( release , itf , usedInterface , selectedFeatureNames , design, constraints );
			report.isValid = true;
		}
		else
		{
			System.err.println( "The current tool implementation only supports 1 design per interface. Check the data." );
			report.isValid = false;
		}
	}

	private boolean crossDesignCheck(List<Node> involvedDesigns, InterfaceUsage report, Map<String, List<List<String>>> validDesignUsages)
	{
		List<String> designAnalyzed = new ArrayList<String>();
		
		for ( Node design : involvedDesigns )
		{
			Map<String, List<String>> overlap = nav.getSharedComponentNames( design );
			if ( overlap.size() == 0 )
				continue;

			String designName = nav.getAttributes( design ).get( "name" );

			for ( String otherDesignName : overlap.keySet() )
			{

				if ( designAnalyzed.contains( otherDesignName ) )
					continue;

				// check the selection of the overlapping features in the
				// current design:
				List<String> sharedFeatures = overlap.get( otherDesignName );
				List<List<String>> currentDesignUsages = validDesignUsages.get( designName );
				List<List<String>> otherDesignUsages = validDesignUsages.get( otherDesignName );

				if ( otherDesignUsages == null )
				{ 	// this design is shared but
					// not relevant for the
					// current configuration
					// test.
					continue;
				}

				List<List<String>> invalidCurrentdesignUsage = new ArrayList<List<String>>();

				for ( List<String> currentDesignUsage : currentDesignUsages )
				{
					boolean foundACompatibleOne = false;
					for ( List<String> otherDesignUsage : otherDesignUsages )
					{
						boolean sameStateForAllShared = true;

						for ( String sharedFeature : sharedFeatures )
						{
							if ( ( currentDesignUsage.contains( sharedFeature ) && !otherDesignUsage
									.contains( sharedFeature ) )
									|| ( !currentDesignUsage
											.contains( sharedFeature ) && otherDesignUsage
											.contains( sharedFeature ) ) )
							{
								sameStateForAllShared = false;
							}
						}

						if ( sameStateForAllShared )
						{
							foundACompatibleOne = true;
						}
					}

					if ( !foundACompatibleOne )
					{
						invalidCurrentdesignUsage.add( currentDesignUsage );
					}
				}
				currentDesignUsages.removeAll( invalidCurrentdesignUsage );
			}
			designAnalyzed.add( designName );
		}

		// updating report

		for ( Interface itf : report.requiredInterfaces )
		{
			List<Implementation> toRemove = new ArrayList<Implementation>();
			for ( Implementation impl : itf.compatibleImpl )
			{
				List<List<String>> validImpl = validDesignUsages
						.get( impl.designName );

				if ( !validImpl.contains( impl.selectedFeatures ) )
				{
					impl.errCode = IncompatibilityType.CROSS_DESIGN_INCOMPATIBILITY;
					toRemove.add( impl );
				}
			}
			itf.compatibleImpl.removeAll( toRemove );
			itf.incompatibleImpl.addAll( toRemove );
		}
		return false;
	}

	private List<String> getUsedParametersInInterface( List<Node> usedParameters, Node itf)
	{
		List<Long> idList = new ArrayList<Long>();
		idList.add( itf.getId() );
		FeatureModel itfModel = gen.CreateModelWithRoots( idList , false );
		Collection<Feature> itfFeatures = itfModel.getFeatures();

		List<String> selectedFeatureNames = new ArrayList<String>();
		for ( Node parameter : usedParameters )
		{
			String paramName = nav.getAttributes( parameter ).get( "name" );
			for ( Feature itfFeature : itfFeatures )
			{
				if ( itfFeature.getName().equalsIgnoreCase( paramName )
						&& !selectedFeatureNames.contains( paramName ) )
				{
					selectedFeatureNames.add( paramName );
				}
			}
		}
		return selectedFeatureNames;
	}

	private void checkDesignInterfaceCompatibility(Node release, Node itf, Interface usedInterface, List<String> selectedFeatureNames, Node design, CompatibilityConstraint constraints)
	{
		List<Long> local = new ArrayList<Long>();
		local.add( itf.getId() );
		local.add( design.getId() );
		
		FeatureModel fm = gen.CreateModelWithRoots( local , true );
		CompatibilityChecker checker = new CompatibilityChecker( fm , nav );
		CompatibilityReport report = null;

		if ( release != null )
			report = checker.assessDesignInterfaceComplianceForRelease( selectedFeatureNames, design, release, constraints );
		else
			report = checker.assessDesignInterfaceCompliance( selectedFeatureNames, design, constraints );

		usedInterface.compatibleImpl.addAll( report.compatibles );
		usedInterface.incompatibleImpl.addAll( report.incompatibles );
	}

}
