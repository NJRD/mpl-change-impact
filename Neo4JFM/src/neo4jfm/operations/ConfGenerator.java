package neo4jfm.operations;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prop4j.And;
import org.prop4j.Literal;
import org.prop4j.Node;
import org.prop4j.SatSolver;

import de.ovgu.featureide.fm.core.Feature;
import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.configuration.Configuration;
//import de.ovgu.featureide.fm.core.configuration.ConfigurationReader;
import de.ovgu.featureide.fm.core.configuration.FeatureNotFoundException;
import de.ovgu.featureide.fm.core.configuration.Selection;
import de.ovgu.featureide.fm.core.configuration.SelectionNotPossibleException;
import de.ovgu.featureide.fm.core.editing.NodeCreator;
import de.ovgu.featureide.ui.actions.generator.BuilderConfiguration;

public class ConfGenerator {
	
	FeatureModel featureModel = null;
	Node rootNode = null;
	private LinkedList<Node> children = null;
	private long configurationNumber = 0;
	private long confs= 0;
	//private ConfigurationReader reader = null;
	private Configuration configuration = null;
	LinkedList<BuilderConfiguration> configurations = new LinkedList<BuilderConfiguration>();
	private int maxSize = 500;
	
	public ConfGenerator(FeatureModel fm) {
		featureModel = fm;
		configuration = new Configuration(featureModel);
		//reader = new ConfigurationReader(configuration);
	}

	/**
	 * Adds the given configuration to configurations.
	 * 
	 * @param configuration
	 */
	public synchronized void addConfiguration(BuilderConfiguration configuration) {
		configurations.add(configuration);
	}

	public LinkedList<BuilderConfiguration> getAllValidConfig()
	{
		buildAll(featureModel.getRoot());
		return configurations;
	}


	/**
	 * Builds all possible valid configurations for the feature project.<br>
	 * Iterates through the structure of the feature model and ignores
	 * constraints, to get a linear expenditure.<br>
	 * After collecting a configurations the satsolver tests its validity.<br>
	 * Then the found configuration will be build into the folder for all valid
	 * products.
	 * 
	 * @param root
	 *            The root feature of the feature model
	 * @param monitor
	 */
	private void buildAll(Feature root) {
		LinkedList<Feature> selectedFeatures2 = new LinkedList<Feature>();
		selectedFeatures2.add(root);
		rootNode = NodeCreator.createNodes(featureModel, false).toCNF();
		children = new LinkedList<Node>();
		try{
			build(root, "", selectedFeatures2);
		}catch(IOException e)
		{
			System.err.println("failed to create all configurations");
		}
	}

	private void build(Feature currentFeature, String selected,
			LinkedList<Feature> selectedFeatures2) throws IOException {
		

		if ((configurationNumber != 0 && confs > configurationNumber)) {
			return;
		}

		
		if (featureModel.getConstraintCount() > 0) {
			children.clear();
			for (String feature : selected.split("[ ]")) {
				children.add(new Literal(feature, true));
			}
			try {
				if (!(new SatSolver(
						new And(rootNode.clone(), new And(children)), 1000))
						.isSatisfiable()) {
					return;
				}
			} catch (org.sat4j.specs.TimeoutException e) {
				System.err.println("Sat4j Time out error: " + e.getMessage());
				e.printStackTrace();
			}
		}

		if (selectedFeatures2.isEmpty()) {
			if (buildConfFromString(selected)) {
				if (configuration.valid()) {
					LinkedList<String> selectedFeatures3 = new LinkedList<String>();
					//for (String f : selected.split("[ ]")) {
					for (String f :selected.split("\\r?\\n")){
						if (!"".equals(f.trim())) {
							selectedFeatures3.add(f.trim());
						}
					}
					
					for (Feature f : configuration.getSelectedFeatures()) {
						if (f.isConcrete()) {
							if (!selectedFeatures3.contains(f.getName())) {
								if(!selectedFeatures3.contains(f.getName().replace("\"", "")))
									return;
							}
						}
					}
					for (String f : selectedFeatures3) {
						if (configuration.getSelectablefeature(f)
								.getSelection() != Selection.SELECTED) {
							return;
						}
					}

					
					addConfiguration(new BuilderConfiguration(configuration,confs));

					if (configurations.size() >= maxSize) {
						
						synchronized (this) {
							while (configurations.size() >= maxSize) {
								try {
									wait(1000);
								} catch (InterruptedException e) {
									System.err.println("cleaning up conf failed.");
								}
							}
						}
					}
					confs++;

				}
			}
			return;
		}

		if (currentFeature.isAnd()) {
			buildAnd(selected, selectedFeatures2);
		} else if (currentFeature.isOr()) {
			buildOr(selected, selectedFeatures2);
		} else if (currentFeature.isAlternative()) {
			buildAlternative(selected, selectedFeatures2);
		}
	}

	private void buildAlternative(String selected,
			LinkedList<Feature> selectedFeatures2) throws IOException {
		Feature currentFeature = selectedFeatures2.getFirst();
		selectedFeatures2.removeFirst();
		LinkedList<Feature> selectedFeatures3 = new LinkedList<Feature>();
		if (currentFeature.isConcrete()) {
			if ("".equals(selected)) {
				selected = ""+currentFeature.getName()+"\n";
			} else {
				selected += "" + currentFeature.getName()+"\n";
			}
		}
		if (!currentFeature.hasChildren()) {
			if (selectedFeatures2.isEmpty()) {
				currentFeature = null;
			} else {
				currentFeature = selectedFeatures2.getFirst();
			}
			selectedFeatures3.addAll(selectedFeatures2);
			build(currentFeature, selected, selectedFeatures3);
			return;
		}
		for (int i2 = 0; i2 < getChildren(currentFeature).size(); i2++) {
			selectedFeatures3 = new LinkedList<Feature>();
			selectedFeatures3.add(getChildren(currentFeature).get(i2));
			selectedFeatures3.addAll(selectedFeatures2);
			build(selectedFeatures3.isEmpty() ? null
					: selectedFeatures3.getFirst(), selected,
					selectedFeatures3);
		}
	}

	private void buildOr(String selected,
			LinkedList<Feature> selectedFeatures2) throws IOException {
		Feature currentFeature = selectedFeatures2.getFirst();
		selectedFeatures2.removeFirst();
		LinkedList<Feature> selectedFeatures3 = new LinkedList<Feature>();
		if (currentFeature.isConcrete()) {
			if ("".equals(selected)) {
				selected = ""+currentFeature.getName()+"\n";
			} else {
				selected += ""+ currentFeature.getName()+"\n";
			}
		}
		if (!currentFeature.hasChildren()) {
			if (selectedFeatures2.isEmpty()) {
				currentFeature = null;
			} else {
				currentFeature = selectedFeatures2.getFirst();
			}
			selectedFeatures3.addAll(selectedFeatures2);
			build(currentFeature, selected, selectedFeatures3);
			return;
		}
		int k2;
		int i2 = 1;
		if (getChildren(currentFeature).size() < currentFeature.getChildren()
				.size()) {
			i2 = 0;
		}
		for (; i2 < (int) java.lang.Math.pow(2, getChildren(currentFeature)
				.size()); i2++) {
			k2 = i2;
			selectedFeatures3 = new LinkedList<Feature>();
			for (int j = 0; j < getChildren(currentFeature).size(); j++) {
				if (k2 % 2 != 0) {
					selectedFeatures3.add(getChildren(currentFeature).get(j));
				}
				k2 = k2 / 2;
			}
			selectedFeatures3.addAll(selectedFeatures2);
			build(selectedFeatures3.isEmpty() ? null
					: selectedFeatures3.getFirst(), selected,
					selectedFeatures3);
		}
	}

	private void buildAnd(String selected,
			LinkedList<Feature> selectedFeatures2) throws IOException {
		Feature currentFeature = selectedFeatures2.getFirst();
		selectedFeatures2.removeFirst();
		LinkedList<Feature> selectedFeatures3 = new LinkedList<Feature>();
		if (currentFeature.isConcrete()) {
			if ("".equals(selected)) {
				selected = currentFeature.getName()+"\n";
			} else {
				selected += currentFeature.getName()+"\n";
			}
		}
		if (!currentFeature.hasChildren()) {
			if (selectedFeatures2.isEmpty()) {
				currentFeature = null;
			} else {
				currentFeature = selectedFeatures2.getFirst();
			}
			selectedFeatures3.addAll(selectedFeatures2);
			build(currentFeature, selected, selectedFeatures3);
			return;
		}
		int k2;
		LinkedList<Feature> optionalFeatures = new LinkedList<Feature>();
		for (Feature f : getChildren(currentFeature)) {
			if (f.isMandatory()) {
				selectedFeatures2.add(f);
			} else {
				optionalFeatures.add(f);
			}
		}
		for (int i2 = 0; i2 < (int) java.lang.Math.pow(2,
				optionalFeatures.size()); i2++) {
			k2 = i2;
			selectedFeatures3 = new LinkedList<Feature>();
			for (int j = 0; j < optionalFeatures.size(); j++) {
				if (k2 % 2 != 0) {
					selectedFeatures3.add(optionalFeatures.get(j));
				}
				k2 = k2 / 2;
			}
			selectedFeatures3.addAll(selectedFeatures2);
			build(selectedFeatures3.isEmpty() ? null
					: selectedFeatures3.getFirst(), selected,
					selectedFeatures3);
		}

	}
	
	/**
	 * Returns all children of a feature if it is a layer or if it has a child
	 * that is a layer.
	 * 
	 * @param currentFeature  The feature
	 * @return The children
	 */
	private LinkedList<Feature> getChildren(Feature currentFeature) {
		LinkedList<Feature> children = new LinkedList<Feature>();
		for (Feature child : currentFeature.getChildren()) {
			if (child.isConcrete() || hasLayerChild(child)) {
				children.add(child);
			}
		}
		return children;
	}
	
	private boolean hasLayerChild(Feature feature) {
		if (feature.hasChildren()) {
			for (Feature child : feature.getChildren()) {
				if (child.isConcrete() || hasLayerChild(child)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	private boolean buildConfFromString(String text)
			throws IOException {
		
		ByteArrayInputStream inputStream = new ByteArrayInputStream(text.getBytes(Charset
				.availableCharsets().get("UTF-8")));
		
		configuration.resetValues();
		BufferedReader reader = null;
		String line = null;
		Integer lineNumber = 1;
		boolean successful = true;
		try {
			reader = new BufferedReader(new InputStreamReader(inputStream,
					Charset.availableCharsets().get("UTF-8")));
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("#") || line.isEmpty() || line.equals(" ")) {
					lineNumber++;
					continue;
				}
				// the string tokenizer is used to also support the expression
				// format used by FeatureHouse
				StringTokenizer tokenizer = new StringTokenizer(line);
				LinkedList<String> hiddenFeatures = new LinkedList<String>();
				while (tokenizer.hasMoreTokens()) {
					String name = tokenizer.nextToken("\n");
					if (name.startsWith("\"")) {
						try {
							name = name.substring(1);
							name += tokenizer.nextToken("\"");
						} catch (NoSuchElementException e) {
							successful = false;

							return false;
						} catch (NullPointerException e) {
							successful = false;
							return false;
						}
						// Check for ending quotation mark
						try {
							String endingDelimiter = tokenizer.nextToken(" ");
							if (!endingDelimiter.startsWith("\"")) {
								successful = false;
								return false;
							}
						} catch (Exception e) {
							successful = false;

							return false;
						}
					}

					Feature feature = configuration.getFeatureModel()
							.getFeature(name);
					if (feature != null && feature.hasHiddenParent()) {
						hiddenFeatures.add(name);
					} else {
						try {
							configuration.setManual(name, Selection.SELECTED);
						} catch (FeatureNotFoundException e) {
							successful = false;
							return false;
						} catch (SelectionNotPossibleException e) {
							successful = false;
							return false;
						}
					}
				}
				for (String name : hiddenFeatures) {
					try {
						configuration.setAutomatic(name, Selection.SELECTED);
					} catch (FeatureNotFoundException e) {
						successful = false;
						return false;
					} catch (SelectionNotPossibleException e) {
						successful = false;
						return false;
					}
				}
				lineNumber++;
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return successful;
	}
}
