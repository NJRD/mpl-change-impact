package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

/**
 * This class represents external interfaces of the system
 * 
 * @author Dante
 *
 */
public class Interface extends Entity
{
	public String name = "";
	public List<String> requiredFeatures = new ArrayList<String>();
	public List<Implementation> compatibleImpl = new ArrayList<Implementation>();
	public List<Implementation> incompatibleImpl = new ArrayList<Implementation>();
	
	public Interface(Node n)
	{
		super(n);
	}
	
	public Interface()
	{
		
	}
	
	public boolean isSatisfied()
	{
		if(compatibleImpl.size() == 0)
			return false;
		
		if(hasUniqueDesign())
		{
			if (! hasSharedFeature(compatibleImpl.get(0).designName))
			{
				return true;
			}
		}
		else
		{//we need to combine designs to validate interface full satisfaction
			
		}
		
		
		return false;
	}
	
	private boolean hasSharedFeature(String designName)
	{
		return false;
	}
	
	/**
	 * Checks whether this interface is implemented by a single design. 
	 * This is false when multiple designs must be used to obtain all possible interface
	 * parameter values.
	 * @return
	 */
	private boolean hasUniqueDesign()
	{
		return false;
	}
	
	
	
	public String report() {
		StringBuffer txt = new StringBuffer(200);
		
		txt.append("	interface name : \"" + name+ "\"\n");
		txt.append("	required features:  \n");
		for(String s : requiredFeatures)
		{
			txt.append("		"+s+"\n");
		}
		
		if(compatibleImpl.size() == 0)
			txt.append("INCOMPATIBLE: no implementation of this interface match the required parameters \n");
		else
		{
			txt.append("	Compatible implementations: \n");
			for(Implementation impl : compatibleImpl)
			{
				txt.append("		"+impl.designName+" "+impl.name +"("+impl.errCode+")\n");
			}
		}
		
		if(incompatibleImpl.size() != 0)
		{
			txt.append("	Incompatible implementations: \n");
			for(Implementation impl  : incompatibleImpl)
			{
				txt.append("		"+impl.designName+" "+impl.name +" ("+impl.errCode+")\n");
			}
		}
		
		return txt.toString();
	}

}
