package neo4jfm.domain;


import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;


/**
 * This class represent a graph db entity. 
 * 
 */
public class Entity
{
	protected long id = -1;
	public Map<String,String> attributes = null;
	protected GraphNavigator nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
	protected Node node = null;
	
	public Entity(Node n)
	{
		attributes = nav.getAttributes( n );
		id = n.getId();
		node = n;
	}
	public Entity()
	{
		attributes = null;
	}
	
}
