package neo4jfm.domain;

import org.neo4j.graphdb.RelationshipType;

public class Model {

	
	public final class Attributes
	{
		public static final String TYPE = "type";
		public static final String DOMAIN = "domain";
		public static final String NAME = "name";
		public static final String CHOICE_TYPE = "choice";
		public static final String PARAM_NAME = "parameter name";
		public static final String PARAM_VALUE = "parameter value";
		public static final String RULE = "rule";
	}
	
	public final class  Domains
	{
		public static final String INTERFACE = "interface";
		public static final String INTERFACE_USAGE = "interface usage"; // a product usage is made of several interface usage
		public static final String PARAMETER = "parameter";
		
		public static final String DESIGN = "design";
		public static final String IMPLEMENTATION = "implementation";
		public static final String COMPONENT = "component";
		public static final String RELEASE = "release";
		
		public static final String PRODUCT = "external product";
		public static final String PRODUCT_USAGE = "external product usage";
		
		public static final String FOLDER = "folder"; 
		


	}
	
	public final class Types
	{
		public static final String FEATURE_MODEL = "feature model";
		public static final String FEATURE = "feature";
		public static final String CONFIGURATION="configuration";
		
		public static final String COMPOSITION_RULE = "rule";		
		public static final String CROSS_DESIGN_RULE = "cross-design rule"; //a rule is made of multiple constraints
		public static final String CROSS_MODEL_CONSTRAINT = "cross-model constraint";
	}
	
    public static enum RelTypes implements RelationshipType
    {
        CONTAINS,
        IS_CHARACTERIZED,
        REQUIRES,
        IMPLEMENTS,
        IS_A,
        CONSTRAINS,
        USES,
        
        MANDATORY, 
        OPTIONAL, 
        ALTERNATIVE,
        OR
    }
    
    
    public static class Constraint 
    {
    	public static final String MANDATORY = "mandatory";
    	public static final String OPTIONAL = "optional";
    	public static final String ALTERNATIVE = "alt";
    	public static final String OR = "or";	
    }
    
}
