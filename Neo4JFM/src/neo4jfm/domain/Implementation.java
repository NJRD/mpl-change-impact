package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

public class Implementation extends Entity {
	
	public Implementation(Node n)
	{
		super( n );
		
		//get the selected features names
		// TODO Auto-generated constructor stub
	}
	
	public Implementation()
	{
		
	}
	
	
	public String name = "";
	public String designName = "";
	
	
	public List<String> selectedFeatures = new ArrayList<String>();
	public List<String> unselectedFeatures = new ArrayList<String>();
	
	
	public IncompatibilityType errCode;
	
	
	public enum IncompatibilityType {
			INCOMPATIBLE_RELEASE,
			INCOMPLETE_SELECTION,
			RULE_VIOLATION,
			VALID,
			PRODUCT_CONSTRAINTS_VIOLATION_FORBIDDEN_FEATURE_INCLUDED,
			PRODUCT_CONSTRAINTS_VIOLATION_REQUIRED_FEATURE_MISSING,
			CROSS_DESIGN_INCOMPATIBILITY
			};
}
