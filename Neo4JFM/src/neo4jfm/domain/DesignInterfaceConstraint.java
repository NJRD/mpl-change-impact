package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;


/**
 * This class represents 1 combination of shared feature that 
 * enables 2 design to operate together.
 * 
 * @author Dante
 *
 */
public class DesignInterfaceConstraint extends Entity
{
	
	public DesignInterfaceConstraint()
	{
		
	}
	
	public DesignInterfaceConstraint(Node n)
	{
		super(n);
	}
	
	public List<String> requiredFeatureNames = new ArrayList<String>();
	public List<String> prohibitedFeatureNames = new ArrayList<String>();
	public List<Implementation> compliantImplementations = new ArrayList<Implementation>();
	
}
