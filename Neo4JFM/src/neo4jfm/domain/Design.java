package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import neo4jfm.operations.FMExporter;

import org.neo4j.graphdb.Node;

import de.ovgu.featureide.fm.core.FeatureModel;
import de.ovgu.featureide.fm.core.configuration.Configuration;
import de.ovgu.featureide.fm.core.configuration.Selection;


public class Design extends Entity
{

	public Design(Node n)
	{
		super( n );
	}
	
	public long nodeId = -1;
	public String name = "";
	
	public boolean isRoot = false;
	public boolean isGroup = false;
	
	
	/**
	 * Check that the cross-design constraints are up-to-date, 
	 * mark as invalid the configurations that do not work with the current design and so on.
	 * 
	 */
	public List<String> update()
	{
		
		FMExporter gen = new FMExporter();
		ArrayList<Long> ids = new ArrayList<Long>();
		ids.add( this.id );
		FeatureModel model = gen.CreateModelWithRoots(  ids , true );
		
		Map<String,List<String>> confs = nav.getDesignImplementations( nodeId );

		List<String> invalidConfNames = new ArrayList<String>();
		
		for(String confName : confs.keySet())
		{
			try{
				Configuration conf = new Configuration( model );
				for(String feat : confs.get( confName ))
				{
					conf.setManual( feat , Selection.SELECTED );
				}
			}
			catch(Exception e)
			{
				invalidConfNames.add( confName );
			}
		}
		
		//retrieve cross-design constraints
		
		
		
		
		return invalidConfNames;
				
		
		//instantiate FM based on database information
		//retrieve configurations
		//validate each one of them
		//keep only valid ones
		//find connected designs/interfaces

		//check valid configuration against cross design constraints
		//update cross-design constraints if none match
		//if cross-design constraints are modified, update touched design.
		
		
		
	}
	
	public List<Implementation> getImplementations()
	{
		Map<String,List<String>> confs1 = nav.getDesignImplementations( nodeId );
		return null;
		
	}
	

	public List<Implementation> implementations = new ArrayList<Implementation>();
	public List<DesignInterface> interfaces = new ArrayList<DesignInterface>();
	public List<Interface> implementedInterfaces = new ArrayList<Interface>();
	
}
