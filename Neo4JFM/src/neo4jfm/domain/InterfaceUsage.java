package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

/**
 * this class represent a concrete usage of an interface
 * (i.e. a configuration of interface parameters)
 * 
 * @author Dante
 *
 */
public class InterfaceUsage extends Entity {
	public String name = ""; 
	public List<String> features = new ArrayList<String>();
	public List<Interface> requiredInterfaces = new ArrayList<Interface>();
	public boolean isValid = false;
	
	public InterfaceUsage(Node n)
	{
		super(n);
	}
	
	public InterfaceUsage()
	{
		
	}
	
	public String report()
	{
		StringBuffer txt = new StringBuffer(200);
		txt.append("	++Interface usage: \""+ name + "\"\n");
		txt.append("	 Required interface parameters :\n");
		for(String s : features)
		{
			txt.append("		"+s + "\n");
		}
		
		for(Interface itf : requiredInterfaces)
		{
			txt.append(itf.report());
		}
		return txt.toString();
	}
	
	
	public boolean isSatisfied()
	{
		for(Interface itf : requiredInterfaces)
		{
			if (itf.isSatisfied())
				return false;
		}
		return true;
	}
}
