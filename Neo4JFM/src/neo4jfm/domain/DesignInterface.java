package neo4jfm.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Node;



/**
 * This class represent the constraints relative to the
 * compatibility of 2 designs. 
 * 
 * @author Dante
 *
 */
public class DesignInterface extends Entity
{
	public static enum InterfaceType {SYSTEM_INTERFACE_REQS, THIRD_PARTY_INTERFACE_REQS};
	
	private long id = -1;
	
	public DesignInterface(Node n)
	{
		super( n );
		// TODO Auto-generated constructor stub
	}
	List<DesignInterfaceConstraint> constraints = new ArrayList<DesignInterfaceConstraint>();
	
	
	/**
	 * creates the data in Neo4j representing the constraints between design d1 and d2
	 * 
	 * @param d1
	 * @param d2
	 * 
	 * 
	 */
	public void createConstraint(Design d1, Design d2)
	{
		
		Map<String, List<String>> shared1 = nav.getSharedComponentNames( d1.nodeId );
		List<String> sharedFeatureNames = null;
		if(!shared1.containsKey( d2.name ))
		{//no shared feature here. 
			return;
		}
		else
		{
			sharedFeatureNames = shared1.get( d2.name );
		}
			
		
		Map<String,List<String>> confs1 = nav.getDesignImplementations( d1.nodeId );
		
		List<DesignInterfaceConstraint> cs = new ArrayList<DesignInterfaceConstraint>();
		for(String s :  confs1.keySet())
		{
			DesignInterfaceConstraint c = getSharedFeatureState(  confs1.get( s ), sharedFeatureNames );
			
			
			if(c!=null)
				cs.add(c);
		}
		
		
		Map<String,List<String>> confs2 = nav.getDesignImplementations( d2.nodeId );

		Map<String, List<String>> shared2 = nav.getSharedComponentNames( d2.nodeId );
		return;
	}
	
	private DesignInterfaceConstraint getSharedFeatureState(List<String> conf,List<String> shared)
	{
		
		DesignInterfaceConstraint constraint = new DesignInterfaceConstraint(  );
		for(String feat : conf)
		{
			if(shared.contains( feat ))
			{
				constraint.requiredFeatureNames.add(feat);
			}
			else
			{
				constraint.prohibitedFeatureNames.add( feat );
			}
		}
		
		if(constraint.prohibitedFeatureNames.size() == 0 && constraint.requiredFeatureNames.size() == 0)
			return null;
	//	else
	//		constraint.compliantImplementations.add(conf);
		
		
		return constraint;
	}
}
