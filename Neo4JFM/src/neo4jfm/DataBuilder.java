package neo4jfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Constraint;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.featuremodel.NeoFeature;
import neo4jfm.utils.GraphEditor;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;



public class DataBuilder {

	GraphDatabaseService database = null;
	GraphEditor editor = null;

	public DataBuilder(GraphDatabaseService db) {
		database = db;
		editor = new GraphEditor(db);
	}

	public void createInterfaceSample() {
		Transaction tx = database.beginTx();
		try {
			createobjects();
			tx.success();
		} catch (Exception e) {
			System.err.println("failed to create interface samples");
		}
		tx.close();
	}
	

	private void createobjects() {
			
		Map<String, String> interface_props = getInterfaceProperties();
		Map<String,String> folder_prop = getFMFolderProperties();
		Map<String,String> product_usage_prop = getProductUsageProperties();
		Map<String,String> interface_usage_prop = getInterfaceUsageProperties();
		
		///////////////// interface declaration
		
		//video export interface creation: video signal + connectivity with parameters
		Node video_export_interface = editor.createNode("video export", interface_props);
		Node video_signal_interface = editor.createNode("Video signal", folder_prop);
		connectFolderToFM(video_export_interface, video_signal_interface);
			
		Node resolution_folder = editor.createNode( "resolutions" , folder_prop );
		connectFolderToFM(video_signal_interface, resolution_folder);
		//3 resolutions parameters
		Node high_resolution = editor.createNode("high resolution video export",getInterfaceParameterProperties( "resolution" , "1600x1200" ));
		connectSubFeature(resolution_folder, high_resolution, Constraint.OPTIONAL);
		Node mid_resolution = editor.createNode("mid resolution video export",getInterfaceParameterProperties( "resolution" , "1024x768" ));
		connectSubFeature(resolution_folder, mid_resolution, Constraint.OPTIONAL);
		Node low_resolution = editor.createNode("low resolution video export",getInterfaceParameterProperties( "resolution" , "800x600" ));
		connectSubFeature(resolution_folder, low_resolution, Constraint.OPTIONAL);
		//refresh rate parameters
		Node refreshRate_folder = editor.createNode( "refresh rate" , folder_prop );
		connectFolderToFM(video_signal_interface, refreshRate_folder);
		Node refresh_rate_standard = editor.createNode("refresh rate: 60Hz",getInterfaceParameterProperties( "refresh rate" , "60Hz" ));
		connectSubFeature(refreshRate_folder, refresh_rate_standard, Constraint.OPTIONAL);
		
		//connectivity folder
		Node video_connectivity_interface = editor.createNode("Video export connectivity", folder_prop);
		connectFolderToFM(video_export_interface, video_connectivity_interface);
		//2 input parameters
		Node dvi_input = editor.createNode("dvi input format",getInterfaceParameterProperties( "input" , "mini-dvi-rx" ));
		connectSubFeature(video_connectivity_interface , dvi_input, Constraint.OR);
		Node rj_input = editor.createNode("dvi input format",getInterfaceParameterProperties( "input" , "rj45" ));
		connectSubFeature(video_connectivity_interface , rj_input, Constraint.OR);
		
		
		//video display mode interface: screen types
		Node display_interface = editor.createNode("display modes", interface_props);
		Node dedicated_display = editor.createNode("display mode: dedicated display",getInterfaceParameterProperties( "display model" , "dedicated" ));
		connectSubFeature(display_interface, dedicated_display, Constraint.ALTERNATIVE);
		Node shared_display = editor.createNode("display mode: shared display",getInterfaceParameterProperties( "display model" , "shared" ));
		connectSubFeature(display_interface, shared_display, Constraint.ALTERNATIVE);
		Node flexvision_display = editor.createNode("display mode: flexvision display",getInterfaceParameterProperties( "display model" , "flexvision" ));
		connectSubFeature(display_interface, flexvision_display, Constraint.ALTERNATIVE);
		
		
		///////////////// 3rd party product definitions
		// Magellan product family
		Node magellan_product = editor.createNode("Magellan", getProductProperties());
		//EU usage and interface usage declaration
		Node magellan_eu_usage= editor.createNode("Magellan-EU", product_usage_prop);
		connectUsageToProduct( magellan_product , magellan_eu_usage );

		Node mag_eu_video_signal_usage = editor.createNode( "magellan_eu video export requirements" , interface_usage_prop );
		connectProductUsageToInterfaceUsage( magellan_eu_usage , mag_eu_video_signal_usage );
		
		Node mag_us_video_signal_usage = editor.createNode( "magellan_us video export requirements" , interface_usage_prop );
		connectProductUsageToInterfaceUsage( mag_us_video_signal_usage , mag_eu_video_signal_usage );
		
		connectInterfaceUsageToUsedParameter(magellan_eu_usage,high_resolution);
		
		//US usage and interface usage declaration
		Node magellan_us_usage= editor.createNode("Magellan-US", product_usage_prop);
		connectUsageToProduct( magellan_product , magellan_us_usage );
		
	}
	
	
	private void connectInterfaceUsageToUsedParameter(Node interface_usage, Node interface_parameter)
	{
		interface_usage.createRelationshipTo(interface_parameter, RelTypes.CONTAINS);
	}

	private void connectSubFeature(Node feature1, Node feature2, String constraint)
	{
		Relationship rel = feature1.createRelationshipTo(feature2, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, constraint);
	}
	
	private void connectUsageToProduct(Node product, Node product_usage)
	{
		product.createRelationshipTo(product_usage, RelTypes.CONTAINS);
	}
	
	private void connectProductUsageToInterfaceUsage(Node product_usage, Node interface_usage)
	{
		product_usage.createRelationshipTo(interface_usage, RelTypes.CONTAINS);
	}

	private void connectFolderToFM(Node fm, Node folder)
	{
		Relationship rel = fm.createRelationshipTo(folder, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.MANDATORY);
	}
	
	private Map<String,String> getInterfaceProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.INTERFACE);
		props.put(Attributes.TYPE, Types.FEATURE_MODEL);
		return props;
	}
	
	private Map<String,String> getProductProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PRODUCT);
		props.put(Attributes.TYPE, "");
		return props;
	}
	
	private Map<String,String> getProductUsageProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PRODUCT_USAGE);
		props.put(Attributes.TYPE, "");
		return props;
	}
	
	private Map<String,String> getInterfaceUsageProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.INTERFACE_USAGE);
		props.put(Attributes.TYPE, Types.CONFIGURATION);
		return props;
	}
	
	private Map<String,String> getFMFolderProperties()
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.FOLDER);
		props.put(Attributes.TYPE, Types.FEATURE);
		return props;
	}
	
	private Map<String,String> getInterfaceParameterProperties(String parameter_name, String parameter_value)
	{
		Map<String,String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.PARAMETER);
		props.put(Attributes.TYPE, Types.FEATURE);
		props.put(Attributes.PARAM_NAME,parameter_name);
		props.put(Attributes.PARAM_VALUE, parameter_value);
		return props;
	}

	private void makeDesignConf(Node design, String name, Node[] items) {
		
		Map <String, String> implProp = new HashMap<String,String>();
		implProp.put(Attributes.DOMAIN, Domains.IMPLEMENTATION);

		Node conf = editor.createNode(name, implProp); 
		conf.createRelationshipTo(design, RelTypes.IS_A);
		
		for(int i = 0 ; i < items.length; i++)
		{
			conf.createRelationshipTo(items[i], RelTypes.CONTAINS);
		}
	}

}
