package neo4jfm.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

public class InterfaceTree implements TreeModel{

	Node root = null;
	GraphNavigator nav = null;
	public InterfaceTree()
	{
		super();

		nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
		root = nav.getInterfaceRootNode();
		
	}
	@Override
	public Object getRoot() {
		return new UITreeNode(root);
	}

	@Override
	public Object getChild(Object parent, int index) {
		Node node =  ((UITreeNode) parent).n;
		
		List<Node> nodes = nav.getPointedNodes(node,RelTypes.CONTAINS);
		nodes.addAll(nav.getPointedNodes(node,RelTypes.IS_CHARACTERIZED));
		
		return new UITreeNode (nodes.get(index));
		
	}
	

	@Override
	public int getChildCount(Object parent) {
		Node node = ( (UITreeNode) parent).n;	
		int nb =  nav.getPointedNodes(node,RelTypes.CONTAINS).size() + nav.getPointedNodes(node,RelTypes.IS_CHARACTERIZED).size();
		return nb;
	}

	@Override
	public boolean isLeaf(Object node) {
		Node parent = ((UITreeNode) node).n;
		boolean leaf = ( ! nav.hasRel(parent, RelTypes.CONTAINS, Direction.OUTGOING) &&
						!nav.hasRel(parent, RelTypes.IS_CHARACTERIZED, Direction.OUTGOING) );
		
		return leaf;
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {

		UITreeNode node = (UITreeNode) parent;		
		UITreeNode childNode = (UITreeNode) child;
		List<Node> nodes = nav.getPointedNodes(node.n,RelTypes.CONTAINS);
		nodes.addAll(nav.getPointedNodes(node.n,RelTypes.IS_CHARACTERIZED));
		return nodes.indexOf(childNode);
	}

	
	// interface parameters
	
	public Object getParamAt(Object parent, int index)
	{
		Node node =  ((UITreeNode) parent).n;
		return new UITreeNode (nav.getPointedNodes(node,RelTypes.IS_CHARACTERIZED).get(index));
	}
	
	public int getParamCount(Object parent)
	{
		Node node = ( (UITreeNode) parent).n;	
		int nb =  nav.getPointedNodes(node,RelTypes.IS_CHARACTERIZED).size();		
		return nb;
	}
	
	//3rd party product using this elements
	
	public int getProductCount(UITreeNode treeNode)
	{
		 return nav.getPointingNodes(treeNode.n, RelTypes.REQUIRES).size();
	}
	
	public Object getUsingProductAt(UITreeNode o, int idx)
	{
		return new UITreeNode (nav.getPointingNodes(o.n,RelTypes.REQUIRES).get(idx));
	}
	
	public String getParamaterUsageInfo(UITreeNode treeNode)
	{
	
		Node node =  treeNode.n;
		
		//first we get the usages
		List<Node> usages = nav.getPointingNodes(node, RelTypes.REQUIRES);
		
		Map<UITreeNode,List<UITreeNode>> usageInfoMap = new HashMap<UITreeNode,List<UITreeNode>>();
		
		for(Node usage : usages)
		{
			UITreeNode use = new UITreeNode(usage);
			List<Node> products = nav.getPointedNodes(usage, RelTypes.IS_A); //there can be only one. 
			UITreeNode product = new UITreeNode(products.get(0));
			
			UITreeNode existingProduct = null;
			for(UITreeNode n : usageInfoMap.keySet())
			{
				if(n.getAttributes().get("name").equalsIgnoreCase(product.getAttributes().get("name")))
					existingProduct = n;
			}
			
			if(existingProduct != null)
			{
				if(!usageInfoMap.get(existingProduct).contains(use))
						usageInfoMap.get(existingProduct).add(use);
			}
			else
			{
				List<UITreeNode> newUsageList = new ArrayList<UITreeNode>();
				newUsageList.add(use);
				usageInfoMap.put(product, newUsageList);
			}
		}
		
		StringBuffer info = new StringBuffer(200);
		for(UITreeNode product : usageInfoMap.keySet())
		{
			info.append("\n");
			info.append("Product \"" + product.getAttributes().get("name")+ "\" requires this parameter in ("+usageInfoMap.get(product).size()+") working mode(s) : \n");
			for(UITreeNode usage : usageInfoMap.get(product))
			{
				info.append("	---\n");
				Map<String,String> attributes = usage.getAttributes();
				for(String s : attributes.keySet())
				{
					
					info.append("	"+s+": "+attributes.get(s)+"\n");
				}
			}
		}
		
		return info.toString();
	}
	
	public String getInterfaceImpl(UITreeNode gn)
	{
		StringBuffer info = new StringBuffer(200);
		
		String nodeType = gn.getAttributes().get( Attributes.DOMAIN );
		List<String> infos = null;
		if(Domains.INTERFACE.equals( nodeType ))
		{ //this might not be the root element of the interface.
				infos = nav.getInterfaceImplementation(gn.n);
		}
		else if (Domains.PARAMETER.equals( nodeType ))
		{	//find design and implementations
			infos= nav.getParameterImplementation(gn.n);
		}
		
		for(String s : infos)
		{
			info.append(" "+s+"\n");
		}
		
		return info.toString();
	}
	
	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}
	
	public Map<String, String> getNodeAttrs(Node n) {
		
		return nav.getAttributes(n);
	}

	

}
