package neo4jfm.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.featuremodel.CFMCChangeReport;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.operations.Scenarios;
import neo4jfm.operations.UpdateSimulator;
import net.miginfocom.swing.MigLayout;

import org.eclipse.wb.swing.FocusTraversalOnArray;

// import test.TraceLog;



import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JSplitPane;

import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class Neo4JFM_Viewer
{

	private JFrame		frmrdPartyIntegration;
	private JTextField import_path;
	private JTextField txtFeatureName;
	private JTextField txtModelModel;

	/**
	 * Launch the application.
	 */
	public static void run()
	{

		EventQueue.invokeLater( new Runnable()
		{

			public void run()
			{
				try
				{
					Neo4JFM_Viewer window = new Neo4JFM_Viewer();
					window.frmrdPartyIntegration.setVisible( true );
				}
				catch ( Exception e )
				{
					e.printStackTrace();
				}
			}
		} );
	}

	/**
	 * Create the application.
	 */
	public Neo4JFM_Viewer()
	{

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmrdPartyIntegration = new JFrame();
		frmrdPartyIntegration.setForeground( Color.WHITE );
		frmrdPartyIntegration.getContentPane().setForeground( Color.WHITE );
		frmrdPartyIntegration.setBackground( Color.WHITE );

		frmrdPartyIntegration.addWindowListener( new WindowAdapter()
		{

			@Override
			public void windowClosing(WindowEvent e)
			{

				ExternalResourcesRepository.shutDown();
			}
		} );
		frmrdPartyIntegration.setTitle( "Feature change impact computation on Multi product line" );
		frmrdPartyIntegration.getContentPane().setBackground( Color.WHITE );
		frmrdPartyIntegration.setBounds( 100 , 100 , 1100 , 596 );
		frmrdPartyIntegration.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frmrdPartyIntegration.getContentPane().setLayout(
				new MigLayout("", "[grow,fill]", "[grow,fill][grow]") );

		JTabbedPane tab_main = new JTabbedPane( JTabbedPane.TOP );
		tab_main.setTabLayoutPolicy( JTabbedPane.SCROLL_TAB_LAYOUT );
		tab_main.setForeground( new Color( 0 , 0 , 0 ) );
		tab_main.setBackground( new Color( 255 , 255 , 255 ) );
		frmrdPartyIntegration.getContentPane().add( tab_main , "cell 0 0 1 2,grow" );
		
		JPanel SimulatorTab = new JPanel();
		tab_main.addTab("Feature Change Impact Simulator", null, SimulatorTab, null);
		tab_main.setEnabledAt(0, true);
		SimulatorTab.setLayout(null);
		
		import_path = new JTextField();
		import_path.setText("/path/to/models");
		import_path.setBounds(266, 10, 275, 28);
		SimulatorTab.add(import_path);
		import_path.setColumns(10);
		
		JButton btnImport = new JButton("Import");

		btnImport.setBounds(553, 39, 117, 29);
		SimulatorTab.add(btnImport);
		
		JLabel lblImportPathFor = new JLabel("Model names (coma seprated)");
		lblImportPathFor.setBounds(6, 44, 248, 16);
		SimulatorTab.add(lblImportPathFor);
		
		JLabel lblNameOfThe = new JLabel("Name of the feature to remove");
		lblNameOfThe.setBounds(6, 94, 248, 16);
		SimulatorTab.add(lblNameOfThe);
		
		txtFeatureName = new JTextField();
		txtFeatureName.setText("feature name");
		txtFeatureName.setBounds(266, 88, 275, 28);
		SimulatorTab.add(txtFeatureName);
		txtFeatureName.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 144, 1041, 348);
		SimulatorTab.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));
		
		final JTree fm_change_tree = new JTree();
		fm_change_tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Changes will be displayed here.") {
				{
				}
			}
		));
		panel_2.add(fm_change_tree);
		
		
		final JTextPane fm_change_details = new JTextPane();
		panel_2.add(fm_change_details);
		
		txtModelModel = new JTextField();
		txtModelModel.setText("model1, model2,...");
		txtModelModel.setColumns(10);
		txtModelModel.setBounds(266, 38, 275, 28);
		SimulatorTab.add(txtModelModel);
		
		fm_change_tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				
				TreePath p = e.getNewLeadSelectionPath();
				if ( p == null )
					return;

				Object o = p.getLastPathComponent();
				
				ChangeNode n = (ChangeNode)o;
				fm_change_details.setText( "change details : " + n.detais() );
			}
		});
		
		
		btnImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//import model into db.
				String folder_name = import_path.getText();
				
				File path = new File(folder_name);
				if (!path.isDirectory())
					fm_change_details.setText("The path given for the root folder is not a folder!");
				
				if(!folder_name.endsWith("/"))
					folder_name +="/";
				
				String models = txtModelModel.getText();
				String[] modelNames = models.split(",");
				
				for(String m : modelNames)
				{
					String mPath = folder_name + m.trim();
					FMImporter importer = new FMImporter(mPath);
					importer.importIntoNeo4J();
				}
			}
		});
		
		JButton btnSimulate = new JButton("Simulate");
		btnSimulate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChangeRepository.getRepo().clear_repo();
				String feat = txtFeatureName.getText();
				List<ConfChangeReport> rs  = NeoFM.simulateFeatureRemoval(feat);
				CFMCChangeReport r= new CFMCChangeReport(); 
				r.setFm1( "removal" );
				r.setFm2( feat );
				r.setPropagation( rs );
				ChangeTree t = new ChangeTree( r );
				fm_change_tree.setModel( t );
			}
		});
		btnSimulate.setBounds(553, 89, 117, 29);
		SimulatorTab.add(btnSimulate);
		
		JLabel label = new JLabel("Path to model root folder");
		label.setBounds(6, 16, 248, 16);
		SimulatorTab.add(label);
		frmrdPartyIntegration.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{frmrdPartyIntegration.getContentPane(), tab_main}));

	}
}
