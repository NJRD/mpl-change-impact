package neo4jfm.ui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;


public class UITreeNode extends DefaultMutableTreeNode
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Node n;
	Map<String,String> attributes = new HashMap<String,String>();
	private GraphNavigator nav = new GraphNavigator(ExternalResourcesRepository.graphDb);
	
	public UITreeNode(Node n)
	{
		this.n = n;
		
		if(n!=null)
			init();
		else
			attributes.put( "name" , "no value found" );
	}
	
	private void init()
	{
		attributes = nav.getAttributes(n);
	}
	
	public String toString()
	{
		return attributes.get("name");
	}
	
	public Map<String,String> getAttributes()
	{
		return attributes;
	}
}