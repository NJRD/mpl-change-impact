package neo4jfm.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.featuremodel.CFMCChangeReport;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

public class ChangeTree implements TreeModel{

	ChangeNode root = null;
	CFMCChangeReport cfmc_root = null;
	
	GraphNavigator nav = null;
	public ChangeTree(ConfChangeReport r)
	{
		super();
		root = new ChangeNode(r);
	}
	
	public ChangeTree(CFMCChangeReport r)
	{
		super();
		root = new ChangeNode(r);
	}
	
	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public Object getChild(Object parent, int index) {
		
		ChangeNode parent_node = (ChangeNode) parent;
		return parent_node.getChildAt( index );
	}
	

	@Override
	public int getChildCount(Object parent) {
		ChangeNode parent_node = (ChangeNode) parent;
		return parent_node.getChildCount();
	}

	@Override
	public boolean isLeaf(Object node) {
		ChangeNode obj = (ChangeNode) node;
		return obj.isLeaf();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {

		ChangeNode p = (ChangeNode) parent;
		ChangeNode c = (ChangeNode) child;
		
		return p.getIndex( c );
	}

	
	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}
	
	public Map<String, String> getNodeAttrs(Node n) {
		
		return nav.getAttributes(n);
	}

	

}
