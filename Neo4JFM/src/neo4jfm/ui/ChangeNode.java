package neo4jfm.ui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.featuremodel.CFMCChangeReport;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.utils.GraphNavigator;

import org.neo4j.graphdb.Node;


public class ChangeNode extends DefaultMutableTreeNode
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ConfChangeReport conf_change;
	private CFMCChangeReport cfmc_change;
	
	public ChangeNode(ConfChangeReport r)
	{
		conf_change = r;
	}
	
	public ChangeNode(CFMCChangeReport r)
	{
		cfmc_change = r;
	}
	
	public boolean isRoot()
	{
		if(conf_change != null)
		{
			if (conf_change.getCause() != null)
				return false;
			else
				return true;
		}
		else if (cfmc_change!=null)
		{
			if (cfmc_change.getCause() != null)
				return false;
			else
				return true;
			
		}
		return false;
	}
	
	public String toString()
	{
		String message = "";
		
		if (isRoot())
		{
			if(cfmc_change!=null)
				return message = "operation: " + cfmc_change.getFm1() + " of " +cfmc_change.getFm2();
//			else if (conf_change!=null)
//				return message="feature removal effects";
		}
		
		if(conf_change != null)
		{
			message = "Configuration changes in model " + conf_change.fm;
		}
		else if (cfmc_change!=null)
		{
			message = "Shared feature constraint change between "+ cfmc_change.getFm1() + " and " +cfmc_change.getFm2();
		}

		
		return message;
	}
	
	
	public ChangeNode getChildAt(int i )
	{
		ChangeNode node = null;
		if(null != conf_change)
		{
			CFMCChangeReport r = conf_change.getPropagation().get( i );
			node = new ChangeNode(r);
		}
		else if (null != cfmc_change)
		{
			ConfChangeReport r = cfmc_change.getPropagation().get( i );
			node = new ChangeNode(r);
		}
		else
			return null;
		
		return node;
	}
	
	public int getChildCount()
	{
		if(null != conf_change)
		{
			return conf_change.getPropagation().size();
		}
		else if (null != cfmc_change)
		{
			return cfmc_change.getPropagation().size();
		}
		else
			return 0;
	}
	
	public boolean isLeaf()
	{
		if(null != conf_change)
		{
			if ( conf_change.getPropagation().size() == 0)
				return true;
			else
				return false;
		}
		else if (null != cfmc_change)
		{
			if (cfmc_change.getPropagation().size() == 0)
				return true;
			else
				return false;
		}
		//default, not sure what we are doing, let's assume it does not have any leaves.
		return true;
	}
	
	public int IndexOf(ChangeNode n)
	{
		if(null != n.cfmc_change)
		{
			CFMCChangeReport r = n.cfmc_change;
			if(conf_change != null)
				return -1;
			else
			{
				return conf_change.getPropagation().indexOf( r);
			}
		}
		else if (n.conf_change != null)
		{
			ConfChangeReport r = conf_change;
			if(cfmc_change != null)
				return -1;
			else
			{
				return cfmc_change.getPropagatedChanges().indexOf( r);
			}
		}
		
		return 0;
	}

	public String detais()
	{
		if(null != cfmc_change)
		{
//			if(isRoot())
//			{
//				return  ("simulation of the " + cfmc_change.getFm1() + " of " +cfmc_change.getFm2() );
//			}
//			else
				return cfmc_change.details();
		}
		else if (conf_change != null)
		{
			return conf_change.details();

		} 
		return null;
	}
	
}