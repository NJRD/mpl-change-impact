package neo4jfm.reports;

import java.util.ArrayList;
import java.util.List;


public class CompatibilityConstraint
{
	public List<String> requiredFeatures = new ArrayList<String>();
	public List<String> forbiddenFeatures = new ArrayList<String>();
	public List<String> expressions = new ArrayList<String>();
	
}
