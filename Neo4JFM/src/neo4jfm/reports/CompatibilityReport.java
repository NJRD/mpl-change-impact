package neo4jfm.reports;

import java.util.ArrayList;
import java.util.List;

import neo4jfm.domain.Implementation;




public class CompatibilityReport {
	public List<Implementation> compatibles = new ArrayList<Implementation>();
	public List<Implementation> incompatibles = new ArrayList<Implementation>();
}
