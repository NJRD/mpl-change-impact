package neo4jfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Constraint;
import neo4jfm.domain.Model.Domains;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.utils.GraphEditor;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;



public class DBInitializer {

	GraphDatabaseService database = null;
	GraphEditor editor = null;

	public DBInitializer(GraphDatabaseService db) {
		database = db;
		editor = new GraphEditor(db);
	}

	public void createInterfaceSample() {
		Transaction tx = database.beginTx();
		try {
			createobjects();
			tx.success();
		} catch (Exception e) {
			System.err.println("failed to create interface samples");
		}
		tx.close();
	}
	

	private void createobjects() {
		
		///INTERFACES
		Map<String, String> props = new HashMap<String, String>();
		props.put(Attributes.DOMAIN, Domains.INTERFACE);

		Node root = editor.createNode("External interfaces", props);

		Node video_export_interface = editor.createNode("video export", props);
		Relationship rel = root.createRelationshipTo(video_export_interface, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);

		Node video_signal_interface = editor.createNode("Video signal", props);
		Node video_connectivity_interface = editor.createNode("Video export connectivity", props);

		rel = video_export_interface.createRelationshipTo(video_signal_interface, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.MANDATORY);
		rel = video_export_interface.createRelationshipTo(video_connectivity_interface, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.MANDATORY);

			//INTERFACE PARAMETERS
			Map<String, String> props2 = new HashMap<String, String>();
			props2.put(Attributes.DOMAIN, Domains.PARAMETER);
			props2.put(Attributes.PARAM_NAME, "resolution");
			props2.put(Attributes.PARAM_VALUE, "1600x1200");
	
			Node param1600 = editor.createNode("Video export resolution: 1600x1200",props2);
			rel = video_signal_interface.createRelationshipTo(param1600,RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
			props2.put(Attributes.PARAM_VALUE,"800x600");
			Node param800 = editor.createNode("Video export resolution : 800x600",props2);
			rel = video_signal_interface.createRelationshipTo(param800, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
			props2.put(Attributes.PARAM_VALUE,"1024x768");
			Node param1024 = editor.createNode("Video export resolution: 1024x768",props2);
			rel = video_signal_interface.createRelationshipTo(param1024, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
			props2.clear();
			props2.put(Attributes.DOMAIN, Domains.PARAMETER);
			props2.put(Attributes.PARAM_NAME, "refresh rate");
			props2.put(Attributes.PARAM_VALUE, "60hz");
			
			Node refresh = editor.createNode("Refresh rate",props2);
			rel = video_signal_interface.createRelationshipTo(refresh, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
		
			props2.clear();
			props2.put(Attributes.DOMAIN, Domains.PARAMETER);
			props2.put(Attributes.PARAM_NAME, "input_type");
			props2.put(Attributes.PARAM_VALUE,"mini-dvi-rx");
			
			Node dviInput = editor.createNode("Video export connection: dvi", props2);
			rel = video_connectivity_interface.createRelationshipTo(dviInput, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
			props2.put(Attributes.PARAM_VALUE,"RJ45");
			Node rj45 = editor.createNode("Video export connection: rj45", props2);
			rel = video_connectivity_interface.createRelationshipTo(rj45, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
			Node displayModeInterface = editor.createNode("display modes",props);
			rel = root.createRelationshipTo(displayModeInterface, RelTypes.CONTAINS);
			rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
			
				props2.put(Attributes.PARAM_VALUE,"Dedicated monitor");
				Node direct = editor.createNode("Dedicated monitor for 3rd party product", props2);
				rel = displayModeInterface.createRelationshipTo(direct, RelTypes.CONTAINS);
				rel.setProperty(Attributes.CHOICE_TYPE, Constraint.ALTERNATIVE);
				
				props2.put(Attributes.PARAM_VALUE, "Shared monitor");
				Node sharing = editor.createNode("Shared monitor for 3rd party product", props2);
				rel = displayModeInterface.createRelationshipTo(sharing, RelTypes.CONTAINS);
				rel.setProperty(Attributes.CHOICE_TYPE, Constraint.ALTERNATIVE);
				
				props2.put(Attributes.PARAM_VALUE, "Flexvision");
				Node flex = editor.createNode("Flexvision for 3rd party product", props2);
				rel = displayModeInterface.createRelationshipTo(flex, RelTypes.CONTAINS);
				rel.setProperty(Attributes.CHOICE_TYPE, Constraint.ALTERNATIVE);

		///EXTERNAL PRODUCTS
		Map<String, String> prodProps = new HashMap<String, String>();
		prodProps.put(Attributes.DOMAIN, Domains.PRODUCT);
		Node rootProductNode = editor.createNode("External products", props);
		
		Node magellanFamilly = editor.createNode("Magellan", prodProps);
		rel = rootProductNode.createRelationshipTo(magellanFamilly, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.OPTIONAL);
		
		Node magellanEU = editor.createNode("Magellan-CE", prodProps);
		rel = magellanFamilly.createRelationshipTo(magellanEU, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.MANDATORY);
		
		Node magellanUS = editor.createNode("Magellan-USA", prodProps);
		rel = magellanFamilly.createRelationshipTo(magellanUS, RelTypes.CONTAINS);
		rel.setProperty(Attributes.CHOICE_TYPE, Constraint.MANDATORY);
		
		magellanUS.createRelationshipTo(video_export_interface, RelTypes.USES);
		magellanEU.createRelationshipTo(video_export_interface, RelTypes.USES);
		magellanEU.createRelationshipTo(displayModeInterface, RelTypes.USES);
		
		/// EXTERNAL PRODUCT USAGE
		Map<String,String> usageProp = new HashMap<String,String>();
		usageProp.put(Attributes.DOMAIN, Domains.PRODUCT_USAGE);
		Node magellanEUUsage = editor.createNode("Magellan EU Usage standard", usageProp);
		magellanEUUsage.createRelationshipTo(magellanEU, RelTypes.IS_A);
		
		Map<String,String> interface_usage_prop = new HashMap<String,String>();
		interface_usage_prop.put( Attributes.DOMAIN, Domains.INTERFACE_USAGE);
		//interface_usage_prop.put( Attributes.TYPE , T)
		
		magellanEUUsage.createRelationshipTo(param1600, RelTypes.REQUIRES);
		magellanEUUsage.createRelationshipTo(refresh, RelTypes.REQUIRES);
		magellanEUUsage.createRelationshipTo(dviInput, RelTypes.REQUIRES);
		magellanEUUsage.createRelationshipTo(flex, RelTypes.REQUIRES);
		
		
		Node magellanUSAUsageLowDef = editor.createNode("Magellan USA Usage low definition", usageProp);
		magellanUSAUsageLowDef.createRelationshipTo( magellanUS, RelTypes.IS_A);
		magellanUSAUsageLowDef.createRelationshipTo(param1024, RelTypes.REQUIRES);
		magellanUSAUsageLowDef.createRelationshipTo(refresh, RelTypes.REQUIRES);
		magellanUSAUsageLowDef.createRelationshipTo(dviInput, RelTypes.REQUIRES);
		
		
		Node magellanUSAUsageHD = editor.createNode("Magellan USA Usage high definition", usageProp);
		magellanUSAUsageHD.createRelationshipTo(magellanUS, RelTypes.IS_A);
		magellanUSAUsageHD.createRelationshipTo(param1600, RelTypes.REQUIRES);
		magellanUSAUsageHD.createRelationshipTo(refresh, RelTypes.REQUIRES);
		magellanUSAUsageHD.createRelationshipTo(dviInput, RelTypes.REQUIRES);
		


		//RELEASE
		Map<String,String> releaseProp  = new HashMap<String,String>();
		releaseProp.put(Attributes.DOMAIN, Domains.RELEASE);
		Node release81 = editor.createNode("8.1", releaseProp);
		Node release82 = editor.createNode("8.2", releaseProp);

		//COMPONENTS
		List<Node> comps81 = new ArrayList<Node>();
		List<Node> comps82 = new ArrayList<Node>();
		
		Map<String,String> compoProp = new HashMap<String,String>(); 
		compoProp.put(Attributes.DOMAIN, Domains.COMPONENT);
		
		Node wcb1 = editor.createNode("w.c.b.1", compoProp);
		Node wcb2 = editor.createNode("w.c.b.2", compoProp);
		comps81.add(wcb1);
		comps81.add(wcb2);
		
		Node utp = editor.createNode("utp 30m", compoProp);
		Node fiber = editor.createNode("fiber 30m", compoProp);
		comps81.add(utp);
		comps81.add(fiber);
		
		Node wmb = editor.createNode("w.m.b",compoProp);
		Node split = editor.createNode("Splitter 5->1", compoProp);
		Node coupling = editor.createNode("Coupling box", compoProp);
		Node rx = editor.createNode("rx shelves", compoProp);
		Node flexComponent = editor.createNode("flexvision", compoProp);
		comps81.add(wmb);
		comps81.add(coupling);
		comps81.add(split);
		comps81.add(rx);
		comps81.add(flexComponent);

		Node philips19 = editor.createNode("19 inches Philips screen", compoProp);
		Node philips21 = editor.createNode("21 inches Philips screen", compoProp);
		Node philips56 = editor.createNode("56 inches Philips screen", compoProp);
		Node thirdPartyScreen = editor.createNode("3rd party screen free position", compoProp);

		comps81.add(philips19);
		comps81.add(philips21);
		comps81.add(philips56);
		comps81.add(thirdPartyScreen);

		comps82.addAll(comps81);
		comps82.remove(wcb2);
		for(Node n : comps81)
		{
			release81.createRelationshipTo(n, RelTypes.CONTAINS);
		}
		
		for(Node n : comps82)
		{
			release82.createRelationshipTo(n, RelTypes.CONTAINS);
		}
		
		//DESIGNS
		
			//VIDEO CHAIN
			Map<String,String> designProps = new HashMap<String,String>();
			designProps.put(Attributes.DOMAIN, Domains.DESIGN);
			
			Node videoChain = editor.createNode("video chain", designProps);
			videoChain.createRelationshipTo(video_export_interface, RelTypes.IMPLEMENTS); //implements "video export"
			
			Node boxes = editor.createNode("connection boxes", designProps);
			Relationship r = videoChain.createRelationshipTo(boxes, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);
			r = boxes.createRelationshipTo(wcb1, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = boxes.createRelationshipTo(wcb2, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			
			Node cables = editor.createNode("cables", designProps);
			r = videoChain.createRelationshipTo(cables, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);
			r = cables.createRelationshipTo(utp, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.OR);
			r = cables.createRelationshipTo(fiber, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.OR);
			
			Node storage = editor.createNode("storage", designProps);
			r = videoChain.createRelationshipTo(storage, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.OPTIONAL);
			r = storage.createRelationshipTo(wmb, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = storage.createRelationshipTo(rx, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = storage.createRelationshipTo(coupling, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			
			Node signalProcessing = editor.createNode("signal processing", designProps);
			r = videoChain.createRelationshipTo(signalProcessing, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.OPTIONAL);
			r = signalProcessing.createRelationshipTo(flexComponent, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = signalProcessing.createRelationshipTo(split, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);

			//DISPLAY MODES
			Node displayDesign = editor.createNode("display", designProps);
			displayDesign.createRelationshipTo(displayModeInterface, RelTypes.IMPLEMENTS); //implements "video export"

			Node displayDesignScreens = editor.createNode("display design screens",designProps);
			r = displayDesign.createRelationshipTo(displayDesignScreens, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);

			r = displayDesignScreens.createRelationshipTo(philips19, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = displayDesignScreens.createRelationshipTo(philips21, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = displayDesignScreens.createRelationshipTo(philips56, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = displayDesignScreens.createRelationshipTo(thirdPartyScreen, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);

			Node displayModes = editor.createNode("display design modes",designProps);
			r = displayDesign.createRelationshipTo(displayModes, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);

			Node dedicatedMonitorMode = editor.createNode("dedicated monitor mode", designProps);
			Node sharedMonitorMode = editor.createNode("shared monitor mode", designProps);
			Node flexvisionMode = editor.createNode("flexvision mode", designProps);

			r = sharedMonitorMode.createRelationshipTo(split, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);
			r = flexvisionMode.createRelationshipTo(flexComponent, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.MANDATORY);

			r = displayModes.createRelationshipTo(dedicatedMonitorMode, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = displayModes.createRelationshipTo(sharedMonitorMode, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);
			r = displayModes.createRelationshipTo(flexvisionMode, RelTypes.CONTAINS);
			r.setProperty(Attributes.DOMAIN, Constraint.ALTERNATIVE);

			Map<String, String> ruleProps = new HashMap<String,String>();
			ruleProps.put(Attributes.DOMAIN,Types.COMPOSITION_RULE);

			Node flexvisionScreenRule = editor.createNode("screen compatibility for flexvision", ruleProps);
			flexvisionScreenRule.setProperty(Attributes.RULE, "\"flexvision mode\" implies \"56 inches Philips screen\"");
			flexvisionScreenRule.createRelationshipTo(displayDesign, RelTypes.CONSTRAINS);

			Node sharedScreenRules= editor.createNode("screen compatibility for shared display", ruleProps);
			sharedScreenRules.setProperty(Attributes.RULE, "\"shared monitor mode\" implies \"19 inches Philips screen\" or \"21 inches Philips screen\"");
			sharedScreenRules.createRelationshipTo(displayDesign, RelTypes.CONSTRAINS);

		//IMPLEMENTATION
			//Video chain
			makeDesignConf(videoChain,"video chain 1", new Node[]{wcb1,fiber,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 2", new Node[]{wcb1,fiber,utp,coupling,refresh,param800,param1024});
			makeDesignConf(videoChain,"video chain 3 - should be invalid", new Node[]{wcb2,fiber,wmb,utp,refresh,param800,param1024,param1600,wcb1});
			makeDesignConf(videoChain,"video chain 4", new Node[]{wcb2,fiber,rx,utp,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 5", new Node[]{wcb1,fiber,split,utp,refresh,param800,param1024});
			makeDesignConf(videoChain,"video chain 6", new Node[]{wcb2,fiber,wmb,split,utp,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 7", new Node[]{wcb2,fiber,rx,split,utp,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 8", new Node[]{wcb1,fiber,flexComponent,utp,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 9", new Node[]{wcb2,fiber,wmb,flexComponent,utp,refresh,param800,param1024,param1600});
			makeDesignConf(videoChain,"video chain 10",new Node[]{wcb2,fiber,rx,flexComponent,utp,refresh,param800,param1024,param1600});

			//Display
			makeDesignConf(displayDesign,"display 1: dedicated 3rd party screen", new Node[]{dedicatedMonitorMode,thirdPartyScreen,direct});
			makeDesignConf(displayDesign,"display 2: dedicated philips screen", new Node[]{dedicatedMonitorMode,philips21,direct});
			makeDesignConf(displayDesign,"display 3: shared monitor",new Node[]{ sharedMonitorMode,philips21,split,sharing});
			makeDesignConf(displayDesign,"display 4: flexvision",new Node[]{flexvisionMode,philips56,flexComponent,flex});

		
		//RULES		
			Node rule1 = editor.createNode("high definition processing", ruleProps);
			rule1.setProperty(Attributes.RULE, "\"Video export resolution: 1600x1200\" iff not \"utp 30m\" or w.c.b.2 or flexvision");
			rule1.createRelationshipTo(videoChain, RelTypes.CONSTRAINS);
	
			Node dedicatedDisplayChoice = editor.createNode("dedicated display user choice", ruleProps);
			dedicatedDisplayChoice.setProperty(Attributes.RULE, "\"Dedicated monitor for 3rd party product\" implies \"dedicated monitor mode\"");
			dedicatedDisplayChoice.createRelationshipTo(displayDesign, RelTypes.CONSTRAINS);
			
			Node sharedDisplayChoice = editor.createNode("shared display user choice", ruleProps);
			sharedDisplayChoice.setProperty(Attributes.RULE, "\"Shared monitor for 3rd party product\" implies \"shared monitor mode\"");
			sharedDisplayChoice.createRelationshipTo(displayDesign, RelTypes.CONSTRAINS);
			
			Node flexvisionUserChoice = editor.createNode("flexvision user choice", ruleProps);
			flexvisionUserChoice.setProperty(Attributes.RULE, "\"Flexvision for 3rd party product\" implies \"flexvision mode\"");
			flexvisionUserChoice.createRelationshipTo(displayDesign, RelTypes.CONSTRAINS);
			
			
			List<Node> toUpdate = new ArrayList<Node>();
			toUpdate.add( videoChain );
			toUpdate.add( displayDesign );
			toUpdate.add( video_export_interface  );
			toUpdate.add( displayModeInterface );
			
			
			
			
					
	}

	private void makeDesignConf(Node design, String name, Node[] items) {
		
		Map <String, String> implProp = new HashMap<String,String>();
		implProp.put(Attributes.DOMAIN, Domains.IMPLEMENTATION);

		Node conf = editor.createNode(name, implProp); 
		conf.createRelationshipTo(design, RelTypes.IS_A);
		
		for(int i = 0 ; i < items.length; i++)
		{
			conf.createRelationshipTo(items[i], RelTypes.CONTAINS);
		}
	}

}
