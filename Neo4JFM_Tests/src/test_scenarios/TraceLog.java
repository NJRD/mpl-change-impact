package test_scenarios;
import java.util.Date;
import java.util.Vector;



public class TraceLog
{
	static TraceLog _instance = null;
	private static final boolean _trace = true;

	private Vector<tick> ticks = new Vector<tick>();
	
	public static TraceLog getLogger()
	{
		if(null == _instance)
			_instance = new TraceLog();
		return _instance;
	}
	
	private TraceLog()
	{
	}
	
	
	public String getLog(boolean flush)
	{
		if(!_trace)
			return "";
		
		StringBuffer msg = new StringBuffer(250);
		for(tick t : ticks)
		{
			msg.append(t.toString() );
		}
		
		if(flush)
			ticks.clear();
		
		return msg.toString();
	}	
	
	public void tick(Exception i)
	{
		if(!_trace)
			return;
		
		ticks.add( new tick(i) );
	}
	
	
	public void clearLog()
	{
		ticks.clear();
	}
	
	
	private class tick
	{
		private Exception _e = null;
		private long _time =  (long) -1.0;
		
		public tick(Exception e)
		{
			_e = e; 
			_time = new Date().getTime();
		}
		
		public String toString()
		{
			String msg = null;
			if(_e != null)
			{ 	
				StackTraceElement[] stack = _e.getStackTrace();
				StackTraceElement init = stack[0];
				 msg =  "["+ _time + "]\t"+ init.getClassName() + " :: " + init.getMethodName() + "\n";
			}
			else
			{
				 msg =  "["+ _time + "] \n";
			}
			return msg;
		}
	}
}
