/**
 * 
 */
package test_scenarios;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Types;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Dante
 *
 */
public class import_export_tests
{

	private String model_name = "/model_1";
	private String model_name_2 = "/model_2";
	private String db_path = test_resources.database_folder+"import_export_tests/";
	private String output_folder = test_resources.output_folder+"import_export_tests/";
	private GraphNavigator nav = null;
	private GraphEditor editor = null;
 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		editor = new GraphEditor( ExternalResourcesRepository.graphDb );
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		GraphEditor.resetAll(db_path);
	}

	@Test
	/**
	 * This test imports a FeatureIDE xml model, with its combination rules and configurations and checks that the data is indeed in the database in the expected format.
	 */
	public void test_1()
	{
		FMImporter importer = new FMImporter(test_resources.model_folder+model_name);
		importer.importIntoNeo4J();
		
		Node fm = nav.getNodeByType( "model_1" , Types.FEATURE_MODEL );
		assertNotNull( fm );
		Node f = nav.getNodeByType( "Feature_12" , Types.FEATURE );
		assertNotNull( f );
		
		Map<String,List<String>>confs = nav.getConfigurations( fm.getId() );
		assertEquals(confs.keySet().size(),3);
		
		List<Long> ctc_ids = nav.getCrossTreeConstraintIds( fm );
		assertEquals(ctc_ids.size(),1);
		
		Map<String,List<String>> shared_feat_names = nav.getSharedFeatureNames( fm.getId() );
		assertEquals(shared_feat_names.keySet().size() ,0);	
		
		importer = new FMImporter(test_resources.model_folder+model_name_2);
		importer.importIntoNeo4J();
		
		Node fm2 = nav.getNodeByType( "model_2" , Types.FEATURE_MODEL );
		assertNotNull( fm2 );
		
		Map<String,List<String>>confs2 = nav.getConfigurations( fm2.getId() );
		assertEquals(confs2.keySet().size(),2);
		
		List<Node> rules = nav.getCrossFMRules( fm, fm2 );
		assertEquals(rules.size(),1);
		Node r = rules.get( 0 );
		Map<String,String> rule_attrs = nav.getAttributes( r );
		String expr = rule_attrs.get( Attributes.RULE);
		String[] feats = expr.split( "\\|\\|" );
		for(String feat : feats)
		{
			
			feat = feat.trim();
			System.out.println( feat );
			if(feat.length() == 0 )
				continue;
			assertTrue(feat.equals( "!Feature_12" ) || feat.equals( "Feature_8" ) || feat.equals( "Feature_7" ));
		}
		
		
		
	}

}
