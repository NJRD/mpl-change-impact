/**
 * 
 */
package test_scenarios;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;


/**
 * @author Dante
 *
 */
public class Propagation_tests
{

	//private String db_path = test_resources.database_folder+"import_export_tests/";
	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"import_export_tests/";

	private GraphNavigator nav = null;
	private GraphEditor editor = null;
 
	private String path_to_model_1= test_resources.model_folder+ "/a_impact_test_1/";
	private String path_to_model_2= test_resources.model_folder+"/a_impact_test_2/";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		editor = new GraphEditor( ExternalResourcesRepository.graphDb );
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		//GraphEditor.resetAll(db_path);
	}

	@Test
	/**
	 * Simulation of the removal of an isolated configuration. 
	 * 
	 * Only configuration I_II_VI_III of model 2 does not share any feature with model 1. 
	 * 
	 * Expected outcome of the simulation: no propagation. Only the configuration removed. 
	 * 
	 * Release mode:
	 * Execution time : total runtime 3 to 6 seconds
	 * Simulation time (ticks) : 99ms / 100ms
	 */
	public void test_1() throws IOException, RendererMustNotBeNullException
	{
	
	
			String output_file = "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4jFM_MPL_Generator/resources/test_scenario.html";
			File report = new File(output_file);
			report.createNewFile();
			BufferedWriter writer = new BufferedWriter(new FileWriter(report));
	
			//importing  FMs
			FMImporter importer = new FMImporter(path_to_model_1);
			importer.importIntoNeo4J();
			importer = new FMImporter(path_to_model_2);
			importer.importIntoNeo4J();
			
			NeoFM fm2 = NeoFM.getFM( "model_2" );

			Node conf_to_remove = nav.getNodeByType( "I_II_VI_III.config" ,Types.CONFIGURATION );
			Map<String, List<String>> conf_desc = buildConfDescriptor( conf_to_remove );

			//create initial report
			ConfChangeReport isolated_conf_removal = new ConfChangeReport();
			isolated_conf_removal.setRemovedConfigurations( conf_desc );
			isolated_conf_removal.fm = "model_2";
			
			ChangeRepository.getRepo().clear_repo();
			
			long _time_1 = new Date().getTime();
			
			fm2.simulateConfChange( isolated_conf_removal );

			long _time_2 = new Date().getTime();

			System.out.println( "simulation time : "+ (_time_2 -_time_1 ) );

			List<ConfChangeReport> reported_conf_changes = ChangeRepository.getRepo().getRootConfChanges();
			
			assertTrue ( ChangeRepository.getRepo().conf_change_count() == 1);
			assertTrue(reported_conf_changes.contains( isolated_conf_removal ));
			assertTrue ( ChangeRepository.getRepo().cfmc_change_count() == 0);
			
			
			 conf_to_remove = nav.getNodeByType( "I_II_IV_VI_D.config" ,Types.CONFIGURATION );
			conf_desc = buildConfDescriptor( conf_to_remove );
			
			ConfChangeReport  impact = new ConfChangeReport();
			impact.setRemovedConfigurations( conf_desc );
			impact.fm = "model_2";

			ChangeRepository.getRepo().clear_repo();
			
			_time_1 = new Date().getTime();

			fm2.simulateConfChange( impact );
			
			_time_2 = new Date().getTime();
			
			System.out.println( "simulation time : "+ (_time_2 -_time_1 ) );
		

			reported_conf_changes = ChangeRepository.getRepo().getRootConfChanges();
			for(int i = 0 ; i < reported_conf_changes.size() ; i++)
			{
				System.out.println( "## Change source ");
				System.out.println( reported_conf_changes.get( i ).dump() );
			}

			assertTrue(reported_conf_changes.size() == 1);
			assertTrue(reported_conf_changes.contains( impact ));
			
			ChangeRepository change = ChangeRepository.getRepo(); 
			
			assertTrue (change.conf_change_count() == 2);
			assertTrue ( change.cfmc_change_count() == 2);

	        writer.append( (MeasureFactory.getReport( new DefaultHTMLRenderer() )));
			writer.flush();
			writer.close();
				
	}

	private Map<String, List<String>> buildConfDescriptor(Node conf_to_remove)
	{
		List<Node> result = nav.getPointedNodes( conf_to_remove , RelTypes.CONTAINS);
		String conf_name = nav.getAttributes( conf_to_remove ).get( Attributes.NAME );
		List<String> feature_names = new ArrayList<String>();
		for(Node n : result)
		{
			feature_names.add ( nav.getAttributes( n).get(Attributes.NAME) ) ;
		}
		Map<String,List<String>> conf_desc = new HashMap<String,List<String>>();
		conf_desc.put( conf_name , feature_names );
		return conf_desc;
	}

}
