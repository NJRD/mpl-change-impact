/**
 * 
 */
package test_scenarios;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.CFMCChangeReport;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Dante
 *
 */
public class Philips_case_test
{
	private String path_to_video_if = test_resources.model_folder+"Philips/interface_video_export/";
	private String path_to_display_if = test_resources.model_folder+"Philips/interface_display/";
	private String path_to_data_if = test_resources.model_folder+"Philips/interface_data_exchange/";
	private String path_to_video_design = test_resources.model_folder+"Philips/design_video_chain/";
	private String path_to_display_design = test_resources.model_folder+"Philips/design_display/";
	private String path_to_data_design = test_resources.model_folder+"Philips/design_data_exchange/";
	
	//private String db_path = test_resources.database_folder+"import_export_tests/";
	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"import_export_tests/";

	private GraphNavigator nav = null;
	private GraphEditor editor = null;
 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		editor = new GraphEditor( ExternalResourcesRepository.graphDb );
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		//GraphEditor.resetAll(db_path);
	}

	@Test
	/**
	 * This test imports a FeatureIDE xml model, with its combination rules and configurations and checks that the data is indeed in the database in the expected format.
	 */
	public void test_1()
	{
		//importing interface FMs
		FMImporter importer = new FMImporter(path_to_video_if);
		importer.importIntoNeo4J();
		importer = new FMImporter(path_to_display_if);
		importer.importIntoNeo4J();
		importer = new FMImporter(path_to_data_if);
		importer.importIntoNeo4J();
		
		//importing design FMs
		importer = new FMImporter(path_to_display_design);
		importer.importIntoNeo4J();
		importer = new FMImporter(path_to_video_design);
		importer.importIntoNeo4J();
		importer = new FMImporter(path_to_data_design);
		importer.importIntoNeo4J();
		
		NeoFM video_design = NeoFM.getFM( "design video chain" );
		
		System.out.println("## test scenario : removing display w.c.b.2 from system \n");
		
		long t1 = new Date().getTime();
		video_design.simulateFeatureRemoval( "display w.c.b.2" );
		long t2 = new Date().getTime();
		
		System.out.println( "Simulation time : "  +  (t2 - t1) + "ms \n" );
		StringBuffer info = new StringBuffer(250);
		for(ConfChangeReport init : ChangeRepository.getRepo().getRootConfChanges())
		{
			info.append(" ## Impact chain\n" );
			info.append(init.dump());
		}
		
		
		System.out.println(info.toString());
		
	}

}
