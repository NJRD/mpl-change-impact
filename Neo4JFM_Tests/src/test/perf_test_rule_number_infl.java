/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import test_scenarios.test_resources;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;
import de.mcs.jmeasurement.Monitor;

/**
 * @author Dante
 *
 */
public class perf_test_rule_number_infl
{
	// 	/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/configs/2.config 

	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"perf_tests//";

	private GraphNavigator nav = null;
	private GraphEditor editor = null;
	
	private int iterations = 10;
 
    @BeforeClass 
    public static void onlyOnce() {
    	System.out.println( "medium fm, 6 shared features, rule # influence test");
    	System.out.println( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time - ms");
     }
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		editor = new GraphEditor( ExternalResourcesRepository.graphDb );
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		ExternalResourcesRepository.shutDown();
	}
	
	
	/************************************************************/
	//		INFLUENCE OF # OF CONF
	/************************************************************/
	@Test
	public void test_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelB_Full/";
			
			String modified_model_name = "modelB";
			
			List<String> removed_confs = new ArrayList<String>(); 
			removed_confs.add(  "B_None.config" );
			perf_test_small_1_shared.multi_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_confs , "" );
	}
	
	@Test
	public void test_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelB_Full/";
			
			String modified_model_name = "modelB";
			
			List<String> removed_confs = new ArrayList<String>(); 
			removed_confs.add(  "B_None.config" );
			perf_test_small_1_shared.multi_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_confs , "" );
	}
	
	
	@Test
	public void test_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelB_Full/";
			
			String modified_model_name = "modelB";
			
			List<String> removed_confs = new ArrayList<String>(); 
			removed_confs.add(  "B_None.config" );
			removed_confs.add(  "B1.config" );
			perf_test_small_1_shared.multi_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_confs , "" );
	}
	
	@Test
	public void test_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelB_Full/";
			
			String modified_model_name = "modelB";
			
			List<String> removed_confs = new ArrayList<String>(); 
			removed_confs.add(  "B_None.config" );
			removed_confs.add(  "B1.config" );
			removed_confs.add(  "B1_2.config" );
			
			perf_test_small_1_shared.multi_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_confs , "" );
	}
	
	@Test
	public void test_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/Large_NbImpactedRules/ModelB_Full/";
			
			String modified_model_name = "modelB";
			
			List<String> removed_confs = new ArrayList<String>(); 
			removed_confs.add(  "B_None.config" );
			removed_confs.add(  "B1.config" );
			removed_confs.add(  "B1_2.config" );
			removed_confs.add(  "B2.config" );
			
			perf_test_small_1_shared.multi_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_confs , "" );
	}
}
