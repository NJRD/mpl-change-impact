/**
 * 
 */
package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

import test_scenarios.test_resources;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.Monitor;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;
import de.ovgu.featureide.fm.core.FeatureModel;

/**
 * @author Dante
 *
 */
public class perf_test_small_1_shared
{
	// 	/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/configs/2.config 

	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"perf_tests//";

	private static GraphNavigator nav = null;
	
 
	static private int iterations = 20; 
	
	
    @BeforeClass 
    public static void onlyOnce() {
    	System.out.println( "small  feature model, 1 shared feature, with effect of delete selection");
		System.out.println( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time - ms");
     }
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).setConfig( GraphDatabaseSettings.cache_type, "none" ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		//GraphEditor.resetAll(db_path);
		String output_file = "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4jFM_MPL_Generator/resources/perf_scenarios.html";
		File report = new File(output_file);
		report.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(report));
		
		writer.append(  MeasureFactory.getReport( new DefaultHTMLRenderer() ) );
		writer.flush();
		writer.close();
		
		//System.out.println( "\n -------------------------" );
	}

	@Test
	public void test_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelA/";
			
			String model_name = "modelA"; 
			String conf_name = "A1.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	/************************************************************/
	//		INFLUENCE OF # OF CONF
	/************************************************************/
	@Test
	/*
	 */
	public void test_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelA/";
			
			String model_name = "modelA"; 
			String conf_name = "A1.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

	
	@Test
	/*
	 */
	public void test_1_2() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelC/";
		String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelD/";
		String model_name = "modelC";
		String conf_name = "C1.config";
		String msg =  "#1 - 1/3 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration";
		//System.out.print("10,1,7,1,3,1,1,2,1,");
		
		single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	
	@Test
	/*
	 */
	public void test_1_3() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelE/";
		String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelF/";
				
		String model_name = "modelE";
		String conf_name = "E1.config";
		String msg = "1/20 confs, 1 shared featured, 1 shared conf each - removing shared one" ;
		//System.out.print("10,1,7,1,20,1,1,2,1,");
		
				single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	
	
	@Test
	/*
	 */
	public void test_1_4() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelG/";
		String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelH/";
		
		String model_name = "modelG";
		String conf_name = "C1.config";
		String msg =  "#1 - 1/50 confs, 1 shared featured, 1 shared confs- removing shared one" ;
				
		//System.out.print("10,1,7,1,50,1,1,2,1,");
		
				single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

//	
	@Test
	/*
	 */
	public void test_1_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelO/";
			String model_name = "modelN";
			String conf_name = "C1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,100,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

	@Test
	/*
	 */
	public void test_1_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelP/";
			String model_name = "modelN";
			String conf_name = "C1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,150,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_1_7() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelQ/";
			String model_name = "modelN";
			String conf_name = "C1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,202,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_1_8() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelR/";
			String model_name = "modelN";
			String conf_name = "C1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,202,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_1_9() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelS/";
			String model_name = "modelN";
			String conf_name = "C1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,202,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_2_1() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelC/";
		String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelD/";
		String model_name = "modelD";
		String conf_name = "D1.config";
		String msg =  "#1 - 3/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//System.out.print("10,1,7,3,1,1,1,2,1,");
		
				single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	
	@Test
	/*
	 */
	public void test_2_2() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelE/";
		String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelF/";
		
		String model_name = "modelF";
		String conf_name = "F1.config";
		String msg = "20/1 confs, 1 shared featured, 1 shared conf each - removing shared one";
		//System.out.print("10,1,7,20,1,1,1,2,1,");
		
				single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	
	
	@Test
	/*
	 */
	public void test_2_3() throws IOException, RendererMustNotBeNullException
	{
		String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelG/";
		String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelH/";
		
		String model_name = "modelH";
		String conf_name ="B1.config";
		String msg =  "#2 - 50/1 confs, 1 shared featured, 1 shared confs- removing shared one" ;

		//System.out.print("10,1,7,50,1,1,1,2,1,");
		
				single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}


	@Test
	/*
	 */
	public void test_2_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelO/";
			
			String model_name = "modelO";
			String conf_name = "D1.config";
			String msg = "#2 - 100/1  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,100,1,1,1,2,1,");
			
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

	@Test
	/*
	 */
	public void test_2_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelP/";
			String model_name = "modelP";
			String conf_name = "D1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,150,1,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_2_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelQ/";
			
			String model_name = "modelQ";
			String conf_name = "D1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,202,1,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_2_7() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelR/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String model_name = "modelR";
			String conf_name = "D1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,202,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_2_8() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelS/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelN/";
			String model_name = "modelS";
			String conf_name = "D1.config";
			String msg =  "#1 - 1/100  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,202,1,1,2,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	

	/************************************************************/
	//		INFLUENCE OF # OF REMOVED CONF
	/************************************************************/

	@Test
	/*
	 */
	public void test_3_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelBB/";
			
			String model_name = "modelAA";
			String conf_name = "A1.config";
			String msg = "#2 - 100/1  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,4,1,1,4,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_3_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelCC/";
			
			String model_name = "modelAA";
			String conf_name = "A1.config";
			String msg = "#2 - 100/1  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,21,1,1,21,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_3_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelDD/";
			
			String model_name = "modelAA";
			String conf_name = "A1.config";
			String msg = "#2 - 100/1  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,51,1,1,51,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	/*
	 */
	public void test_3_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelEE/";
			
			String model_name = "modelAA";
			String conf_name = "A1.config";
			String msg = "#2 - 100/1  confs, 1 shared featured, 1 shared conf each - 1 removing shared one" ;
			//System.out.print("10,1,7,1,101,1,1,101,1,");
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_3_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";	
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelFF/";
			
			String modified_model_name = "modelAA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			//System.out.print("10,1,7,1,151,1,1,151,1,");
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	

	@Test
	public void test_3_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelAA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelGG/";
			
			String modified_model_name = "modelAA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			//System.out.print("10,1,7,1,201,1,1,");
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	
	static void single_conf_removal(String path_to_model_1, String path_to_model_2, String model_name,
			String conf_name, String msg)
	{
		
		if(nav == null)
			nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		
		//importing  FMs
		FMImporter importer = new FMImporter(path_to_model_1);
		NeoFM model_1 = importer.importIntoNeo4J();
		importer = new FMImporter(path_to_model_2);
		NeoFM model_2 = importer.importIntoNeo4J();

		
		print_stats( model_1 , model_2 );
		
		NeoFM fm = NeoFM.getFM(model_name );

		Node conf_to_remove = nav.getNodeByType( conf_name ,Types.CONFIGURATION );
		Map<String, List<String>> conf_desc = buildConfDescriptor( conf_to_remove );

		//create initial report
		ConfChangeReport removal = new ConfChangeReport();
		removal.setRemovedConfigurations( conf_desc );
		removal.fm =model_name;

		ChangeRepository.getRepo().clear_repo();			
		Monitor m = MeasureFactory.getMonitor( msg);
		
		run( fm , removal , m );
	}
	
	static void multi_conf_removal(String path_to_model_1, String path_to_model_2, String model_name, List<String> conf_names, String msg)
	{
		
		if(nav == null)
			nav = new GraphNavigator( ExternalResourcesRepository.graphDb );
		
		//importing  FMs
		FMImporter importer = new FMImporter(path_to_model_1);
		NeoFM model_1 = importer.importIntoNeo4J();
		importer = new FMImporter(path_to_model_2);
		NeoFM model_2 = importer.importIntoNeo4J();

		
		print_stats( model_1 , model_2 );
		
		NeoFM fm = NeoFM.getFM(model_name );
		//create initial report
		ConfChangeReport removal = new ConfChangeReport();
		
		removal.fm =model_name;
		Map<String, List<String>> conf_descs = new HashMap<String,List<String>>();
		
		for(String conf_name : conf_names)
		{
			Node conf_to_remove = nav.getNodeByType( conf_name ,Types.CONFIGURATION );
			conf_descs.putAll( buildConfDescriptor( conf_to_remove ) );
		}
		removal.setRemovedConfigurations( conf_descs );
		ChangeRepository.getRepo().clear_repo();			
		Monitor m = MeasureFactory.getMonitor( msg);
		
		run( fm , removal , m );
	}

	static public void print_stats(NeoFM model_1, NeoFM model_2)
	{
		FeatureModel fm1 = model_1.get_featureide_fm();
		FeatureModel fm2 = model_2.get_featureide_fm();
		
		Node n1 = model_1.getNode();
		Node n2 = model_2.getNode();
		
		int feats_1 = fm1.getNumberOfFeatures();
		int shared_1 = nav.getSharedFeatureNames( n1 ).entrySet().iterator().next().getValue().size();
		
		int feats_2 =fm2.getNumberOfFeatures();
		int shared_2 = nav.getSharedFeatureNames( n2 ).entrySet().iterator().next().getValue().size();
		
		Map<String,List<String>> confs_1 = nav.getConfigurations( model_1.getNode().getId() );
		Map<String,List<String>> confs_2 = nav.getConfigurations( model_2.getNode().getId() );
		
		int conf_1_size = confs_1.size();
		int conf_2_size = confs_2.size();
		
		int avg_1 = 0;
		for(String conf_name : confs_1.keySet())
		{
			avg_1 += confs_1.get( conf_name ).size();
		}
		avg_1 = avg_1 / confs_1.size();
		
		int avg_2 = 0;
		for(String conf_name : confs_2.keySet())
		{
			avg_2 += confs_2.get( conf_name ).size();
		}
		avg_2 = avg_2 / confs_2.size();
		
		List<List<String>> rules = nav.getCrossFMRules( model_1.getNode().getId() , model_2.getNode().getId() );
		int nb_rules = rules.size();
		int rule_size = 0;
		
		List<String> rule = rules.get( 0 );
			if(rule != null)
				rule_size = rule.size();
			//FORMAT/( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time ");
			System.out.print( feats_1 +","+ shared_1+","+conf_1_size  +","+avg_1 +","+ feats_2 + "," +shared_2+","+ conf_2_size +","+ avg_2+","+nb_rules +","+rule_size+",");
	}
	
	
	public static void run(NeoFM fm, ConfChangeReport removal, Monitor m)
	{
		long elapsed = 0;
		for(int i = 0; i < iterations ; i++)
		{
			ChangeRepository.getRepo().clear_repo();

			m.start();
			long lDateTime = System.nanoTime();
			
			fm.simulateConfChange( removal );
			
			long lDateTime2 = System.nanoTime();
			m.stop();
			elapsed += (lDateTime2 - lDateTime);

			removal.resetPropagatedChanges();
			
		}
		ChangeRepository r  = ChangeRepository.getRepo();
		int cfmc = r.cmfc_change_report_counts()/2;
		int c = r.conf_change_report_counts();
		//FORMAT/( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, avg. elapsed time - ms");
		System.out.print(" " + c + "," + cfmc+",");
		System.out.print((elapsed) / ( 1000000 * iterations )+"\n");


	}
	
	
	static private Map<String, List<String>> buildConfDescriptor(Node conf_to_remove)
	{
		List<Node> result = nav.getPointedNodes( conf_to_remove , RelTypes.CONTAINS);
		String conf_name = nav.getAttributes( conf_to_remove ).get( Attributes.NAME );
		List<String> feature_names = new ArrayList<String>();
		for(Node n : result)
		{
			feature_names.add ( nav.getAttributes( n).get(Attributes.NAME) ) ;
		}
		Map<String,List<String>> conf_desc = new HashMap<String,List<String>>();
		conf_desc.put( conf_name , feature_names );
		return conf_desc;
	}
	
	
	

}
