/**
 * 
 */
package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;

import test_scenarios.test_resources;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.Monitor;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;
import de.ovgu.featureide.fm.core.FeatureModel;

/**
 * @author Dante
 *
 */
public class perf_test_small_2_shared
{
	// 	/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/configs/2.config 

	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"perf_tests//";

	private static GraphNavigator nav = null;
	
 
	static private int iterations = 20; 
	
	
    @BeforeClass 
    public static void onlyOnce() {
    	System.out.println( "small  feature model, 2 shared feature");
		System.out.println( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time - ms");
     }
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).setConfig( GraphDatabaseSettings.cache_type, "none" ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
		
		nav = new GraphNavigator( ExternalResourcesRepository.graphDb );

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		//GraphEditor.resetAll(db_path);
		String output_file = "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4jFM_MPL_Generator/resources/perf_scenarios.html";
		File report = new File(output_file);
		report.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(report));
		
		writer.append(  MeasureFactory.getReport( new DefaultHTMLRenderer() ) );
		writer.flush();
		writer.close();
		
		//System.out.println( "\n -------------------------" );
	}

	@Test
	public void test_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelB/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelA/";
			
			String model_name = "modelA"; 
			String conf_name = "A1.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	/************************************************************/
	//		INFLUENCE OF # OF CONF
	/************************************************************/

	/*
	 */
	@Test
	public void test_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelB/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelA/";
			
			String model_name = "modelA"; 
			String conf_name = "A1.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

	@Test
	public void test_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelD/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}	
	
	@Test
	public void test_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelD/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelE/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelF/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_7() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelH/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	
	@Test
	public void test_8() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelI/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_9() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelJ/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}
	
	@Test
	public void test_10() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelK/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs_2_shared/ModelC/";
			
			String model_name = "modelA"; 
			String conf_name = "A1_2.config";
			String msg = "#1 - 1/1 confs, 1 shared featured, shared conf: 1/1 - remove shared configuration" ;
		//	System.out.print("10,1,7,1,1,1,1,2,1,");
			
			perf_test_small_1_shared.single_conf_removal( path_to_model_1 , path_to_model_2 , model_name , conf_name , msg );
	}

}
