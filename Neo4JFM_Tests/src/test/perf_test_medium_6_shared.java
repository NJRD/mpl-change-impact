/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.domain.Model.Attributes;
import neo4jfm.domain.Model.RelTypes;
import neo4jfm.domain.Model.Types;
import neo4jfm.featuremodel.ChangeRepository;
import neo4jfm.featuremodel.ConfChangeReport;
import neo4jfm.featuremodel.NeoFM;
import neo4jfm.operations.FMImporter;
import neo4jfm.utils.GraphEditor;
import neo4jfm.utils.GraphNavigator;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import test_scenarios.test_resources;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;
import de.mcs.jmeasurement.Monitor;

/**
 * @author Dante
 *
 */
public class perf_test_medium_6_shared
{
	// 	/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/configs/2.config 

	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"perf_tests//";


 
    @BeforeClass 
    public static void onlyOnce() {
    	System.out.println( "medium fm, 6 shared features");
    	System.out.println( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time - ms");
     }
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		ExternalResourcesRepository.shutDown();
	}
	
	
	/************************************************************/
	//		INFLUENCE OF # OF CONF
	/************************************************************/
	@Test
	public void test_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelB/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelB/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelC/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelD/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelE/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelF/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelG/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelH/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelJ/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelL/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_7() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelN/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_8() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelP/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_1_9() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelR/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	
	@Test
	public void test_2_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelB/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelC/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelD/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	
	@Test
	public void test_2_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelE/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelF/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelG/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelH/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelJ/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelL/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_7() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelN/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_8() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelP/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	@Test
	public void test_2_9() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelA_Full/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs_HigherDep/ModelR/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B_Shared_1_Shared_2_Shared_3_Shared_4_Shared_5_Shared_6.config";

			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , "" );
	}
	
	
	private void single_conf_removal(String path_to_model_1, String path_to_model_2, String modified_model_name,
			String removed_config_name, String msg)
	{
		perf_test_small_1_shared.single_conf_removal( path_to_model_1, path_to_model_2, modified_model_name,removed_config_name,msg);
	}
	
	
}
