/**
 * 
 */
package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import neo4jfm.ExternalResourcesRepository;
import neo4jfm.utils.GraphEditor;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import test_scenarios.test_resources;

import de.mcs.jmeasurement.MeasureFactory;
import de.mcs.jmeasurement.RendererMustNotBeNullException;
import de.mcs.jmeasurement.renderer.DefaultHTMLRenderer;

/**
 * @author Dante
 *
 */
public class perf_test_medium_2_shared
{
	// 	/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/SmallFMs/ModelB/configs/2.config 

	private String db_path = "/Users/Dante/Documents/workspace/Git_repositories/impact/database/"; //temp to visualize the data.
	private String output_folder = test_resources.output_folder+"perf_tests//";

	
 
    @BeforeClass 
    public static void onlyOnce() {
    	System.out.println( "medium fm, 2 shared features");
    	System.out.println( "# feat. 1, # shared feat. 1, # conf. 1, avg conf. size 1,# feat. 2, # shared feat. 2, # conf. 2, avg conf. size 2, # cross fm rules, rule size, # impact conf, # impact rule, elapsed time - ms");
     }
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		GraphEditor.resetAll(db_path);
		 
		//initialize database
		ExternalResourcesRepository.setDbPath( db_path  );
		ExternalResourcesRepository.setImportPath( test_resources.model_folder );
		ExternalResourcesRepository.setExportPath(output_folder);
		
		GraphDatabaseService db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder( db_path ).newGraphDatabase();
		ExternalResourcesRepository.createDb(db);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		//cleaning up database
		ExternalResourcesRepository.shutDown();
		//GraphEditor.resetAll(db_path);
		String output_file = "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4jFM_MPL_Generator/resources/perf_scenarios_medium.html";
		File report = new File(output_file);
		report.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(report));
		
		writer.append(  MeasureFactory.getReport( new DefaultHTMLRenderer() ) );
		writer.flush();
		writer.close();
		

	}

	@Test
	public void test_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelB/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	/************************************************************/
	//		INFLUENCE OF # OF CONF
	/************************************************************/
	@Test
	public void test_1_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA_1/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelB_1/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	

	
	
	@Test
	public void test_1_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelB/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_2() throws IOException, RendererMustNotBeNullException
	{	
				String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelC/";
				String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelD/";
				String modified_model_name = "modelC";
				String removed_config_name = "A1.config";
				String msg = "4/50 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable 10 of target)";
				
				single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}

	@Test
	public void test_1_3() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelE/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelF/";
				
			String modified_model_name = "modelE";
			String removed_config_name = "A1.config";
			String msg =  "4/50 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_4() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelH/";
				
			String modified_model_name = "modelG";
			String removed_config_name = "A1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_5() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelI/";
				
			String modified_model_name = "modelG";
			String removed_config_name = "A1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_6() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelJ/";
				
			String modified_model_name = "modelG";
			String removed_config_name = "A1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_7() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelK/";
				
			String modified_model_name = "modelG";
			String removed_config_name = "A1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_1_8() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelL/";
				
			String modified_model_name = "modelG";
			String removed_config_name = "A1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	@Test
	
	public void test_2_0() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA_1/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelB_1/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_2_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_2= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_1="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelB/";
			
			String modified_model_name = "modelB";
			String removed_config_name = "B1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_2_2() throws IOException, RendererMustNotBeNullException
	{	
				String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelC/";
				String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelD/";
				String modified_model_name = "modelD";
				String removed_config_name = "B1.config";
				String msg = "4/50 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable 10 of target)";
				
				single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}

	@Test
	public void test_2_3() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelE/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelF/";

			String modified_model_name = "modelF";
			String removed_config_name = "B1.config";
			String msg =  "4/50 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_2_4() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelH/";
				
			String modified_model_name = "modelH";
			String removed_config_name = "B1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	public void test_2_5() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelI/";
				
			String modified_model_name = "modelI";
			String removed_config_name = "B1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	
	@Test
	public void test_2_6() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelJ/";
				
			String modified_model_name = "modelJ";
			String removed_config_name = "B1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_2_7() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelK/";
				
			String modified_model_name = "modelK";
			String removed_config_name = "B1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_2_8() throws IOException, RendererMustNotBeNullException
	{
		
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelG/";
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelL/";
				
			String modified_model_name = "modelL";
			String removed_config_name = "B1.config";
			String msg =  "4/100 confs in each, 2 shared featured, 4 shared conf each - removing shared one (disable none)" ;
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	/************************************************************/
	//		INFLUENCE OF # OF IMPACTED CONF
	/************************************************************/
	
	@Test
	public void test_3_1() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelBB/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	
	@Test
	public void test_3_2() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelCC/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_3_3() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelDD/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_3_4() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelEE/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_3_5() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelFF/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	@Test
	public void test_3_6() throws IOException, RendererMustNotBeNullException
	{
			String path_to_model_1= "/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelA/";	
			String path_to_model_2="/Users/Dante/Documents/workspace/Git_repositories/impact/Neo4JFM_Tests/resources/fms/Scalability/MediumFMs/ModelGG/";
			
			String modified_model_name = "modelA";
			String removed_config_name = "A1.config";
			
			String msg = "#1 - 4 confs in each, 2 shared featured, 3 shared conf each, removing 1 shared config";
			
			single_conf_removal( path_to_model_1 , path_to_model_2 , modified_model_name , removed_config_name , msg );
	}
	
	
	private void single_conf_removal(String path_to_model_1, String path_to_model_2, String modified_model_name,
			String removed_config_name, String msg)
	{
		
		perf_test_small_1_shared.single_conf_removal( path_to_model_1, path_to_model_2, modified_model_name,removed_config_name,msg);		
	}

	
	

}
